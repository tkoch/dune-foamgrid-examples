// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_BOX_CONSTRAINT_HH
#define DUNE_BOX_CONSTRAINT_HH

#include <array>
#include <limits>

#include <dune/common/fvector.hh>

template <class T, int dim>
class BoxConstraint {
public:

    /** \brief Default constructor, makes the obstacle as large as possible */
    BoxConstraint() {
        for (int i=0; i<dim; i++) {
            val[2*i]   = -std::numeric_limits<T>::max();
            val[2*i+1] =  std::numeric_limits<T>::max();
        }
    }

    /** \brief Set obstacle values to +/- std::numeric_limits<T>::max() */
    void clear() {
        for (int i=0; i<dim; i++) {
            val[2*i]   = -std::numeric_limits<T>::max();
            val[2*i+1] =  std::numeric_limits<T>::max();
        }
    }

    //! Subtract vector from box
    BoxConstraint<T,dim>& operator-= (const Dune::FieldVector<T, dim>& v)
    {
        for (int i=0; i<dim; i++) {
            val[2*i]   -= v[i];
            val[2*i+1] -= v[i];
        }
        return *this;
    }

    //! Access to the lower obstacles
    T& lower(size_t i) {return val[2*i];}

    //! Const access to the lower obstacles
    const T& lower(size_t i) const {return val[2*i];}

    //! Access to the upper obstacles
    T& upper(size_t i) {return val[2*i+1];}

    //! Const access to the upper obstacles
    const T& upper(size_t i) const {return val[2*i+1];}

    //! Send box constraint to an output stream
    friend
    std::ostream& operator<< (std::ostream& s, const BoxConstraint<T,dim>& v)
    {
        for (int i=0; i<dim; i++)
            s << "Direction: " << i <<",  val[0] = " << v.val[2*i]
              << ", val[1] = " << v.val[2*i+1] << std::endl;

        return s;
    }

protected:

    std::array<T, 2*dim> val;

};

#endif
