// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef LINEAR_ITERATIONSTEP_HH
#define LINEAR_ITERATIONSTEP_HH

#include <memory>
#include <vector>

#include <dune/common/bitsetvector.hh>
#include <dune/common/shared_ptr.hh>

#include "iterationstep.hh"

//! Base class for iteration steps being called by an iterative linear solver
template<class MatrixType, class VectorType, class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension> >
class LinearIterationStep : public IterationStep<VectorType, BitVectorType>
{
public:

    //! Default constructor
    LinearIterationStep() {}

    /** \brief Destructor */
    virtual ~LinearIterationStep() {}

    //! Constructor being given linear operator, solution and right hand side
    LinearIterationStep(const MatrixType& mat, VectorType& x, const VectorType& rhs) {
        mat_     = Dune::stackobject_to_shared_ptr(mat);
        this->x_ = &x;
        rhs_     = &rhs;
    }

    //! Set linear operator, solution and right hand side
    virtual void setProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs) {
        this->x_ = &x;
        rhs_     = &rhs;
        mat_     = Dune::stackobject_to_shared_ptr(mat);
    }

    //! Set linear operator
    virtual void setMatrix(const MatrixType& mat) {
        mat_     = Dune::stackobject_to_shared_ptr(mat);
    }

    //! Do the actual iteration
    virtual void iterate() = 0;

    //! Return solution object
    virtual VectorType getSol()
    {
        return *(this->x_);
    }

    //! Return linear operator
    virtual const MatrixType* getMatrix() {return mat_.get();}

    /** \brief Checks whether all relevant member variables are set
     * \exception SolverError if the iteration step is not set up properly
     */
    virtual void check() const {
#if 0
        if (!x_)
            DUNE_THROW(SolverError, "Iteration step has no solution vector");
        if (!rhs_)
            DUNE_THROW(SolverError, "Iteration step has no right hand side");
        if (!mat_)
            DUNE_THROW(SolverError, "Iteration step has no matrix");
#endif
    }

    virtual void apply(VectorType& x, const VectorType& r)
    {
        x = 0;
        this->x_ = &x;
        rhs_     = &r;
        iterate();
        x = getSol();
    }

    //! The container for the right hand side
    const VectorType* rhs_;

    //! The linear operator
    std::shared_ptr<const MatrixType> mat_;

};

#endif
