// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_ITERATIVE_SOLVER_HH
#define DUNE_SOLVERS_ITERATIVE_SOLVER_HH

#include <dune/common/ftraits.hh>

#include "solver.hh"
#include "iterationstep.hh"
#include "norm.hh"

    /** \brief Abstract base class for iterative solvers */
    template <class VectorType, class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension> >
    class IterativeSolver : public Solver
    {
        typedef typename VectorType::value_type::field_type field_type;

        // For complex-valued data
        typedef typename Dune::FieldTraits<field_type>::real_type real_type;

    public:

        /** \brief Constructor taking all relevant data */
        IterativeSolver(double tolerance,
                        int maxIterations,
                        VerbosityMode verbosity,
                        bool useRelativeError=true)
            : Solver(verbosity),
              tolerance_(tolerance),
              maxIterations_(maxIterations), errorNorm_(NULL),
              historyBuffer_(""),
              useRelativeError_(useRelativeError)
        {}

        /** \brief Constructor taking all relevant data */
        IterativeSolver(int maxIterations,
                        double tolerance,
                        const Norm<VectorType>* errorNorm,
                        VerbosityMode verbosity,
                        bool useRelativeError=true)
            : Solver(verbosity),
              tolerance_(tolerance),
              maxIterations_(maxIterations), errorNorm_(errorNorm),
              historyBuffer_(""),
              useRelativeError_(useRelativeError)
        {}

        /** \brief Loop, call the iteration procedure
         * and monitor convergence */
        virtual void solve() = 0;

        /** \brief Checks whether all relevant member variables are set
         */
        virtual void check() const
        {
          if (!errorNorm_)
            DUNE_THROW(SolverError, "You need to supply a norm-computing routine to an iterative solver!");
        }

        /** \brief Write the current iterate to disk (for convergence measurements) */
        void writeIterate(const VectorType& iterate, int iterationNumber) const
        {}

        /** \brief The requested tolerance of the solver */
        double tolerance_;

        //! The maximum number of iterations
        int maxIterations_;

        //! The norm used to measure convergence
        const Norm<VectorType>* errorNorm_;

        /** \brief If this string is nonempty it is expected to contain a valid
            directory name.  All intermediate iterates are then written there. */
        std::string historyBuffer_;

        real_type maxTotalConvRate_;

        bool useRelativeError_;

    };

#endif
