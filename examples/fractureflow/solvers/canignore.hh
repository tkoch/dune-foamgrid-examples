// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef CAN_IGNORE_HH
#define CAN_IGNORE_HH

/** \brief Abstract base class for solvers and iterationsteps that are
 *      able to ignore degrees of freedom described by a bit field.
 */
template <class BitVectorType>
class CanIgnore
{
public:

    /** \brief Default constructor */
    CanIgnore()
        : ignoreNodes_(0)
    {}

    /** \brief Virtual destructor.  Does NOT delete the bitfield! */
    virtual ~CanIgnore()
    {}

    /** \brief A flag for each degree of freedom stating whether the
        dof should be ignored by the solver */
    const BitVectorType* ignoreNodes_;

};

#endif
