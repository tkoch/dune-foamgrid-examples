// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_TRANSFEROPERATORS_MANDEL_OBSTACLE_RESTRICTOR_HH
#define DUNE_SOLVERS_TRANSFEROPERATORS_MANDEL_OBSTACLE_RESTRICTOR_HH

#include <dune/common/bitsetvector.hh>

#include "obstaclerestrictor.hh"
#include "multigridtransfer.hh"
#include "compressedmultigridtransfer.hh"
#include "boxconstraint.hh"

template <class VectorType>
class MandelObstacleRestrictor : public ObstacleRestrictor<VectorType>
{
    typedef typename VectorType::field_type field_type;
    
    enum {blocksize = VectorType::block_type::dimension};

public:
    
    virtual void restrict(const std::vector<BoxConstraint<field_type,blocksize> >& f,
                          std::vector<BoxConstraint<field_type,blocksize> >& t,
                          const Dune::BitSetVector<blocksize>& fHasObstacle,
                          const Dune::BitSetVector<blocksize>& tHasObstacle,
                          const MultigridTransfer<VectorType>& transfer,
                          const Dune::BitSetVector<blocksize>& critical);
};

template <class VectorType>
void MandelObstacleRestrictor<VectorType>::
restrict(const std::vector<BoxConstraint<field_type, blocksize> >&f,
        std::vector<BoxConstraint<field_type, blocksize> > &t,
        const Dune::BitSetVector<blocksize>& fHasObstacle,
        const Dune::BitSetVector<blocksize>& tHasObstacle,
        const MultigridTransfer<VectorType>& transfer,
        const Dune::BitSetVector<blocksize>& critical)
{
    if (!(dynamic_cast<const CompressedMultigridTransfer<VectorType>*>(&transfer)))
        DUNE_THROW(Dune::NotImplemented, "Implementation assumes transfer to be of type CompressedMultigridTransfer!");

    const Dune::BCRSMatrix<Dune::FieldMatrix<field_type, 1, 1> >& transferMat =
        dynamic_cast<const CompressedMultigridTransfer<VectorType>*>(&transfer)->getMatrix();

    t.resize(transferMat.M());
    for (size_t i=0; i<t.size(); i++)
        t[i].clear();

    // The Mandel restriction computes the maximum $m$ of all nodal base coefficients
    // in the support of a given coarse grid base function \f$ \phi_c \f$.  If the
    // coefficient of \f$ \phi_c \f$ is smaller than \f$ m \f$ , it is set to \f$ m \f$.

    // We use the multigrid transfer matrix to find all relevant fine grid dofs
    typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<field_type,1,1> >::row_type RowType;
    typedef typename RowType::ConstIterator ColumnIterator;

    // Loop over all coarse grid dofs
    for (size_t i=0; i<transferMat.N(); i++) {

        if (fHasObstacle[i].none())
            continue;

        const RowType& row = transferMat[i];

        ColumnIterator cIt    = row.begin();
        ColumnIterator cEndIt = row.end();

        // Loop over all fine grid base functions in the support of tObsIt
        for(; cIt!=cEndIt; ++cIt) {

            if (tHasObstacle[cIt.index()].none())
                continue;

            // Sort out spurious entries due to numerical dirt
            if ((*cIt)[0][0] < 1e-3)
                continue;

            for (int j=0; j<blocksize; j++)
                if (!critical[i][j] && fHasObstacle[i][j] && tHasObstacle[cIt.index()][j]) {
                    t[cIt.index()].lower(j) = std::max(t[cIt.index()].lower(j), f[i].lower(j));
                    t[cIt.index()].upper(j) = std::min(t[cIt.index()].upper(j), f[i].upper(j));
                }

        }

    }

}

#endif
