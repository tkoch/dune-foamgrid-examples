// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <cassert>

template<class MatrixType, class DiscFuncType, class BitVectorType>
inline
DiscFuncType BlockGSStep<MatrixType, DiscFuncType, BitVectorType>::getSol()
{
    return *(this->x_);
}

template<class MatrixType, class DiscFuncType, class BitVectorType>
inline
void BlockGSStep<MatrixType, DiscFuncType, BitVectorType>::
residual(int index, VectorBlock& r) const
{
    const MatrixType& mat = *this->mat_;

    typedef typename MatrixType::row_type RowType;
    const RowType& row = mat[index];

    typedef typename RowType::ConstIterator ColumnIterator;

    r = (*this->rhs_)[index];

    /* The following loop subtracts
     * \f[ sum_i = \sum_j A_{ij}w_j \f]
     */
    ColumnIterator cIt    = row.begin();
    ColumnIterator cEndIt = row.end();

    for (; cIt!=cEndIt; ++cIt) {
        // r_i -= A_ij x_j
        cIt->mmv((*this->x_)[cIt.index()], r);
    }

}

template<class MatrixType, class DiscFuncType, class BitVectorType>
inline
void BlockGSStep<MatrixType, DiscFuncType, BitVectorType>::iterate()
{
    assert(this->ignoreNodes_ != NULL);

    if (gs_type_ != Direction::BACKWARD)
        for (std::size_t i=0; i<this->x_->size(); i++)
            iterate_step(i);

    if (gs_type_ != Direction::FORWARD)
        for (std::size_t i=this->x_->size()-1; i>=0 && i<this->x_->size(); i--)
            iterate_step(i);
}

template<class MatrixType, class DiscFuncType, class BitVectorType>
inline
void BlockGSStep<MatrixType, DiscFuncType, BitVectorType>::iterate_step(int i)
{
    const MatrixType& mat = *this->mat_;

    size_t const count = (*this->ignoreNodes_)[i].count();
    if (count == BlockSize)
        return;

    VectorBlock r;
    residual(i, r);

    // Compute x_i += A_{i,i}^{-1} r[i]
    VectorBlock v;
    VectorBlock& x = (*this->x_)[i];

    if (count == 0) {
        // No degree of freedom shall be ignored --> solve linear problem
        mat[i][i].solve(v, r);
    } else {
        // Copy the matrix and adjust rhs and matrix so that dofs given in ignoreNodes[i]
        // are not touched
        typename MatrixType::block_type matRes;
        for (int j = 0; j < BlockSize; ++j) {
            if ((*this->ignoreNodes_)[i][j]) {
                r[j] = 0;

                for (int k = 0; k < BlockSize; ++k)
                    matRes[j][k] = (k == j);
            } else
                matRes[j] = mat[i][i][j];

        }
        matRes.solve(v, r);
    }
    // Add correction;
    x += v;
}
