// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef MULTIGRID_STEP_HH
#define MULTIGRID_STEP_HH

#include <vector>
#include <map>
#include <dune/common/bitsetvector.hh>

#include "multigridtransfer.hh"
#include "iterativesolver.hh"
#include "lineariterationstep.hh"

/** \brief A linear multigrid step */
    template<
        class MatrixType,
        class VectorType,
        class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension> >
    class MultigridStep : public LinearIterationStep<MatrixType, VectorType, BitVectorType>
    {

        static const int blocksize = VectorType::block_type::dimension;

    public:

        MultigridStep() :
            presmoother_(0),
            postsmoother_(0),
            basesolver_(0),
            ignoreNodesHierarchy_(0),
            preprocessCalled(false)
        {}

        MultigridStep(const MatrixType& mat,
                      VectorType& x,
                      const VectorType& rhs,
                      int mu, int nu1, int nu2,
                      LinearIterationStep<MatrixType, VectorType>* preSmoother,
                      LinearIterationStep<MatrixType, VectorType>* postSmoother,
                      Solver* baseSolver,
                      const BitVectorType* ignoreNodes) :
            LinearIterationStep<MatrixType, VectorType>(mat, x, rhs),
            preprocessCalled(false)
        {
            mu_  = mu;
            nu1_ = nu1;
            nu2_ = nu2;

            setSmoother(preSmoother,postSmoother);

            basesolver_   = baseSolver;

            this->ignoreNodes_ = ignoreNodes;
        }

        MultigridStep(const MatrixType& mat,
                      VectorType& x,
                      const VectorType& rhs) :
            LinearIterationStep<MatrixType, VectorType>(mat, x, rhs),
            basesolver_(0),
            preprocessCalled(false)
        {}

        virtual ~MultigridStep()
        {
            for (int i=0; i<int(ignoreNodesHierarchy_.size()-1); i++)
            {
                if (ignoreNodesHierarchy_[i])
                    delete(ignoreNodesHierarchy_[i]);
            }
        }

        virtual void setProblem(const MatrixType& mat,
                                VectorType& x,
                                const VectorType& rhs)
        {
            level_ = numLevels()-1;
            LinearIterationStep<MatrixType, VectorType, BitVectorType>::setProblem(mat,x,rhs);

            // Preprocess must be called again, to create the new matrix hierarchy
            preprocessCalled = false;
        }

        void setRhs(const VectorType& rhs)
        {
            level_ = numLevels()-1;
            this->rhs_ = &rhs;
            rhsHierarchy_.back() = rhs;
        }

        template <class DerivedTransferHierarchy>
        void setTransferOperators(const DerivedTransferHierarchy& transfer)
        {
            mgTransfer_.resize(transfer.size());
            for(size_t j=0; j<transfer.size(); ++j)
                mgTransfer_[j] = transfer[j];
        }

        /**
         * \brief Set transfer operator hierarchy from vector of shared_ptr's.
         *
         * Be careful: The Multigrid step does currently not share ownership
         * afterwards. This may change in the future.
         */
        template <class DerivedTransfer>
        void setTransferOperators(const std::vector<typename std::shared_ptr<DerivedTransfer> >& transfer)
        {
            mgTransfer_.resize(transfer.size());
            for(size_t j=0; j<transfer.size(); ++j)
                mgTransfer_[j] = transfer[j].get();
        }

        virtual void iterate();

        virtual void nestedIteration();

        virtual void preprocess();

        virtual void postprocess();

//        virtual VectorType getSol();

//        virtual const MatrixType* getMatrix();

        /** \brief Return total number of levels of the multigrid hierarchy */
        virtual size_t numLevels() const
        {
            return mgTransfer_.size() + 1;
        }

        virtual int level() const {return level_;}

        /** \brief Sets the number of pre- and postsmoothing steps
            and of coarse corrections.
            \param mu Number of coarse corrections
            \param nu1 Number of presmoothing steps
            \param nu2 Number of postsmoothing steps
        */
        virtual void setMGType(int mu, int nu1, int nu2);

        /** \brief Set the smoother iteration step */
        virtual void setSmoother(LinearIterationStep<MatrixType, VectorType>* smoother)
        {
                presmootherDefault_ = postsmootherDefault_ = Dune::stackobject_to_shared_ptr(*smoother);

                levelWiseSmoothers_.clear();
        }

        /** \brief Set the smoother iteration step from a smart pointer*/
        virtual void setSmoother(std::shared_ptr<LinearIterationStep<MatrixType, VectorType> > smoother)
        {
                presmootherDefault_ = postsmootherDefault_ = smoother;

                levelWiseSmoothers_.clear();
        }

        /** \brief Set pre- and post smoothers individually */
        virtual void setSmoother(LinearIterationStep<MatrixType, VectorType>* preSmoother,
                                 LinearIterationStep<MatrixType, VectorType>* postSmoother)
        {
                presmootherDefault_  = Dune::stackobject_to_shared_ptr(*preSmoother);
                postsmootherDefault_ = Dune::stackobject_to_shared_ptr(*postSmoother);

                levelWiseSmoothers_.clear();
        }

        /** \brief Set the smoother iteration step for a particular level */
        virtual void setSmoother(LinearIterationStep<MatrixType, VectorType>* smoother, std::size_t level)
        {
            levelWiseSmoothers_[level] = Dune::stackobject_to_shared_ptr(*smoother);
        }

        /** \brief Set the smoother iteration step for a particular level, from a smart pointer */
        virtual void setSmoother(std::shared_ptr<LinearIterationStep<MatrixType, VectorType> > smoother, std::size_t level)
        {
            levelWiseSmoothers_[level] = smoother;
        }

    protected:
        /** \brief The presmoothers, one for each level */
        std::vector<std::shared_ptr<LinearIterationStep<MatrixType, VectorType> > > presmoother_;

        /** \brief The postsmoothers, one for each level */
        std::vector<std::shared_ptr<LinearIterationStep<MatrixType, VectorType> > > postsmoother_;

    public:
        /** \brief The base solver */
        Solver* basesolver_;

    protected:
        //! Number of presmoothing steps
        int nu1_;

        //! Number of postsmoothing steps
        int nu2_;

        //! Number of coarse corrections
        int mu_;
    public:
        //! Variable used to store the current level
        int level_;

        //! The linear operators on each level
        std::vector<std::shared_ptr<const MatrixType> > matrixHierarchy_;

    protected:
        //! Flags specifying the dirichlet nodes on each level
        std::vector<BitVectorType*> ignoreNodesHierarchy_;

    public:
        /**  \brief! hierarchy of solution/correction vectors.
          *
          *  on the fine level it contains the iterate, whereas on the coarse levels the corresponding corrections
          */
        std::vector<std::shared_ptr<VectorType> > xHierarchy_;

        std::vector<VectorType> rhsHierarchy_;

        //protected:
        std::vector<MultigridTransfer<VectorType, BitVectorType, MatrixType>* > mgTransfer_;

    protected:

        std::shared_ptr<LinearIterationStep<MatrixType, VectorType> > presmootherDefault_;
        std::shared_ptr<LinearIterationStep<MatrixType, VectorType> > postsmootherDefault_;
        typedef std::map<std::size_t, std::shared_ptr<LinearIterationStep<MatrixType, VectorType> > > SmootherCache;
        SmootherCache levelWiseSmoothers_;

        bool preprocessCalled;
    };

#include "multigridstep.cc"

#endif
