// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef LOOP_SOLVER_HH
#define LOOP_SOLVER_HH

#include "iterativesolver.hh"
#include "iterationstep.hh"
#include "norm.hh"

/** \brief A solver which consists of a single loop
  *
  *  This class basically implements a loop that calls an iteration procedure
  *  (which is to be supplied by the user).  It also monitors convergence.
  */
template <class VectorType, class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension> >
class LoopSolver : public IterativeSolver<VectorType, BitVectorType>
{
    typedef typename VectorType::field_type field_type;

    // For complex-valued data
    typedef typename Dune::FieldTraits<field_type>::real_type real_type;

public:

    /** \brief Constructor taking all relevant data */
    LoopSolver(IterationStep<VectorType, BitVectorType>* iterationStep,
               int maxIterations,
               double tolerance,
               const Norm<VectorType>* errorNorm,
               Solver::VerbosityMode verbosity,
               bool useRelativeError=true,
               const VectorType* referenceSolution=0)
        : IterativeSolver<VectorType, BitVectorType>(maxIterations,
                                                     tolerance,
                                                     errorNorm,
                                                     verbosity,
                                                     useRelativeError),
          iterationStep_(iterationStep),
          referenceSolution_(referenceSolution)
    {}

    /**  \brief Checks whether all relevant member variables are set
      *  \exception SolverError if the iteration step is not set up properly
      */
    virtual void check() const;

    virtual void preprocess();

    /**  \brief Loop, call the iteration procedure
      *  and monitor convergence
      */
    virtual void solve();

    //! The iteration step used by the algorithm
    IterationStep<VectorType, BitVectorType>* iterationStep_;

    const VectorType* referenceSolution_;

};

#include "loopsolver.cc"

#endif
