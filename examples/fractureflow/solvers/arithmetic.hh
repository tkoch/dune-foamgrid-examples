// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef ARITHMETIC_HH
#define ARITHMETIC_HH

#include <type_traits>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/diagonalmatrix.hh>
#include <dune/common/unused.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/scaledidmatrix.hh>

/** \brief Namespace containing helper classes and functions for arithmetic operations
 *
 * Everything in this namespace is experimental and might change
 * in the near future. Especially the naming of namespace, structs,
 * and functions is not final.
 */
namespace Arithmetic
{

    // type promotion (smallest matrix that can hold the sum of two matrices *******************
    template<class MatrixA, class MatrixB>
    struct Promote
    {
        typedef Dune::FieldMatrix<typename MatrixA::field_type, MatrixA::rows, MatrixA::cols> Type;
    };

    template<class Matrix>
    struct Promote<Matrix, Matrix>
    {
        typedef Matrix Type;
    };

    template<typename FieldType, int n>
    struct Promote<Dune::FieldMatrix<FieldType,n,n>, Dune::DiagonalMatrix<FieldType,n> >
    {
        typedef Dune::FieldMatrix<FieldType,n,n> Type;
    };

    template<typename FieldType, int n>
    struct Promote<Dune::DiagonalMatrix<FieldType,n>, Dune::FieldMatrix<FieldType,n,n> >
    {
        typedef Dune::FieldMatrix<FieldType,n,n> Type;
    };

    template<typename FieldType, int n>
    struct Promote<Dune::DiagonalMatrix<FieldType,n>, Dune::ScaledIdentityMatrix<FieldType,n> >
    {
        typedef Dune::DiagonalMatrix<FieldType,n> Type;
    };

    template<typename FieldType, int n>
    struct Promote<Dune::ScaledIdentityMatrix<FieldType,n>, Dune::DiagonalMatrix<FieldType,n> >
    {
        typedef Dune::DiagonalMatrix<FieldType,n> Type;
    };

    /** \brief Class to identify scalar types
     *
     * Specialize this class for all types that can be used
     * like scalar quantities.
     */
    template<class T>
    struct ScalarTraits
    {
        enum { isScalar=(std::is_scalar<T>::value and not std::is_pointer<T>::value)};
    };

    template<class T>
    struct ScalarTraits<Dune::FieldVector<T,1> >
    {
        enum { isScalar=true };
    };

    template<class T>
    struct ScalarTraits<Dune::FieldMatrix<T,1,1> >
    {
        enum { isScalar=true };
    };

    template<class T>
    struct ScalarTraits<Dune::DiagonalMatrix<T,1> >
    {
        enum { isScalar=true };
    };

    template<class T>
    struct ScalarTraits<Dune::ScaledIdentityMatrix<T,1> >
    {
        enum { isScalar=true };
    };


    /** \brief Class to identify matrix types and extract information
     *
     * Specialize this class for all types that can be used like a matrix.
     */
    template<class T>
    struct MatrixTraits
    {
        enum { isMatrix=false };
        enum { rows=-1};
        enum { cols=-1};
    };

    template<class T, int n, int m>
    struct MatrixTraits<Dune::FieldMatrix<T,n,m> >
    {
        enum { isMatrix=true };
        enum { rows=n};
        enum { cols=m};
    };

    template<class T, int n>
    struct MatrixTraits<Dune::DiagonalMatrix<T,n> >
    {
        enum { isMatrix=true };
        enum { rows=n};
        enum { cols=n};
    };

    template<class T, int n>
    struct MatrixTraits<Dune::ScaledIdentityMatrix<T,n> >
    {
        enum { isMatrix=true };
        enum { rows=n};
        enum { cols=n};
    };

    template<class T>
    struct MatrixTraits<Dune::BCRSMatrix<T> >
    {
        enum { isMatrix=true };
    };

    namespace detail {
        enum class enabler {};
    }

    template <bool Condition, typename Type=void>
    using enable_if_t = typename std::enable_if<Condition, Type>::type;

    /** \brief Internal helper class for product operations
     *
     */
    template<class A, class B, class C, bool AisScalar, bool BisScalar, bool CisScalar>
    struct ProductHelper
    {
        template <class ADummy=A, enable_if_t<!MatrixTraits<ADummy>::isMatrix,int> SFINAE_Dummy=0>
        static void addProduct(A& a, const B& b, const C& c)
        {
            b.umv(c, a);
        }

        template <class ADummy=A, enable_if_t<MatrixTraits<ADummy>::isMatrix,int> SFINAE_Dummy=0>
        static void addProduct(A& a, const B& b, const C& c)
        {
            typename B::ConstRowIterator bi  = b.begin();
            typename B::ConstRowIterator bEnd = b.end();
            for(; bi!=bEnd; ++bi)
            {
                typename B::ConstColIterator bik = bi->begin();
                typename B::ConstColIterator biEnd= bi->end();
                for(; bik!=biEnd; ++bik)
                {
                    typename C::ConstColIterator ckj =c[bik.index()].begin();
                    typename C::ConstColIterator ckEnd=c[bik.index()].end();
                    for(; ckj!=ckEnd; ++ckj)
                        a[bi.index()][ckj.index()] += (*bik) * (*ckj);
                }
            }
        }
    };

    // Internal helper class for scaled product operations (i.e., b is always a scalar)
    template<class A, class Scalar, class B, class C, bool AisScalar, bool BisScalar, bool CisScalar>
    struct ScaledProductHelper
    {
        template <class ADummy=A, enable_if_t<!MatrixTraits<ADummy>::isMatrix,int> SFINAE_Dummy=0>
        static void addProduct(A& a, const Scalar& scalar, const B& b, const C& c)
        {
            b.usmv(scalar, c, a);
        }

        template <class ADummy=A, enable_if_t<MatrixTraits<ADummy>::isMatrix,int> SFINAE_Dummy=0>
        static void addProduct(A& a, const Scalar& scalar, const B& b, const C& c)
        {
            typename B::ConstRowIterator bi  = b.begin();
            typename B::ConstRowIterator bEnd = b.end();
            for(; bi!=bEnd; ++bi)
            {
                typename B::ConstColIterator bik =b[bi.index()].begin();
                typename B::ConstColIterator biEnd=b[bi.index()].end();
                for(; bik!=biEnd; ++bik)
                {
                    typename C::ConstColIterator ckj =c[bik.index()].begin();
                    typename C::ConstColIterator ckEnd=c[bik.index()].end();
                    for(; ckj!=ckEnd; ++ckj)
                        a[bi.index()][ckj.index()] += scalar * (*bik) * (*ckj);
                }
            }
        }
    };

    template<class T, int n, int m, int k>
    struct ProductHelper<Dune::FieldMatrix<T,n,k>, Dune::FieldMatrix<T,n,m>, Dune::FieldMatrix<T,m,k>, false, false, false>
    {
        typedef Dune::FieldMatrix<T,n,k> A;
        typedef Dune::FieldMatrix<T,n,m> B;
        typedef Dune::FieldMatrix<T,m,k> C;
        static void addProduct(A& a, const B& b, const C& c)
        {
            for (size_t row = 0; row < n; ++row)
            {
                for (size_t col = 0 ; col < k; ++col)
                {
                    for (size_t i = 0; i < m; ++i)
                        a[row][col] += b[row][i]*c[i][col];
                }
            }
        }
    };

    template<class T, int n, int m, int k>
    struct ScaledProductHelper<Dune::FieldMatrix<T,n,k>, T, Dune::FieldMatrix<T,n,m>, Dune::FieldMatrix<T,m,k>, false, false, false>
    {
        typedef Dune::FieldMatrix<T,n,k> A;
        typedef Dune::FieldMatrix<T,n,m> C;
        typedef Dune::FieldMatrix<T,m,k> D;
        static void addProduct(A& a, const T& b, const C& c, const D& d)
        {
            for (size_t row = 0; row < n; ++row) {
                for (size_t col = 0 ; col < k; ++col) {
                    for (size_t i = 0; i < m; ++i)
                        a[row][col] += b * c[row][i]*d[i][col];
                }
            }
        }
    };

    template<class T, int n>
    struct ProductHelper<Dune::DiagonalMatrix<T,n>, Dune::DiagonalMatrix<T,n>, Dune::DiagonalMatrix<T,n>, false, false, false>
    {
        typedef Dune::DiagonalMatrix<T,n> A;
        typedef A B;
        typedef B C;
        static void addProduct(A& a, const B& b, const C& c)
        {
            for (size_t i=0; i<n; ++i)
                a.diagonal(i) += b.diagonal(i)*c.diagonal(i);
        }
    };

    template<class T, int n>
    struct ScaledProductHelper<Dune::DiagonalMatrix<T,n>, T, Dune::DiagonalMatrix<T,n>, Dune::DiagonalMatrix<T,n>, false, false, false>
    {
        typedef Dune::DiagonalMatrix<T,n> A;
        typedef A C;
        typedef C D;
        static void addProduct(A& a, const T& b, const C& c, const D& d)
        {
            for (size_t i=0; i<n; ++i)
                a.diagonal(i) += b * c.diagonal(i)*d.diagonal(i);
        }
    };

    template<class T, int n>
    struct ProductHelper<Dune::FieldMatrix<T,n,n>, Dune::DiagonalMatrix<T,n>, Dune::DiagonalMatrix<T,n>, false, false, false>
    {
        typedef Dune::FieldMatrix<T,n,n> A;
        typedef Dune::DiagonalMatrix<T,n> B;
        typedef B C;
        static void addProduct(A& a, const B& b, const C& c)
        {
            for (size_t i=0; i<n; ++i)
                a[i][i] += b.diagonal(i)*c.diagonal(i);
        }
    };

    template<class T, int n>
    struct ScaledProductHelper<Dune::FieldMatrix<T,n,n>, T, Dune::DiagonalMatrix<T,n>, Dune::DiagonalMatrix<T,n>, false, false, false>
    {
        typedef Dune::FieldMatrix<T,n,n> A;
        typedef Dune::DiagonalMatrix<T,n> C;
        typedef C D;
        static void addProduct(A& a, const T& b, const C& c, const D& d)
        {
            for (size_t i=0; i<n; ++i)
                a[i][i] += b * c.diagonal(i)*d.diagonal(i);
        }
    };

    template<class T, int n, int m>
    struct ProductHelper<Dune::FieldMatrix<T,n,m>, Dune::DiagonalMatrix<T,n>, Dune::FieldMatrix<T,n,m>, false, false, false>
    {
        typedef Dune::FieldMatrix<T,n,m> A;
        typedef Dune::DiagonalMatrix<T,n> B;
        typedef A C;
        static void addProduct(A& a, const B& b, const C& c)
        {
            for (size_t i=0; i<n; ++i)
                a[i].axpy(b.diagonal(i),c[i]);
        }
    };

    template<class T, int n, int m>
    struct ScaledProductHelper<Dune::FieldMatrix<T,n,m>, T, Dune::DiagonalMatrix<T,n>, Dune::FieldMatrix<T,n,m>, false, false, false>
    {
        typedef Dune::FieldMatrix<T,n,m> A;
        typedef Dune::DiagonalMatrix<T,n> C;
        typedef A D;
        static void addProduct(A& a, const T& b, const C& c, const D& d)
        {
            for (size_t i=0; i<n; ++i)
                a[i].axpy(b*c.diagonal(i),d[i]);
        }
    };

    template<class T, int n, int m>
    struct ProductHelper<Dune::FieldMatrix<T,n,m>, Dune::FieldMatrix<T,n,m>, Dune::DiagonalMatrix<T,m>, false, false, false>
    {
        typedef Dune::FieldMatrix<T,n,m> A;
        typedef Dune::DiagonalMatrix<T,m> C;
        typedef A B;

        static void addProduct(A& a, const B& b, const C& c)
        {
            for (size_t i=0; i<n; ++i)
                for (size_t j=0; j<m; j++)
                    a[i][j] += c.diagonal(j)*b[i][j];
        }
    };

    template<class T, int n, int m>
    struct ScaledProductHelper<Dune::FieldMatrix<T,n,m>, T, Dune::FieldMatrix<T,n,m>, Dune::DiagonalMatrix<T,m>, false, false, false>
    {
        typedef Dune::FieldMatrix<T,n,m> A;
        typedef Dune::DiagonalMatrix<T,m> D;
        typedef A C;

        static void addProduct(A& a, const T& b, const C& c, const D& d)
        {
            for (size_t i=0; i<n; ++i)
                for (size_t j=0; j<m; j++)
                    a[i][j] += b*d.diagonal(j)*c[i][j];
        }
    };

    template<class T, int n>
    struct ProductHelper<Dune::ScaledIdentityMatrix<T,n>, Dune::ScaledIdentityMatrix<T,n>, Dune::ScaledIdentityMatrix<T,n>, false, false, false>
    {
        typedef Dune::ScaledIdentityMatrix<T,n> A;
        typedef A B;
        typedef B C;

        static void addProduct(A& a, const B& b, const C& c)
        {
            a.scalar() += b.scalar()*c.scalar();
        }
    };

    template<class T, class Scalar, int n>
    struct ScaledProductHelper<Dune::ScaledIdentityMatrix<T,n>, Scalar, Dune::ScaledIdentityMatrix<T,n>, Dune::ScaledIdentityMatrix<T,n>, false, false, false>
    {
        typedef Dune::ScaledIdentityMatrix<T,n> A;
        typedef A C;
        typedef C D;

        static void addProduct(A& a, const Scalar& b, const C& c, const D& d)
        {
            a.scalar() += b * c.scalar()*d.scalar();
        }
    };

    /** \brief Specialization for b being a scalar type
      */
    template<class A, class ScalarB, class C, bool AisScalar, bool CisScalar>
    struct ProductHelper<A, ScalarB, C, AisScalar, true, CisScalar>
    {
        typedef ScalarB B;

        template <class ADummy=A, enable_if_t<!MatrixTraits<ADummy>::isMatrix,int> SFINAE_Dummy=0>
        static void addProduct(A& a, const B& b, const C& c)
        {
            a.axpy(b, c);
        }

        template <class ADummy=A, enable_if_t<MatrixTraits<ADummy>::isMatrix,int> SFINAE_Dummy=0>
        static void addProduct(A& a, const B& b, const C& c)
        {
            typename C::ConstRowIterator ci  = c.begin();
            typename C::ConstRowIterator cEnd = c.end();
            for(; ci!=cEnd; ++ci)
            {
                typename C::ConstColIterator cik =c[ci.index()].begin();
                typename C::ConstColIterator ciEnd=c[ci.index()].end();
                for(; cik!=ciEnd; ++cik)
                {
                    a[ci.index()][cik.index()] += b*(*cik);
                }
            }
        }
    };

    template<class A, class Scalar, class ScalarB, class C, bool AisScalar, bool CisScalar>
    struct ScaledProductHelper<A, Scalar, ScalarB, C, AisScalar, true, CisScalar>
    {
        typedef ScalarB B;

        template <class ADummy=A, enable_if_t<!MatrixTraits<ADummy>::isMatrix,int> SFINAE_Dummy=0>
        static void addProduct(A& a, const Scalar& scalar, const B& b, const C& c)
        {
            a.axpy(scalar*b, c);
        }

        template <class ADummy=A, enable_if_t<MatrixTraits<ADummy>::isMatrix,int> SFINAE_Dummy=0>
        static void addProduct(A& a, const Scalar& scalar, const B& b, const C& c)
        {
            typename C::ConstRowIterator ci  = c.begin();
            typename C::ConstRowIterator cEnd = c.end();
            for(; ci!=cEnd; ++ci)
            {
                typename C::ConstColIterator cik =c[ci.index()].begin();
                typename C::ConstColIterator ciEnd=c[ci.index()].end();
                for(; cik!=ciEnd; ++cik)
                {
                    a[ci.index()][cik.index()] += scalar*b*(*cik);
                }
            }
        }
    };

    template<class ABlock, class ScalarB, class CBlock>
    struct ProductHelper<Dune::BCRSMatrix<ABlock>, ScalarB, Dune::BCRSMatrix<CBlock>, false, true, false>
    {
        typedef Dune::BCRSMatrix<ABlock> A;
        typedef ScalarB B;
        typedef Dune::BCRSMatrix<CBlock> C;

        static void addProduct(A& a, const B& b, const C& c)
        {
            // Workaround for the fact that Dune::BCRSMatrix assumes
            // its blocks to have an axpy() implementation, yet
            // Dune::DiagonalMatrix does not.

            //a.axpy(b, c);

            for (typename C::ConstIterator i=c.begin(); i!=c.end(); ++i) {
                const size_t iindex = i.index();
                for (typename C::row_type::ConstIterator j=i->begin(); j!=i->end(); ++j)
                    ProductHelper<typename A::block_type, B, typename C::block_type, false, true, false>::addProduct(a[iindex][j.index()],b,*j);
            }
        }
    };

    template<class ABlock, class Scalar, class ScalarB, class CBlock>
    struct ScaledProductHelper<Dune::BCRSMatrix<ABlock>, Scalar, ScalarB, Dune::BCRSMatrix<CBlock>, false, true, false>
    {
        typedef Dune::BCRSMatrix<ABlock> A;
        typedef ScalarB B;
        typedef Dune::BCRSMatrix<CBlock> C;

        static void addProduct(A& a, const Scalar& scalar, const B& b, const C& c)
        {
            // Workaround for the fact that Dune::BCRSMatrix assumes
            // its blocks to have an axpy() implementation, yet
            // Dune::DiagonalMatrix does not.

            //a.axpy(scalar*b, c);

            for (typename C::ConstIterator i=c.begin(); i!=c.end(); ++i) {
                const size_t iindex = i.index();
                for (typename C::row_type::ConstIterator j=i->begin(); j!=i->end(); ++j)
                    ProductHelper<typename A::block_type, B, typename C::block_type, false, true, false>::addProduct(a[iindex][j.index()],scalar*b,*j);
            }
        }
    };

    template<class T, int n, class ScalarB>
    struct ProductHelper<Dune::ScaledIdentityMatrix<T,n>, ScalarB, Dune::ScaledIdentityMatrix<T,n>, false, true, false>
    {
        typedef Dune::ScaledIdentityMatrix<T,n> A;
        typedef ScalarB B;
        typedef Dune::ScaledIdentityMatrix<T,n> C;

        static void addProduct(A& a, const B& b, const C& c)
        {
            a.scalar() += b*c.scalar();
        }
    };

    template<class T, int n, class Scalar, class ScalarB>
    struct ScaledProductHelper<Dune::ScaledIdentityMatrix<T,n>, Scalar, ScalarB, Dune::ScaledIdentityMatrix<T,n>, false, true, false>
    {
        typedef Dune::ScaledIdentityMatrix<T,n> A;
        typedef ScalarB B;
        typedef Dune::ScaledIdentityMatrix<T,n> C;

        static void addProduct(A& a, const Scalar& scalar, const B& b, const C& c)
        {
            a.scalar() += scalar*b*c.scalar();
        }
    };

    template<class A, class B, class C>
    struct ProductHelper<A, B, C, true, true, true>
    {
        static void addProduct(A& a, const B& b, const C& c)
        {
            a += b*c;
        }
    };

    template<class A, class B, class C, class D>
    struct ScaledProductHelper<A, B, C, D, true, true, true>
    {
        static void addProduct(A& a, const B& b, const C& c, const D& d)
        {
            a += b*c*d;
        }
    };

   //! Helper class for computing the cross product
    template <class T, int n>
    struct CrossProductHelper
    {
        static Dune::FieldVector<T,n> crossProduct(const Dune::FieldVector<T,n>& a, const Dune::FieldVector<T,n>& b)
        {
            DUNE_UNUSED_PARAMETER(a);
            DUNE_UNUSED_PARAMETER(b);
            DUNE_THROW(Dune::Exception, "You can only call crossProduct with dim==3");
        }
    };

    //! Specialisation for n=3
    template <class T>
    struct CrossProductHelper<T,3>
    {
        static Dune::FieldVector<T,3> crossProduct(const Dune::FieldVector<T,3>& a, const Dune::FieldVector<T,3>& b)
        {
            Dune::FieldVector<T,3> r;
            r[0] = a[1]*b[2] - a[2]*b[1];
            r[1] = a[2]*b[0] - a[0]*b[2];
            r[2] = a[0]*b[1] - a[1]*b[0];
            return r;
        }
    };

    template<class A>
    struct TransposeHelper;

    template <class MatrixType>
    using Transposed = typename TransposeHelper<MatrixType>::TransposedType;

    template<class A>
    struct TransposeHelper
    {
        typedef A TransposedType;

        static void transpose(const A& a, TransposedType& aT)
        {
            DUNE_UNUSED_PARAMETER(a);
            DUNE_UNUSED_PARAMETER(aT);
            DUNE_THROW(Dune::Exception, "Not implemented for general matrix types!");
        }
    };

    //! Specialization for Dune::FieldMatrix
    template<class T, int n, int m>
    struct TransposeHelper<Dune::FieldMatrix<T,n,m> >
    {
        typedef Dune::FieldMatrix<T,n,m> MatrixType;
        typedef Dune::FieldMatrix<T,m,n> TransposedType;

        static void transpose(const MatrixType& a, TransposedType& aT)
        {
            for (int row = 0; row < m; ++row)
                for (int col = 0 ; col < n; ++col)
                    aT[row][col] = a[col][row];
        }
    };

    template<class T, int n>
    struct TransposeHelper<Dune::DiagonalMatrix<T,n> >
    {
        typedef Dune::DiagonalMatrix<T,n> MatrixType;
        typedef Dune::DiagonalMatrix<T,n> TransposedType;

        static void transpose(const MatrixType& a, TransposedType& aT)
        {
            aT = a;
        }
    };

    template<class T, int n>
    struct TransposeHelper<Dune::ScaledIdentityMatrix<T,n> >
    {
        typedef Dune::ScaledIdentityMatrix<T,n> MatrixType;
        typedef Dune::ScaledIdentityMatrix<T,n> TransposedType;

        static void transpose(const MatrixType& a, TransposedType& aT)
        {
            aT = a;
        }
    };

    //! Specialization for Dune::BCRSMatrix Type
    template<class A>
    struct TransposeHelper<Dune::BCRSMatrix<A> >
    {
        typedef Dune::BCRSMatrix<Transposed<A> > TransposedType;

        static void transpose(const Dune::BCRSMatrix<A>& a, TransposedType& aT)
        {
            Dune::MatrixIndexSet idxSetaT(a.M(),a.N());

            typedef typename Dune::BCRSMatrix<A>::ConstColIterator ColIterator;

            // add indices into transposed matrix
            for (size_t row = 0; row < a.N(); ++row)
            {
                ColIterator col = a[row].begin();
                ColIterator end = a[row].end();

                for ( ; col != end; ++col)
                    idxSetaT.add(col.index(),row);
            }

            idxSetaT.exportIdx(aT);

            for (size_t row = 0; row < a.N(); ++row)
            {
                ColIterator col = a[row].begin();
                ColIterator end = a[row].end();

                for ( ; col != end; ++col)
                    TransposeHelper<A>::transpose(*col, aT[col.index()][row]);
            }
        }
    };

    /** \brief Add a product to some matrix or vector
     *
     * This function computes a+=b*c.
     *
     * This function should tolerate all meaningful
     * combinations of scalars, vectors, and matrices.
     *
     * a,b,c could be matrices with appropriate
     * dimensions. But b can also always be a scalar
     * represented by a 1-dim vector or a 1 by 1 matrix.
     */
    template<class A, class B, class C>
    void addProduct(A& a, const B& b, const C& c)
    {
        ProductHelper<A,B,C,ScalarTraits<A>::isScalar, ScalarTraits<B>::isScalar, ScalarTraits<C>::isScalar>::addProduct(a,b,c);
    }

    /** \brief Subtract a product from some matrix or vector
     *
     * This function computes a-=b*c.
     *
     * This function should tolerate all meaningful
     * combinations of scalars, vectors, and matrices.
     *
     * a,b,c could be matrices with appropriate
     * dimensions. But b can also always be a scalar
     * represented by a 1-dim vector or a 1 by 1 matrix.
     */
    template<class A, class B, class C>
    void subtractProduct(A& a, const B& b, const C& c)
    {
        ScaledProductHelper<A,int,B,C,ScalarTraits<A>::isScalar, ScalarTraits<B>::isScalar, ScalarTraits<C>::isScalar>::addProduct(a,-1,b,c);
    }

    //! Compute the cross product of two vectors. Only works for n==3
    template<class T, int n>
    Dune::FieldVector<T,n> crossProduct(const Dune::FieldVector<T,n>& a, const Dune::FieldVector<T,n>& b)
    {
        return CrossProductHelper<T,n>::crossProduct(a,b);
    }

    //! Compute the transposed of a matrix
    template <class A>
    void transpose(const A& a, Transposed<A>& aT)
    {
        TransposeHelper<A>::transpose(a,aT);
    }

    /** \brief Add a scaled product to some matrix or vector
     *
     * This function computes a+=b*c*d.
     *
     * This function should tolerate all meaningful
     * combinations of scalars, vectors, and matrices.
     *
     * a,c,d could be matrices with appropriate dimensions. But b must
     * (currently) and c can also always be a scalar represented by a
     * 1-dim vector or a 1 by 1 matrix.
     */
    template<class A, class B, class C, class D>
    typename std::enable_if<ScalarTraits<B>::isScalar, void>::type
    addProduct(A& a, const B& b, const C& c, const D& d)
    {
        ScaledProductHelper<A,B,C,D,ScalarTraits<A>::isScalar, ScalarTraits<C>::isScalar, ScalarTraits<D>::isScalar>::addProduct(a,b,c,d);
    }

    /** \brief Subtract a scaled product from some matrix or vector
     *
     * This function computes a-=b*c*d.
     *
     * This function should tolerate all meaningful
     * combinations of scalars, vectors, and matrices.
     *
     * a,c,d could be matrices with appropriate dimensions. But b must
     * (currently) and c can also always be a scalar represented by a
     * 1-dim vector or a 1 by 1 matrix.
     */
    template<class A, class B, class C, class D>
    typename std::enable_if<ScalarTraits<B>::isScalar, void>::type
    subtractProduct(A& a, const B& b, const C& c, const D& d)
    {
        ScaledProductHelper<A,B,C,D,ScalarTraits<A>::isScalar, ScalarTraits<C>::isScalar, ScalarTraits<D>::isScalar>::addProduct(a,-b,c,d);
    }

    /** \brief Internal helper class for Matrix operations
     *
     */
    template<class OperatorType, bool isMatrix>
    struct OperatorHelper
    {
        template <class VectorType, class VectorType2>
        static typename VectorType::field_type
        Axy(const OperatorType &A,
            const VectorType &x,
            const VectorType2 &y)
        {
            VectorType2 tmp(y.size());
            tmp = 0.0;
            addProduct(tmp, A, x);
            return tmp * y;
        }

        template <class VectorType, class VectorType2>
        static typename VectorType::field_type
        bmAxy(const OperatorType &A, const VectorType2 &b,
              const VectorType &x, const VectorType2 &y)
        {
            VectorType2 tmp = b;
            subtractProduct(tmp, A, x);
            return tmp * y;
        }
    };

    template<class MatrixType>
    struct OperatorHelper<MatrixType, true>
    {
        template <class VectorType, class VectorType2>
        static typename VectorType::field_type
        Axy(const MatrixType &A,
            const VectorType &x,
            const VectorType2 &y)
        {
            assert(x.N() == A.M());
            assert(y.N() == A.N());

            typename VectorType::field_type outer = 0.0;
            typename VectorType2::block_type inner;
            typename MatrixType::ConstIterator endi=A.end();
            for (typename MatrixType::ConstIterator i=A.begin(); i!=endi; ++i) {
                inner = 0.0;
                const size_t iindex = i.index();
                typename MatrixType::row_type::ConstIterator endj = i->end();
                for (typename MatrixType::row_type::ConstIterator j=i->begin();
                     j!=endj; ++j)
                    addProduct(inner, *j, x[j.index()]);
                outer += inner * y[iindex];
            }
            return outer;
        }

        template <class VectorType, class VectorType2>
        static typename VectorType::field_type
        bmAxy(const MatrixType &A, const VectorType2 &b,
              const VectorType &x, const VectorType2 &y)
        {
            assert(x.N() == A.M());
            assert(y.N() == A.N());
            assert(y.N() == b.N());

            typename VectorType::field_type outer = 0.0;
            typename VectorType2::block_type inner;
            typename MatrixType::ConstIterator endi=A.end();
            for (typename MatrixType::ConstIterator i=A.begin(); i!=endi; ++i) {
                const size_t iindex = i.index();
                inner = b[iindex];
                typename MatrixType::row_type::ConstIterator endj = i->end();
                for (typename MatrixType::row_type::ConstIterator j=i->begin();
                     j!=endj; ++j)
                    subtractProduct(inner, *j, x[j.index()]);
                outer += inner * y[iindex];
            }
            return outer;
        }
    };

    //! Compute \f$(Ax,y)\f$
    template <class OperatorType, class VectorType, class VectorType2>
    typename VectorType::field_type
    Axy(const OperatorType &A,
        const VectorType &x,
        const VectorType2 &y)
    {
        return OperatorHelper<OperatorType,
                              MatrixTraits<OperatorType>::isMatrix>::Axy(A, x, y);
    }

    //! Compute \f$(b-Ax,y)\f$
    template <class OperatorType, class VectorType, class VectorType2>
    typename VectorType::field_type
    bmAxy(const OperatorType &A,
          const VectorType2 &b,
          const VectorType &x,
          const VectorType2 &y)
    {
        return OperatorHelper<OperatorType,
                              MatrixTraits<OperatorType>::isMatrix>::bmAxy(A, b, x, y);
    }
}

#endif
