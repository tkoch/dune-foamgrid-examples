// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_OBSTACLE_RESTRICTOR_HH
#define DUNE_OBSTACLE_RESTRICTOR_HH

#include <vector>

#include "multigridtransfer.hh"
#include "boxconstraint.hh"

/** \brief Abstract base class for all obstacle restriction operators
 * \todo Do we need the template parameter?
 */
template <class DiscFuncType>
class ObstacleRestrictor
{

    enum {blocksize = DiscFuncType::block_type::dimension};

    typedef typename DiscFuncType::field_type field_type;

public:

    virtual void restrict(const std::vector<BoxConstraint<field_type,blocksize> >& f,
                          std::vector<BoxConstraint<field_type,blocksize> >& t,
                          const Dune::BitSetVector<blocksize>& fHasObstacle,
                          const Dune::BitSetVector<blocksize>& tHasObstacle,
                          const MultigridTransfer<DiscFuncType>& transfer,
                          const Dune::BitSetVector<blocksize>& critical) = 0;

};

#endif
