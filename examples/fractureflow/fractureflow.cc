#include <config.h>

#include <dune/common/parametertree.hh>
#include <dune/common/parametertreeparser.hh>

#include <dune/foamgrid/foamgrid.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include "fufem/prolongboundarypatch.hh"
#include "fufem/p1nodalbasis.hh"
#include "fufem/basisinterpolator.hh"
#include "fufem/operatorassembler.hh"
#include "fufem/laplaceassembler.hh"

#include "solvers/loopsolver.hh"

#include "richards/richardsgsstep.hh"
#include "richards/richardsmmgstep.hh"
#include "richards/richardsparameters.hh"
#include "richards/richardstransforms.hh"
#include "richards/upwindgravityassembler.hh"
#include "richards/richardssolverfactory.hh"
#include "richards/richardspotential.hh"

// The grid dimension
const int dim = 2;
const int dimworld = 3;

using namespace Dune;

struct InitialPressure
  : public VirtualFunction<FieldVector<double, dimworld>, FieldVector<double,1> >
{
  virtual void evaluate(const FieldVector<double, dimworld>& x, FieldVector<double,1> & out) const
  {
    out = (x[1] > 6.499 and (x[0]*x[0]+x[2]*x[2]) <1) ? 3.0 : -10.0;
  }
};

struct DirichletVerticesPredicate
  : public VirtualFunction<FieldVector<double, dimworld>, bool >
{
  virtual void evaluate(const FieldVector<double, dimworld>& x, bool& out) const
  {
    out = (x[1] > 6.499 and (x[0]*x[0]+x[2]*x[2]) <1);
  }
};


int main (int argc, char *argv[]) try
{
  typedef BlockVector<FieldVector<double, 1> > VectorType;
  typedef BCRSMatrix<FieldMatrix<double,1,1> > MatrixType;

  // parse data file
  ParameterTree parameterSet;
  if (argc < 2)
    DUNE_THROW(Exception, "Usage: ./fractureflow <parameter file>");

  ParameterTreeParser::readINITree(argv[1], parameterSet);

  // allow to overwrite individual parameters from the command line
  ParameterTreeParser::readOptions(argc,argv,parameterSet);

  // read solver settings
  const int numLevels        = parameterSet.get<int>("numLevels");
  const double timeStep      = parameterSet.get<double>("timeStep");
  const int numTimeSteps     = parameterSet.get<int>("numTimeSteps");

  std::string resultPath     = parameterSet.get<std::string>("resultPath");

  // read problem settings
  std::string path                = parameterSet.get<std::string>("path");
  std::string gridFile            = parameterSet.get<std::string>("gridFile");
  bool withGravity                = parameterSet.get<bool>("withGravity");
  int gravityAxis                 = parameterSet.get<int>("gravityAxis");
  int gravityDirection            = parameterSet.get<int>("gravityDirection");
  std::string material            = parameterSet.get<std::string>("material");

  std::vector<BitSetVector<1> > dirichletNodes(numLevels);

  // Generate the grid
  typedef FoamGrid<dim,3> GridType;

  std::shared_ptr<GridType> grid;

  std::string ending = ".msh";

  if (std::equal(ending.rbegin(), ending.rend(), gridFile.rbegin()))
    grid = std::shared_ptr<GridType>(GmshReader<GridType>::read(path + gridFile));

  // Read Dirichlet information from input file
  std::vector<BoundaryPatch<GridType::LevelGridView> > dirichletBoundary(numLevels);
  dirichletBoundary[0].setup(grid->levelGridView(0));

  const auto& indexSet = grid->levelGridView(0).indexSet();
  DirichletVerticesPredicate dirichletVerticesPredicate;

  Dune::BitSetVector<1> dirichletVertices(grid->levelGridView(0).size(dim));
  for (const auto& vertex : vertices(grid->levelGridView(0)))
  {
    bool isDirichlet;
    dirichletVerticesPredicate.evaluate(vertex.geometry().corner(0), isDirichlet);
    dirichletVertices[indexSet.index(vertex)] = isDirichlet;
  }

  dirichletBoundary[0].setup(grid->levelGridView(0), dirichletVertices);

  std::cout << "Dirichlet boundary has " << dirichletBoundary[0].numFaces() << " faces." << std::endl;

  for (int i=0; i<numLevels-1; i++)
    grid->globalRefine(1);

  // Prolong Dirichlet bitfield to all levels
  PatchProlongator<GridType>::prolong(dirichletBoundary);

  // Recopy into bitfield
  for (int i=0; i<numLevels; i++)
    dirichletNodes[i] = *dirichletBoundary[i].getVertices();

  int maxlevel = grid->maxLevel();

  //////////////////////////////////////////////////////////////////
  //   Load the soil parameter data base and pick a parameter set
  //////////////////////////////////////////////////////////////////

  ParameterTree materials;
  ParameterTreeParser::readINITree(parameterSet.get<std::string>("materialsFile"), materials);

  RichardsParameters parameters;
  parameters.setup(materials.sub(material), parameterSet.get<bool>("saturation"));
  parameters.L = 1e8;

  materials.sub(material).report();

  ////////////////////////////////////////////////////
  //   Create solution and rhs vectors
  ////////////////////////////////////////////////////

  VectorType rhs(grid->size(maxlevel,dim));
  VectorType p(grid->size(maxlevel,dim));
  VectorType u(grid->size(maxlevel,dim));

  ///////////////////////////////
  //   Assemble the system
  ///////////////////////////////

  typedef P1NodalBasis<GridType::LevelGridView,double> P1Basis;
  P1Basis p1Basis(grid->levelGridView(maxlevel));

  // A global assembler using the p1 basis
  OperatorAssembler<P1Basis,P1Basis> assembler(p1Basis, p1Basis);

  // The local assembler: this contains the actual equations
  LaplaceAssembler<GridType,P1Basis::LocalFiniteElement,P1Basis::LocalFiniteElement> laplaceLocalAssembler;

  // Create an empty stiffness matrix
  MatrixType bilinearForm;

  // Assemble the global stiffness matrix
  assembler.assemble(laplaceLocalAssembler, bilinearForm);

  // Hack: Multiply with the scaling tensor _after_ assembly
  bilinearForm *= parameters.hatK_h * parameters.u_r;

  // Compute h_p, the integrals over the shape functions
  NodalWeights<GridType> nodalWeights;
  nodalWeights.setGrid(*grid);
  nodalWeights.compute();

  //
  FieldVector<double,dimworld> gravityVector(0);
  gravityVector[gravityAxis] = gravityDirection;

  // Assemble the matrix for the upwind gravity term
  UpwindGravityAssembler<GridType,P1Basis::LocalFiniteElement,P1Basis::LocalFiniteElement> upwindGravityLocalAssembler(gravityVector);
  MatrixType upwindGravityMatrix;

  assembler.assemble(upwindGravityLocalAssembler, upwindGravityMatrix);

  typedef Nonlinearity<Dune::FieldVector<double,1>,Dune::FieldMatrix<double,1,1> > NonlinearityType;
  std::shared_ptr<NonlinearityType> nonlinearity
      = shared_ptr<RichardsPotential<double> >(new RichardsPotential<double>(&parameters,
                                                                             nodalWeights.nodalWeights_.back()));

  RichardsSolverFactory<GridType,MatrixType,VectorType> solverFactory(parameterSet,
                                                                      grid.get(),
                                                                      &parameters,
                                                                      &bilinearForm,
                                                                      &u,
                                                                      &rhs,
                                                                      nonlinearity,
                                                                      &dirichletNodes.back(),
                                                                      nullptr,
                                                                      nullptr);
  shared_ptr<Solver> solver = solverFactory.getSolver();

  // Choose an initial value
  InitialPressure initialPressure;
  Functions::interpolate(p1Basis, p, initialPressure);

  for (size_t i=0; i<p.size(); i++)
    u[i] = parameters.physicalToGeneralizedPressure(p[i]);

  VTKWriter<GridType::LeafGridView> vtkWriter(grid->leafGridView());
  vtkWriter.addVertexData(p, "solution");
  vtkWriter.write(resultPath + "func_0");

  //////////////////////////////////////////
  //  Prepare the upwind mapping
  //////////////////////////////////////////

  std::dynamic_pointer_cast<::LoopSolver<VectorType> >(solver)->iterationStep_->preprocess();

  // ////////////////////////////////
  // The time loop
  // ////////////////////////////////
  for (int i=0; i<numTimeSteps; i++)
  {
    // Global time
    std::cout << "Time step: " << i <<",  global time: " << (i*timeStep) << std::endl;

    // Compute the right hand side from the previous solution
    computeRhs(u,
               rhs,
               *nonlinearity,
               parameters,
               upwindGravityMatrix,
               timeStep,
               withGravity);

    // Solve
    solver->solve();

    u = std::dynamic_pointer_cast<::LoopSolver<VectorType> >(solver)->iterationStep_->getSol();

    // Output result
    for (size_t j=0; j<u.size(); j++)
      p[j] = parameters.generalizedToPhysicalPressure(u[j]);

    std::cout.precision(15);
    std::cout.setf(std::ios::fixed,std::ios::floatfield);

    // compute range
    double min = *std::min_element(p.begin(),p.end());
    double max = *std::max_element(p.begin(),p.end());

    std::cout << "Range: ( " << min << ", " << max << " )" << std::endl;

    VTKWriter<GridType::LeafGridView> vtkWriter(grid->leafGridView());
    vtkWriter.addVertexData(p, "solution");
    vtkWriter.write(resultPath + "func_" + std::to_string(i+1));

  }

  return 0;
} catch (Exception e) {
    std::cout << e << "\n";
 }
