#ifndef DUNE_NODAL_WEIGHTS_HH
#define DUNE_NODAL_WEIGHTS_HH

#include <vector>

#include <dune/common/fmatrix.hh>

#include <dune/geometry/quadraturerules.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/bcrsmatrix.hh>

#include "../fufem/boundarypatch.hh"
#include "../fufem/p1nodalbasis.hh"
#include "../fufem/l2functionalassembler.hh"
#include "../fufem/functionalassembler.hh"
#include "../fufem/constantfunction.hh"

    template <class GridType>
    class NodalWeights
    {
        typedef typename GridType::template Codim<0>::LevelIterator ElementIterator;
        typedef typename GridType::template Codim<0>::Entity EntityType;

    public:

        NodalWeights() : grid_(NULL), surfacePatch_(NULL)
        {}

        void setGrid(const GridType& grid) {
            grid_ = &grid;
        }

        void setSurface(const std::vector<BoundaryPatch<typename GridType::LevelGridView> >& surface) {
            surfacePatch_ = &surface;
        }

        void compute() {

            using namespace Dune;
            const int dimworld = GridType::dimensionworld;

            if (!grid_)
                DUNE_THROW(Exception, "You have to supply a grid!");

            nodalWeights_.resize(grid_->maxLevel()+1);

            for (int i=0; i<=grid_->maxLevel(); i++) {

                ConstantFunction<Dune::FieldVector<double,dimworld>, Dune::FieldVector<double,1> > onefunction(Dune::FieldVector<double,1>(1));
                typedef P1NodalBasis<typename GridType::LevelGridView,double> P1Basis;
                P1Basis p1Basis(grid_->levelGridView(i));
                FunctionalAssembler<P1Basis> functionalAssembler(p1Basis);
                L2FunctionalAssembler<GridType,typename P1Basis::LocalFiniteElement> l2functional(onefunction);
                functionalAssembler.assemble(l2functional,nodalWeights_[i]);

            }

        }

    protected:
        const GridType* grid_;

        const std::vector<BoundaryPatch<typename GridType::LevelGridView> >* surfacePatch_;

    public:
        std::vector<Dune::BlockVector<Dune::FieldVector<double,1> > > nodalWeights_;

        std::vector<Dune::BlockVector<Dune::FieldVector<double,1> > > nodalSurfaceWeights_;

    };

#endif
