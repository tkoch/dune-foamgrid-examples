/* Computes the local damping factor.  That is formula (5.5)
 * in Kornhuber: 'On Constrained Newton Linearization and
 * Multigrid for Variational Inequalities'
 */
template<class MatrixType, class VectorType>
inline
typename VectorType::field_type RichardsCoarseGSStep<MatrixType, VectorType>::
computeDampingFactor(const field_type& residual, const field_type& z_l,
                     const field_type& a_ll, const field_type& L_l) const
{
    field_type omega = 2*(fabs(residual) - L_l* (*B_l)* (*B_l));
    omega /= fabs(z_l)*(a_ll + L_l*((*B_l) + fabs(z_l)));

    omega = std::max(omega, (field_type)0.0);
    omega = std::min((field_type)1.0, omega);

    return omega;
}

template<class MatrixType, class VectorType>
void RichardsCoarseGSStep<MatrixType, VectorType>::iterate()
{
    const MatrixType& mat = *this->mat_;
    VectorType& x = *this->x_;

    const int nDof = mat.N();

    int i;

    for (i=0; i<nDof; i++) {

        if ((*this->ignoreNodes_)[i][0])
            continue;

        // compute residual
        Dune::FieldVector<field_type,1> r;
        this->residual(i, r);

        // compute correction
        field_type z = r / mat[i][i];

        // project onto obstacle if the obstacle is violated
        if ( x[i]+z < (*obstacle_)[i].lower(0))
            z = (*obstacle_)[i].lower(0) - x[i];
        else if ( x[i]+z > (*obstacle_)[i].upper(0))
            z = (*obstacle_)[i].upper(0) - x[i];

        if (z!=0) {
            // Compute damping factor
            field_type omega = computeDampingFactor(r, z, mat[i][i], (*lipschitzConstants)[i][0]);

            // Apply correction
            x[i] += omega*z;

            // Update B_l
            *B_l += omega * std::abs(z);

        }

    }

}
