#ifndef DUNE_RICHARDS_PARAMETERS_HH
#define DUNE_RICHARDS_PARAMETERS_HH

#include <cmath>
#include <limits>

#include <dune/common/parametertree.hh>

#include "../solvers/numproc.hh"

#include "kirchhofftransformation.hh"

#if defined CENTER_IN_HATU && defined ELLIPTIC
#error This code path is not tested!
#endif

class RichardsParameters
{
public:

    // Minimal saturation
     double theta_m_;

    // Maximal saturation
     double theta_M_;

    // Porosity
     double hatn;

    // Zeiteinheit
     double tzero;

    // Permeabilität  (m/s)
    double hatK_h;

    // Negative bubbling pressure
    double u_r;

    // Does the model contain saturation?
    bool saturation_;

    // Porengrößenverteilung
    double lambda;

    // critical generalized pressure
    double u_c_;

#ifdef ELLIPTIC
    KirchhoffTransformation<double,true> kirchhoffTransformation_;
#else
    KirchhoffTransformation<double,false> kirchhoffTransformation_;
#endif
    void setup(const Dune::ParameterTree& parameterSet, bool saturation) {

        saturation_ = saturation;
        
        if (saturation_) {
            theta_m_ = parameterSet.get<double>("theta_m");
            theta_M_ = parameterSet.get<double>("theta_M");
        } else {
            theta_m_ = theta_M_ = 0;
        }

        hatn = parameterSet.get<double>("hatn");
        tzero = parameterSet.get<double>("tzero");
        hatK_h = parameterSet.get<double>("hatK_h");
        u_r    = parameterSet.get<double>("u_r");

        lambda = parameterSet.get<double>("lambda");
        u_c_ = -lambda*elambda() / (lambda*elambda() - 1);

        L = 1e8;
        
        kirchhoffTransformation_.setup(parameterSet);
    }

    // The exponent of the Brooks-Corey function mapping the saturation
    // to the relative permeability
    double elambda() const {
        return 3 + 2/lambda;
    }

public:

    double generalizedPhaseBoundary() const
    {
#ifdef CENTER_IN_HATU
        return -1-u_c;
#else
        return -1;
#endif
    }

    //! If a dof is in the saturated phase at a value where $\Phi'''$ is
    //! larger than this L we consider it critical, and don't apply a coarse
    //! grid correction to it.
    double L;

    double hatu_L() const
    {
        if (saturation_) {
            double result = L / ((theta_M_ - theta_m_) * lambda * (lambda * (elambda() - 1) - 1));
            result = std::pow(result, (lambda*elambda()-1) / (lambda*(1-2*elambda()) + 2));
#ifndef CENTER_IN_HATU
            result -= lambda*elambda();
#endif
            result /= lambda*elambda()-1;


            return result;
        } else
            return -std::numeric_limits<double>::max();

    }

    
    /** \brief One of the Brooks-Corey functions: Saturation as a function of physical pressure */
    double soilWaterRetention(double p) const
    {
#ifdef CENTER_IN_HATU
        if (p <= u_r * (1+u_c))
#else
        if (p <= u_r)
#endif
            return theta_M_;
        
        return theta_m_ + (theta_M_ - theta_m_) * std::pow(p/u_r, -lambda);
    }

    double M(double u) const
    {
        if (saturation_) {
#ifndef ELLIPTIC
            // hatu must be larger or equal to u_c, if it is not then there either
            // is a programming error or a problem with the finite precision arithmetic.
            // We test whether it is _much_ smaller.  If it is we abort, if not we
            // tacitly set hatu = u_c.
#ifdef CENTER_IN_HATU
            if ( u  < -1e-4)
#else
            if ( ((u - u_c_) / std::abs(u_c_)) < -1e-4)
#endif
                DUNE_THROW(SolverError, "Function M is evaluated at " << u << ", where it isn't defined!");
#endif

#ifdef CENTER_IN_HATU
            if (u < 0)
                u = 0;
#else
            if (u < u_c_)
                u = u_c_;
#endif
            
            // Actually compute the function value
            return soilWaterRetention(-generalizedToPhysicalPressure(u));

        } else
            return 0;

    }
    
    double MPrime(double u) const
    {
        if (saturation_) {
#ifndef ELLIPTIC
#ifdef CENTER_IN_HATU
            if ( u >= -1-u_c )
#else
            if ( u >= -1 )
#endif
                return 0;
            
            // hatu must be larger or equal to u_c, if it is not then there either
            // is a programming error or a problem with the finite precision arithmetic.
            // We test whether it is _much_ smaller.  If it is we abort, if not we
            // tacitly set hatu = u_c.
#ifdef CENTER_IN_HATU
            if ( u  < -1e-4)
#else
            if ( ((u - u_c_) / std::abs(u_c_)) < -1e-4)
#endif
                DUNE_THROW(SolverError, "Function MDer is evaluated at " << u << ", where it isn't defined!");
#endif  // #ifndef ELLIPTIC
                
#ifdef CENTER_IN_HATU
            if (u < 0)
                u = 0;
#else
            if (u < u_c_)
                u = u_c_;
#endif

            // Actually compute the function value
#ifdef CENTER_IN_HATU
            double result = (lambda*elambda() - 1) * u;
#else
            double result = (lambda*elambda() - 1) * u + lambda*elambda();
#endif
            // for safety: may be negative due to rounding errors
            if  (result<=0)
                return std::numeric_limits<double>::max();
            result = std::pow(result, (lambda / (lambda*elambda() - 1)) -1);
            result *= (theta_M_ - theta_m_)*lambda;
            
            return result;

        } else
            return 0;

    }

    double MPrimePrime(double u) const
    {
#ifdef CENTER_IN_HATU
        double result = (lambda*elambda()-1)*u;
#else
        double result = (lambda*elambda()-1)*u + lambda*elambda();
#endif
        result = std::pow(result, (lambda*(1-2*elambda())+2) / (lambda*elambda() -1));
        result *= (theta_M_-theta_m_) * lambda * (lambda*(1-elambda()) -1);
        return result;
    }

    
    // relative Permeabilität als Funktion des verallgemeinerten Drucks
    double krH(double u) const {
#ifdef CENTER_IN_HATU
        if (u <= -1-u_c) {
            double result = (lambda*elambda() - 1) * u;
            // for safety: may be negative due to rounding errors
            result = std::max(0.0, result);
            return std::pow(result, (lambda*elambda() / (lambda*elambda() - 1)));
        } else
            return 1;
#else
        if (u <= -1) {
            double result = (lambda*elambda() - 1) * u + lambda*elambda();
            // for safety: may be negative due to rounding errors
            result = std::max(0.0, result);
            return std::pow(result, (lambda*elambda() / (lambda*elambda() - 1)));
        } else
            return 1;
#endif
    }

    // relative Permeabilität als Funktion der Sättigung
    double krTheta(double theta) const {
        return std::pow( (theta - theta_m_) / (theta_M_ - theta_m_), elambda() );
    }

    // Physikalischer Druck als Funktion der Sättigung
    double saturationToPhysicalPressure(double theta) {
        // p = -u_r * [(theta_0-theta_m)/(theta_M-theta_m)]^(-1/lambda) 
        return -1 * u_r * std::pow( (theta - theta_m_) / (theta_M_ - theta_m_), -1/lambda);
    }

    
    double physicalToGeneralizedPressure(const double& p) const
    {
        return kirchhoffTransformation_.evaluate(p / u_r);
    }
    
    double generalizedToPhysicalPressure(const double& u) const
    {
        return kirchhoffTransformation_.inverse(u) * u_r;
    }

};

#endif
