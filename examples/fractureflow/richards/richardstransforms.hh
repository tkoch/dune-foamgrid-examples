#ifndef RICHARDS_TRANSFORMS_HH
#define RICHARDS_TRANSFORMS_HH

#include <dune/istl/bcrsmatrix.hh>

#include "../tnnmg/nonlinearity.hh"

#include "nodalweights.hh"
#include "richardsparameters.hh"

template <class VectorType>
void computeRhs(const VectorType& formerSolution,
                VectorType& rhs,
                const Dune::BlockVector<Dune::FieldVector<double,1> >& h_p,
                const RichardsParameters& parameters,
                const Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >& upwindMatrix,
                double timeStepSize,
                bool withGravity)
{
    /** Discretization of
        \f[ \int_{\Omega_i} \frac {n}{t_0} H_i(u_{i,n}) v_i dx \f]
    */
    for (size_t i=0; i<rhs.size(); i++)
        rhs[i] = (parameters.hatn / parameters.tzero) * parameters.M(formerSolution[i]) * h_p[i];

    /** Convection/gravity:
        Discretization of \f \int_\Omega_i K_h z_0^-1 kr_i(H_i(u_{i,n])) \partial_z \lambda_p dx
    */
    if (withGravity) {

        for (size_t i=0; i<rhs.size(); i++) {

            typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >::row_type::ConstIterator ColumnIterator;
            ColumnIterator cIt    = upwindMatrix[i].begin();
            ColumnIterator cEndIt = upwindMatrix[i].end();

            for (; cIt!=cEndIt; ++cIt) {

                int q = cIt.index();
                rhs[i][0] += timeStepSize * parameters.hatK_h * parameters.krH(formerSolution[q]) * (*cIt)[0][0];

            }

        }

    }

}


template <class VectorType>
void computeRhs(const VectorType& formerSolution,
                VectorType& rhs,
                const Nonlinearity<Dune::FieldVector<double,1>,Dune::FieldMatrix<double,1,1> >& nonlinearity,
                const RichardsParameters& parameters,
                const Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >& upwindMatrix,
                double timeStepSize,
                bool withGravity)
{
    /** Discretization of
        \f[ \int_{\Omega_i} \frac {n}{t_0} H_i(u_{i,n}) v_i dx \f]
    */
    std::fill(rhs.begin(), rhs.end(), 0);
    nonlinearity.addGradient(formerSolution, rhs);

    /** Convection/gravity:
        Discretization of \f \int_\Omega_i K_h z_0^-1 kr_i(H_i(u_{i,n])) \partial_z \lambda_p dx
    */
    if (withGravity) {

        for (size_t i=0; i<rhs.size(); i++) {

            typedef typename Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >::row_type::ConstIterator ColumnIterator;
            ColumnIterator cIt    = upwindMatrix[i].begin();
            ColumnIterator cEndIt = upwindMatrix[i].end();

            for (; cIt!=cEndIt; ++cIt) {

                int q = cIt.index();
                rhs[i][0] += timeStepSize * parameters.hatK_h * parameters.krH(formerSolution[q]) * (*cIt)[0][0];

            }

        }

    }

}


#endif
