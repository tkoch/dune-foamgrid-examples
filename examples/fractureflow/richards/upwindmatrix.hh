#ifndef UPWIND_MATRIX_HH
#define UPWIND_MATRIX_HH

#warning This file is deprecated.  Please use upwindgravityassembler.hh instead!

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/matrix.hh>

#include <dune/grid/common/quadraturerules.hh>
//#include <dune/disc/shapefunctions/lagrangeshapefunctions.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>

template <class GridType, int polOrd>
class UpwindMatrix {
    
    //! The grid
    enum {dim = GridType::dimension};
    
    typedef typename GridType::template Codim<0>::Entity EntityType;
    typedef typename GridType::template Codim<0>::LevelIterator ElementIterator;
    
    //!
    typedef Dune::FieldMatrix<double, 1, 1> MatrixBlock;
    
public:
    
    Dune::BCRSMatrix<MatrixBlock>* matrix_;
    
    const GridType* grid_; 
    
    Dune::FieldVector<double,GridType::dimensionworld> gravityDirection_;
    
    /** \brief Default constructor */
    UpwindMatrix()
    : grid_(NULL)
    {}
    
    //! ???
    UpwindMatrix(const GridType &grid, 
                 Dune::FieldVector<double,GridType::dimensionworld>& gravityDirection) : 
        grid_(&grid), gravityDirection_(gravityDirection)
    { }
    
    void setup(const GridType &grid, 
               Dune::FieldVector<double,GridType::dimensionworld>& gravityDirection)
    {
        grid_ = &grid;
        gravityDirection_ = gravityDirection;
    }
    
    //! Returns the actual matrix if it is assembled
    const Dune::BCRSMatrix<MatrixBlock>* getMatrix() const {
        return this->matrix_;
    }
    
    /** \todo Generalize this to higher-order spaces */
    void getNeighborsPerVertex(Dune::MatrixIndexSet& nb) {
        
        int n = grid_->size(grid_->maxLevel(),dim);
        const typename GridType::Traits::LevelIndexSet& indexSet = grid_->levelIndexSet(grid_->maxLevel());

        nb.resize(n, n);
        
        ElementIterator it = grid_->template lbegin<0>( grid_->maxLevel() );
        ElementIterator endit = grid_->template lend<0> ( grid_->maxLevel() );
        
        for (; it!=endit; ++it) {
            
            for (int i=0; i<it->geometry().corners(); i++) {
                
                for (int j=0; j<it->geometry().corners(); j++) {
                    
                    int iIdx = indexSet.subIndex(*it,i,dim);
                    int jIdx = indexSet.subIndex(*it,j,dim);
                    
                    nb.add(iIdx, jIdx);
                    
                }
                
            }
            
        }
        
    }
    
    void assembleMatrix() {

        using namespace Dune;

        const typename GridType::Traits::LevelIndexSet& indexSet = grid_->levelIndexSet(grid_->maxLevel());
        
        MatrixIndexSet neighborsPerVertex;
        getNeighborsPerVertex(neighborsPerVertex);
        
        matrix_ = new BCRSMatrix<MatrixBlock>;
        
        neighborsPerVertex.exportIdx(*matrix_);
        (*matrix_) = 0;
        
        ElementIterator it = grid_->template lbegin<0>( grid_->maxLevel() );
        ElementIterator endit = grid_->template lend<0> ( grid_->maxLevel() );
        
        Matrix<MatrixBlock> mat;
        
        for( ; it != endit; ++it ) {
            
            const int numOfBaseFct =it->geometry().corners();
            
            mat.setSize(numOfBaseFct, numOfBaseFct);
            // setup matrix 
            getLocalMatrix( *it, numOfBaseFct, mat);
            
            for(int i=0; i<numOfBaseFct; i++) { 
                
                int row = indexSet.subIndex(*it,i,dim);
                for (int j=0; j<numOfBaseFct; j++ ) {
                    
                    int col = indexSet.subIndex(*it,j,dim);
                    (*matrix_)[row][col] += mat[i][j];
                    
                }
            }
        }
        
    }
    
    //! ???
    template < class  EntityType, class MatrixType>
    void getLocalMatrix( EntityType &entity, const int matSize, MatrixType& mat) const 
    {
        using namespace Dune;

        
        typedef Dune::PQkLocalFiniteElementCache<double, double, dim, 1> FiniteElementCache;
        typedef typename FiniteElementCache::FiniteElementType LocalFiniteElement;

        FiniteElementCache finiteElementCache;
        

//         const typename LagrangeShapeFunctionSetContainer<double,double,dim>::value_type& baseSet
//             = LagrangeShapeFunctions<double,double,dim>::general(entity.geometry().type(),1);
        const LocalFiniteElement& localFiniteElement = finiteElementCache.get(entity.type());
        
        // Clear local scalar matrix
        mat = 0;
        
        std::vector<FieldMatrix<double,1,dim> > mygrad;

        // Get quadrature rule
        const QuadratureRule<double, dim>& quad = QuadratureRules<double, dim>::rule(entity.geometry().type(), polOrd);
        
        for (unsigned int pt=0; pt < quad.size(); pt++ ) {
            
            // Get position of the quadrature point
            const FieldVector<double,dim>& quadPos = quad[pt].position();
            
            const FieldMatrix<double,dim,dim>& inv = entity.geometry().jacobianInverseTransposed(quadPos);

            // The factor in the integral transformation formula
            const double integrationElement = entity.geometry().integrationElement(quadPos);
            
            double directionalDerivative[matSize];

            // Compute partial derivative of the base function in gravity direction
            localFiniteElement.localBasis().evaluateJacobian(quadPos,mygrad);

            for(int i=0; i<matSize; i++) {
      
                //baseSet.jacobian(i, quadPos, mygrad[i]);
//                 for (int l=0; l<dim; l++)
//                     mygrad[i][l] = baseSet[i].evaluateDerivative(0,l,quadPos);
                
                // multiply with jacobian inverse 
                FieldVector<double,dim> tmp(0);
                inv.umv(mygrad[i][0], tmp);
                mygrad[i][0] = tmp;
            
                directionalDerivative[i] = mygrad[i][0] * gravityDirection_;
            }

            // Compute diameter of the element
            double diameter = 0;
            for (int i=0; i<entity.geometry().corners(); i++)
                for (int j=0; j<i; j++)
                    diameter = std::max(diameter, (entity.geometry().corner(i)-entity.geometry().corner(j)).two_norm());

            // Evaluate base functions
            std::vector<FieldVector<double,1> > v;
            localFiniteElement.localBasis().evaluateFunction(quadPos,v);
            
            for(int i=0; i<matSize; i++) 
                for (int j=0; j<matSize; j++ ) {
                    mat[i][j][0][0] += (directionalDerivative[i] * v[j]) * quad[pt].weight() * integrationElement;
                    //mat[i][j][0][0] -= 2*diameter * ( directionalDerivative[i] * directionalDerivative[j] ) * quad[pt].weight() * integrationElement;
                    mat[i][j][0][0] -= (diameter/std::sqrt(2)/3) * ( directionalDerivative[i] * directionalDerivative[j] ) * quad[pt].weight() * integrationElement;
                }

        }
        

//         printf("element matrix:\n");
//         std::cout << mat << std::endl;
//         exit(0);
    }
    
    
}; // end class


#endif
