#ifndef RICHARDS_SOLVER_FACTORY_HH
#define RICHARDS_SOLVER_FACTORY_HH

#include <dune/common/parametertree.hh>

#include "../solvers/truncatedblockgsstep.hh"
#include "../solvers/energynorm.hh"

#include "richardsmmgstep.hh"

template <class GridType, class MatrixType, class VectorType>
class RichardsSolverFactory
{

    enum {dim = GridType::dimension};

    typedef Nonlinearity<Dune::FieldVector<double,1>,Dune::FieldMatrix<double,1,1> > NonlinearityType;

public:

    RichardsSolverFactory(const Dune::ParameterTree& parameters,
                          const GridType* grid,
                          const RichardsParameters* richardsParameters,
                          const MatrixType* stiffnessMatrix,
                          VectorType* u,
                          const VectorType* rhs,
                          const Dune::shared_ptr<NonlinearityType>& nonlinearity,
                          const Dune::BitSetVector<1>* dirichletNodes,
                          const Dune::BitSetVector<1>* robinNodes,
                          const Dune::BitSetVector<1>* signoriniNodes)
    : parameters_(parameters),
      grid_(grid),
      richardsParameters_(richardsParameters),
      stiffnessMatrix_(Dune::stackobject_to_shared_ptr(*stiffnessMatrix)),
      u_(u),
      rhs_(rhs),
      nonlinearity_(nonlinearity),
      dirichletNodes_(dirichletNodes),
      robinNodes_(robinNodes),
      signoriniNodes_(signoriniNodes)
      {}

    std::shared_ptr<Solver> getSolver() const;

protected:

    Dune::ParameterTree parameters_;

    const GridType* grid_;

    const RichardsParameters* richardsParameters_;

    std::shared_ptr<const MatrixType> stiffnessMatrix_;

    VectorType* u_;

    const VectorType* rhs_;

    const Dune::shared_ptr<NonlinearityType> nonlinearity_;

    const Dune::BitSetVector<1>* dirichletNodes_;

    const Dune::BitSetVector<1>* robinNodes_;

    const Dune::BitSetVector<1>* signoriniNodes_;

};


template <class GridType, class MatrixType, class VectorType>Dune::shared_ptr<Solver>
RichardsSolverFactory<GridType,MatrixType,VectorType>::getSolver() const
{
    const double timeStep      = parameters_.get("timeStep", 1.0);
    const double baseTolerance = parameters_.get<double>("baseTolerance");
    const int nu1              = parameters_.get<int>("nu1");
    const int nu2              = parameters_.get<int>("nu2");
    const int mu               = parameters_.get<int>("mu");
    const int baseIt           = parameters_.get<int>("baseIt");
    const int numLevels        = parameters_.get<int>("numLevels");
    const int numIt            = parameters_.get<int>("numIt");
    const double tolerance     = parameters_.get<double>("tolerance");

    // Compute h_p, the integrals over the shape functions
    NodalWeights<GridType>* nodalWeights = new NodalWeights<GridType>;
    nodalWeights->setGrid(*grid_);
    nodalWeights->compute();

    // //////////////////////////////////////////////////////////////////
    //   Build the constraint Newton solver
    // //////////////////////////////////////////////////////////////////

    IterationStep<VectorType>* multigridStep;
    EnergyNorm<MatrixType, VectorType>* energyNorm;

    typedef RichardsMultigridStep<MatrixType, VectorType> MultiGridType;
    multigridStep = new MultiGridType(*stiffnessMatrix_, *u_, *const_cast<VectorType*>(rhs_), nonlinearity_, numLevels);
    dynamic_cast<MultiGridType*>(multigridStep)->setMGType(mu, nu1, nu2);
    dynamic_cast<MultiGridType*>(multigridStep)->parameters_ = richardsParameters_;
    multigridStep->ignoreNodes_ = dirichletNodes_;
    dynamic_cast<MultiGridType*>(multigridStep)->h_p_            = &nodalWeights->nodalWeights_;
    dynamic_cast<MultiGridType*>(multigridStep)->h_Gamma_p_      = &nodalWeights->nodalSurfaceWeights_.back();
    dynamic_cast<MultiGridType*>(multigridStep)->robinNodes_     = robinNodes_;
    if (robinNodes_)
        dynamic_cast<MultiGridType*>(multigridStep)->setRobinDamping(parameters_.get<double>("robinDamping"));
    dynamic_cast<MultiGridType*>(multigridStep)->signoriniNodes_ = signoriniNodes_;
    dynamic_cast<MultiGridType*>(multigridStep)->timestep_       = timeStep;

    // create base solver
    LinearIterationStep<MatrixType, VectorType>* basesolverStep;
    basesolverStep = new RichardsCoarseGSStep<MatrixType, VectorType>;

    EnergyNorm<MatrixType, VectorType>* baseEnergyNorm = new EnergyNorm<MatrixType, VectorType>(*basesolverStep);

    LoopSolver<VectorType>* basesolver = new LoopSolver<VectorType>(basesolverStep,
                                                                    baseIt,
                                                                    baseTolerance,
                                                                    baseEnergyNorm,
                                                                    Solver::QUIET);

    dynamic_cast<MultiGridType*>(multigridStep)->basesolver_ = basesolver;

    dynamic_cast<MultiGridType*>(multigridStep)->mgTransfer_.resize(numLevels-1);

    for (size_t i=0; i<dynamic_cast<MultiGridType*>(multigridStep)->mgTransfer_.size(); i++) {
        CompressedMultigridTransfer<VectorType>* newTransferOp = new CompressedMultigridTransfer<VectorType>;
        newTransferOp->setup(*grid_, i, i+1);
        dynamic_cast<MultiGridType*>(multigridStep)->mgTransfer_[i] = newTransferOp;
    }

    energyNorm = new EnergyNorm<MatrixType, VectorType>(*dynamic_cast<MultiGridType*>(multigridStep));

    // get solver verbosity from the parameters
    NumProc::VerbosityMode solverVerbosity;

    if (parameters_.get<std::string>("solverVerbosity") == "quiet")
        solverVerbosity = Solver::QUIET;
    else if (parameters_.get<std::string>("solverVerbosity") == "reduced")
        solverVerbosity = Solver::REDUCED;
    else if (parameters_.get<std::string>("solverVerbosity") == "full")
        solverVerbosity = Solver::FULL;
    else
        DUNE_THROW(SolverError, "No solverVerbosity given");

    return Dune::shared_ptr<LoopSolver<VectorType> > (new LoopSolver<VectorType>(multigridStep,
                                                                                 numIt,
                                                                                 tolerance,
                                                                                 energyNorm,
                                                                                 solverVerbosity));

}

#endif