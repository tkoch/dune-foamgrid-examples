#include <dune/istl/io.hh>
#include <dune/istl/bdmatrix.hh>

#include "../solvers/compressedmultigridtransfer.hh"

template <class MatrixType, class VectorType>
VectorType RichardsMultigridStep<MatrixType, VectorType>::
getSol()
{
    return *u;
}

template <class MatrixType, class VectorType>
std::string RichardsMultigridStep<MatrixType, VectorType>::
getOutput() const
{
    std::string s = outStream_.str();
    outStream_.str("");
    return s;
}

template <class MatrixType, class VectorType>
void RichardsMultigridStep<MatrixType, VectorType>::
preprocess()
{
    int maxlevel = this->numLevels()-1;
    this->level_ = maxlevel;

    // //////////////////////////////////////////////////////////
    //   Check that transfer operators have been supplied
    // //////////////////////////////////////////////////////////
    for (size_t i=0; i<this->mgTransfer_.size(); i++) {

        if (this->mgTransfer_[i] == NULL)
            DUNE_THROW(SolverError, "No transfer operator on level " << i << "!");

    }

    // //////////////////////////////////////////////////////////
    //   Allocate space for the obstacles
    // //////////////////////////////////////////////////////////

    obstacle_.resize(maxlevel+1);
    lipschitzConstants_.resize(maxlevel+1);

    for (int i=0; i<maxlevel; i++) {

        int nDofs = dynamic_cast<CompressedMultigridTransfer<VectorType>*>(this->mgTransfer_[i])->getMatrix().M();

        obstacle_[i].resize(nDofs);
        if (this->xHierarchy_[i])
            this->xHierarchy_[i]->resize(nDofs);
        else
            this->xHierarchy_[i] = Dune::make_shared<VectorType>(nDofs);
        this->rhsHierarchy_[i].resize(nDofs);
        lipschitzConstants_[i].resize(nDofs);
    }

    int topDofs = u->size();
    obstacle_[maxlevel].resize(topDofs);
    if (this->xHierarchy_[maxlevel])
        this->xHierarchy_[maxlevel]->resize(topDofs);
    else
        this->xHierarchy_[maxlevel] = Dune::make_shared<VectorType>(topDofs);

    this->rhsHierarchy_[maxlevel].resize(topDofs);
    lipschitzConstants_[maxlevel].resize(topDofs);

    // The matrices for the linear correction problems
    /** \todo We don't really need a copy of bilinearForm but only its occupation structure */
    this->matrixHierarchy_.back() = Dune::shared_ptr<MatrixType>(new MatrixType(*bilinearForm));

    for (int i=this->numLevels()-2; i>=0; i--) {

        int nDofs = dynamic_cast<CompressedMultigridTransfer<VectorType>*>(this->mgTransfer_[i])->getMatrix().M();

        this->matrixHierarchy_[i] = Dune::shared_ptr<MatrixType>(new MatrixType(nDofs, nDofs, MatrixType::row_wise));

        // Compute which entries are present in the (sparse) coarse grid stiffness
        // matrices.  Even though the matrices change during computation (due to
        // truncation) the occupation patterns remain the same and can be precomputed
        this->mgTransfer_[i]->galerkinRestrictSetOccupation(*this->matrixHierarchy_[i+1], *std::const_pointer_cast<MatrixType>(this->matrixHierarchy_[i]));
    }

    // /////////////////////////////////////////////////////
    //   Setup dirichlet bitfields
    // /////////////////////////////////////////////////////
    for (int i=0; i<((int)this->ignoreNodesHierarchy_.size())-1; i++)
        delete(this->ignoreNodesHierarchy_[i]);

    if (this->ignoreNodes_!=0)
    {
        this->ignoreNodesHierarchy_[this->numLevels()-1] = const_cast<Dune::BitSetVector<1>*>(this->ignoreNodes_);
        for (int i=this->numLevels()-2; i>=0; --i)
        {
            this->ignoreNodesHierarchy_[i] = new Dune::BitSetVector<1>();
            this->mgTransfer_[i]->restrict(*this->ignoreNodesHierarchy_[i+1], *this->ignoreNodesHierarchy_[i]);
        }
    } else
        DUNE_THROW(SolverError, "We need a set of nodes to ignore");

    // If the fineGridDirichletField is not given then it becomes the ignoreNodes field
    if (fineGridDirichlet_.size() != this->ignoreNodes_->size())
        fineGridDirichlet_ = *this->ignoreNodes_;
    else
        for (size_t i=0; i<this->ignoreNodes_->size(); i++)
            fineGridDirichlet_[i] = (fineGridDirichlet_[i][0] || (*this->ignoreNodes_)[i][0]);

    // Set up fine grid presmoother
    nonlinearPreSmoother_.setProblem(*bilinearForm, *u, *l);
    nonlinearPreSmoother_.ignoreNodes_    = &fineGridDirichlet_;
    nonlinearPreSmoother_.signoriniNodes_ = this->signoriniNodes_;
    nonlinearPreSmoother_.parameters_     = parameters_;
    nonlinearPreSmoother_.timestep_       = timestep_;

    // Set up fine grid postsmoother
    nonlinearPostSmoother_.setProblem(*bilinearForm, *u, *l);
    nonlinearPostSmoother_.ignoreNodes_       = &fineGridDirichlet_;
    nonlinearPostSmoother_.signoriniNodes_    = this->signoriniNodes_;
    nonlinearPostSmoother_.parameters_     = parameters_;
    nonlinearPostSmoother_.timestep_       = timestep_;

    // ////////////////////////////////////
    //   Set the coarse grid smoothers
    // ////////////////////////////////////

    this->setSmoother(std::make_shared<RichardsCoarseGSStep<MatrixType,VectorType> >());

    // ////////////////////////////////////
    //   Set up base solver
    // ////////////////////////////////////

    LoopSolver<VectorType>* iterativeBaseSolver = dynamic_cast<LoopSolver<VectorType>* > (this->basesolver_);
    if (!iterativeBaseSolver)
        DUNE_THROW(SolverError, "Base solver of RichardsMMGStep must be a loop solver!");

    LinearIterationStep<MatrixType,VectorType>* linearCoarseIterationStep = dynamic_cast<LinearIterationStep<MatrixType,VectorType>*>(iterativeBaseSolver->iterationStep_);
    if (!linearCoarseIterationStep)
        DUNE_THROW(SolverError, "Iteration step of the RichardsMMGStep coarse grid solver must be a LinearIterationStep!");
    linearCoarseIterationStep->setProblem(*(this->matrixHierarchy_[0]), *this->xHierarchy_[0], this->rhsHierarchy_[0]);

    CanIgnore<Dune::BitSetVector<1> >* canIgnore = dynamic_cast<CanIgnore<Dune::BitSetVector<1> >*>(iterativeBaseSolver->iterationStep_);
    if (canIgnore)
        canIgnore->ignoreNodes_ = this->ignoreNodesHierarchy_[0];
    else
        DUNE_THROW(SolverError, "RichardsMMGStep needs a base solver that can ignore nodes!");

    typedef RichardsCoarseGSStep<MatrixType, VectorType> RichardsCoarseIteratorType;
    RichardsCoarseIteratorType* richardsCoarseIterator = dynamic_cast<RichardsCoarseIteratorType*>(iterativeBaseSolver->iterationStep_);

    if (richardsCoarseIterator) {

        richardsCoarseIterator->obstacle_ = &obstacle_[0];
        richardsCoarseIterator->lipschitzConstants = &lipschitzConstants_[0];
        richardsCoarseIterator->B_l = &B_l_;

    }

    this->basesolver_->preprocess();

}



template<class MatrixType, class VectorType>
void RichardsMultigridStep<MatrixType, VectorType>::nestedIteration()
{
    // ////////////////////////////////////////////////////////////////////////////
    //   Create a temporary hierarchy of matrices and right-hand-side vectors
    // ////////////////////////////////////////////////////////////////////////////

    std::vector<Dune::shared_ptr<MatrixType> > bilinearFormsHierarchy(this->numLevels());
    std::vector<VectorType*> rightHandSidesHierarchy(this->numLevels());
    // also restrict the inital iterate, because it contains the Dirichlet values
    std::vector<VectorType>  iteratesHierarchy(this->numLevels());

    for (size_t i=0; i<this->numLevels()-1; i++) {
        bilinearFormsHierarchy[i] = Dune::shared_ptr<MatrixType>(new MatrixType);
        rightHandSidesHierarchy[i] = new VectorType;
    }

    bilinearFormsHierarchy.back()  = std::const_pointer_cast<MatrixType>(bilinearForm);
    rightHandSidesHierarchy.back() = const_cast<VectorType*>(l);
    iteratesHierarchy.back()       = *u;

    // backup u
    VectorType* actualSolutionVector = u;

    // restrict top level matrix and right hand side to the lower levels
    for (int i=this->numLevels()-2; i>=0; i--) {

        this->mgTransfer_[i]->galerkinRestrictSetOccupation(*bilinearFormsHierarchy[i+1], *bilinearFormsHierarchy[i]);
        this->mgTransfer_[i]->galerkinRestrict(*bilinearFormsHierarchy[i+1], *bilinearFormsHierarchy[i]);
        this->mgTransfer_[i]->restrict(*rightHandSidesHierarchy[i+1], *rightHandSidesHierarchy[i]);

        // /////////////////////////////////////////////////////////////////////////////////////////////
        //   Transfer the initial iterate to the lower levels, because it contains the Dirichlet data
        //   The following loop picks out the '1'-entries of the prolongation matrix to select only
        //   those vertices of the the fine grid initial iterate that exist on the coarser levels.
        // /////////////////////////////////////////////////////////////////////////////////////////////
        const MatrixType& transferMatrix = dynamic_cast<CompressedMultigridTransfer<VectorType>*>(this->mgTransfer_[i])->getMatrix();

        iteratesHierarchy[i].resize(transferMatrix.M());
        iteratesHierarchy[i] = 0;

        typedef typename MatrixType::row_type RowType;
        typedef typename RowType::ConstIterator ColumnIterator;

        for (size_t rowIdx=0; rowIdx<transferMatrix.N(); rowIdx++) {

            const RowType& row = transferMatrix[rowIdx];

            ColumnIterator cIt    = row.begin();
            ColumnIterator cEndIt = row.end();

            for(; cIt!=cEndIt; ++cIt)
                if ((*cIt)[0][0] > 0.95)
                    (*cIt).umtv(iteratesHierarchy[i+1][rowIdx], iteratesHierarchy[i][cIt.index()]);
        }

    }

    // ///////////////////////////////////////////////////
    //   The actual nested-iteration loop
    // ///////////////////////////////////////////////////

    for (size_t i=0; i<this->numLevels()-1; i++) {

        // set current nonlinear level
        nonlinearLevel_ = i;

        bilinearForm = bilinearFormsHierarchy[i];
        l            = rightHandSidesHierarchy[i];
        u            = &iteratesHierarchy[i];

        if (this->signoriniNodes_)
            std::cout << "WARNING: nested iteration disregards Signorini information!" << std::endl;

        // Set up fine grid presmoother
        nonlinearPreSmoother_.setProblem(*bilinearForm, *u, *l);
        nonlinearPreSmoother_.ignoreNodes_ = this->ignoreNodesHierarchy_[i];
        //nonlinearPreSmoother_.signorini_      = &((*this->signorini_)[i]);
        if (robinNodes_)
            std::cout << "WARNING: nested iteration disregards Robin coupling term!" << std::endl;

        // Set up fine grid postsmoother
        nonlinearPostSmoother_.setProblem(*bilinearForm, *u, *l);
        nonlinearPostSmoother_.ignoreNodes_ = this->ignoreNodesHierarchy_[i];
        //nonlinearPostSmoother_.signorini_      = &((*this->signorini_)[i]);
        if (robinNodes_)
            std::cout << "WARNING: nested iteration disregards Robin coupling term!" << std::endl;

        // iterate once
        iterate();

        // transfer function to the next-higher level
        this->mgTransfer_[i]->prolong(iteratesHierarchy[i], iteratesHierarchy[i+1]);

    }

    // Make the highest level the nonlinearLevel
    nonlinearLevel_ = this->numLevels()-1;

    // make the pointers to the matrix, rhs, solution point to their original values
    bilinearForm = bilinearFormsHierarchy.back();
    l            = rightHandSidesHierarchy.back();
    u            = actualSolutionVector;
    *u           = iteratesHierarchy.back();

    // //////////////////////////////////////////////////
    //   Delete temporary vectors
    // //////////////////////////////////////////////////

    // delete all but the top level entries
    for (size_t i=0; i<this->numLevels()-1; i++)
        delete rightHandSidesHierarchy[i];
}


// //////////////////////////////////////////////////////////////////
//   Compute obstacles that trap each value in the phase it is
//   currently in.  The phases boundaries are -1 and hatu_a.
//
// //////////////////////////////////////////////////////////////////

template <class MatrixType, class VectorType>
void RichardsMultigridStep<MatrixType, VectorType>::
computeObstacles(std::vector<BoxConstraint<double,1> >& obstacles)
{
    unsigned int truncated = 0;

    for (size_t i=0; i<u->size(); i++) {

        // Computes the defect obstacles
        Dune::Solvers::Interval<double> localObstacle;
        //#warning smoothDomain used to be used here!
        //nonlinearity_->smoothDomain(i,(*u)[i],0,localObstacle,0);
        nonlinearity_->domain(i,localObstacle,0);

        if ((*this->ignoreNodesHierarchy_.back())[i][0]
            || (fineGridDirichlet_.size()==u->size() && fineGridDirichlet_[i][0])
            || (*u)[i] <= parameters_->hatu_L() ) {

            obstacles[i].lower(0) = obstacles[i].upper(0) = 0;

        } else {
            obstacles[i].lower(0) = localObstacle[0] - (*u)[i];
            obstacles[i].upper(0) = localObstacle[1] - (*u)[i];
        }

        // Compute Lipschitz constants for proper local damping
        lipschitzConstants_[nonlinearLevel_][i] = 0;

        if ( !(*this->ignoreNodesHierarchy_.back())[i][0]
            and not (fineGridDirichlet_.size()==u->size() && fineGridDirichlet_[i][0])
            and (*u)[i] <= parameters_->generalizedPhaseBoundary()) {
            lipschitzConstants_[nonlinearLevel_][i] = parameters_->L;
        }

        // Count the number of active nodes (just for the users' information)
        if ((*this->ignoreNodesHierarchy_.back())[i][0]
            or (fineGridDirichlet_.size()==u->size() && fineGridDirichlet_[i][0])
            or (*u)[i] <= parameters_->hatu_L() )
            truncated++;


    }

    outStream_ << std::setw(9) << truncated;

    // Compute the lower levels of L!
    for (int i=nonlinearLevel_; i>0; i--) {

        const Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >& mat = dynamic_cast<CompressedMultigridTransfer<VectorType>*>(this->mgTransfer_[i-1])->getMatrix();

        typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >::row_type::ConstIterator ColumnIterator;

        lipschitzConstants_[i-1] = 0;

        VectorType& tIt = lipschitzConstants_[i-1];
        const VectorType& fIt = lipschitzConstants_[i];
        const Dune::BlockVector<Dune::FieldVector<double,1> >& lAW = (*h_p_)[i];

        for(size_t row=0; row<mat.N(); row++) {

            ColumnIterator cIt    = mat[row].begin();
            ColumnIterator cEndIt = mat[row].end();

            for(; cIt!=cEndIt; ++cIt) {
                //printf("level: %d  row: %d   %g\n", i, row, fIt[row][0]);
                tIt[cIt.index()][0] += fabs(*cIt) * fIt[row][0]  * lAW[row][0];
            }

        }

    }

}

template <class MatrixType, class VectorType>
void RichardsMultigridStep<MatrixType, VectorType>::iterate()
{
    // Presmoothing
    for (int i=0; i<this->nu1_; i++)
        nonlinearPreSmoother_.iterate();

    // Do a coarse correction step
    coarseCorrection();

    // Add the correction to the solution
    *u += (*this->xHierarchy_[nonlinearLevel_]);

    // Postsmoothing
    for (int i=0; i<this->nu2_; i++)
        nonlinearPostSmoother_.iterate();

}

template <class MatrixType, class VectorType>
void RichardsMultigridStep<MatrixType, VectorType>::
computeNewtonLinearization()
 {
     MatrixType& mat = *std::const_pointer_cast<MatrixType>(this->matrixHierarchy_[nonlinearLevel_]);
     std::vector<VectorType>& rhs = this->rhsHierarchy_;

     // The matrix holding the Newton linearization
     mat = *bilinearForm;
     mat *= timestep_;

     // Add second derivatives of the nonlinearity to the matrix
     nonlinearity_->addHessian(*u, mat);

    /////////////////////////////////////////////////////////////////////////////////////////////
    //  Compute the linear part of the quadratic model (i.e. of the Newton linearization)
    // \f$ l_{\bar{u}_j^\nu}(w,w) = l(w)
    //                            - \phi'_{\bar{u}_j^\nu}(\bar{u}_j^\nu)(w)
    //                            + \phi''_{\bar{u}_j^\nu}(\bar{u}_j^\nu)(\bar{u}_j^\nu,w) \f$
    /////////////////////////////////////////////////////////////////////////////////////////////

    rhs[nonlinearLevel_] = *l;

    // The negative gradient of the nonlinearity
    VectorType gradientOfNonlinearity(rhs[nonlinearLevel_].size());
    gradientOfNonlinearity = 0;
    nonlinearity_->addGradient(*u, gradientOfNonlinearity);
    rhs[nonlinearLevel_] -= gradientOfNonlinearity;

    // The second derivative of the nonlinearity times the presmoothed iterate
    Dune::BDMatrix<Dune::FieldMatrix<double,1,1> > secondDerivativeOfNonlinearity(rhs[nonlinearLevel_].size());
    secondDerivativeOfNonlinearity = 0;
    nonlinearity_->addHessian(*u, secondDerivativeOfNonlinearity);

    for (size_t i=0; i<mat.N(); i++)
        rhs[nonlinearLevel_][i] += secondDerivativeOfNonlinearity[i][i][0][0] * (*u)[i];

    //////////////////////////////////////////////
    // Compute restricted stiffness matrices
    //////////////////////////////////////////////
    for (int i=nonlinearLevel_-1; i>=0; i--)
        this->mgTransfer_[i]->galerkinRestrict(*(this->matrixHierarchy_[i+1]), *std::const_pointer_cast<MatrixType>(this->matrixHierarchy_[i]));

}

template <class MatrixType, class VectorType>
void RichardsMultigridStep<MatrixType, VectorType>::
coarseCorrection()
{
    // Determine phase and obstacle for each coarse grid dof
    if (parameters_->saturation_) {
        computeObstacles(obstacle_[nonlinearLevel_]);
    } else {
        obstacle_[nonlinearLevel_].clear();
    }

    // Construct Newton linearization at the current solution
    computeNewtonLinearization();

    // Initialize B_l
    B_l_ = 0;

    // Set up residual problem
    this->matrixHierarchy_[nonlinearLevel_]->mmv(*u, this->rhsHierarchy_[nonlinearLevel_]);
    *(this->xHierarchy_[nonlinearLevel_]) = 0;

    // Coarse grid correction
    for (int i=0; i<this->mu_; i++)
        coarseCorrectionIterate(nonlinearLevel_);

}

template <class MatrixType, class VectorType>
void RichardsMultigridStep<MatrixType, VectorType>::
coarseCorrectionIterate(int level)
{
    // Define references just for ease of notation
    std::vector<Dune::shared_ptr<VectorType> >& x   = this->xHierarchy_;
    std::vector<VectorType>& rhs = this->rhsHierarchy_;

    // Solve directly if we're looking at the coarse problem
    if (level == 0) {
        this->basesolver_->solve();
        return;
    }

    // Presmoothing
    Dune::shared_ptr<LinearIterationStep<MatrixType, VectorType> > linearPresmoother = this->presmootherDefault_;

    linearPresmoother->setProblem(*(this->matrixHierarchy_[level]), *(x[level]), rhs[level]);

    std::dynamic_pointer_cast<CanIgnore<Dune::BitSetVector<1> > >(linearPresmoother)->ignoreNodes_ = this->ignoreNodesHierarchy_[level];

    if (Dune::shared_ptr<RichardsCoarseGSStep<MatrixType,VectorType> > linearPreSmoother
        = std::dynamic_pointer_cast<RichardsCoarseGSStep<MatrixType,VectorType> >(linearPresmoother)) {
        linearPreSmoother->setObstacles(obstacle_[level]);
        linearPreSmoother->lipschitzConstants = &lipschitzConstants_[level];
        linearPreSmoother->B_l = &B_l_;
    }

    for (int i=0; i<this->nu1_; i++)
        linearPresmoother->iterate();

    // First, a backup of the obstacles for postsmoothing
    std::vector<BoxConstraint<double,1> > obstacleBackup = obstacle_[level];

    // Compute defect obstacles
    for (size_t i=0; i<obstacle_[level].size(); i++)
        obstacle_[level][i] -= (*(x[level]))[i];

    // /////////////////////////
    // Restriction
    ////////////////////////////
    for (size_t i=0; i<obstacle_[level].size(); i++) {
        if (obstacle_[level][i].lower(0) > obstacle_[level][i].upper(0) + 1e-10)
            DUNE_THROW(Dune::Exception, "level " << level << " obstacle order violated at " << i
                       << " (by " << (obstacle_[level][i].lower(0) - obstacle_[level][i].upper(0)) << ")");
        if (obstacle_[level][i].lower(0) > 1e-10)
            DUNE_THROW(Dune::Exception, "level " << level << " positive lower obstacle at " << i);
        if (obstacle_[level][i].upper(0) < -1e-10)
            DUNE_THROW(Dune::Exception, "level " << level << " negative upper obstacle at " << i);
    }

    /** \todo This needs to be set properly */
    Dune::BitSetVector<1> critical(obstacle_[level].size(),false);

    // The Mandel obstacle restrictor ignores these fields.  They just have to be there.
    Dune::BitSetVector<1> fineHasObstacle(obstacle_[level].size(), false);
    Dune::BitSetVector<1> coarseHasObstacle(obstacle_[level-1].size(), false);

    obstacleRestrictor_.restrict(obstacle_[level], obstacle_[level-1],
                                 fineHasObstacle, coarseHasObstacle,
                                 *this->mgTransfer_[level-1],
                                 critical);

    for (size_t i=0; i<obstacle_[level-1].size(); i++) {
        if (obstacle_[level-1][i].lower(0) > obstacle_[level-1][i].upper(0) + 1e-10) {
            std::cout << "Lower obstacle: " << obstacle_[level-1][i].lower(0)
                      << ",   upper obstacle: " << obstacle_[level-1][i].upper(0) << std::endl;
            DUNE_THROW(Dune::Exception, "obstacle order violated at " << i);
        }
    }

    // compute residuum
    // fineResiduum = rhs[level] - mat[level] * x[level];
    VectorType fineResidual = rhs[level];
    this->matrixHierarchy_[level]->mmv(*(x[level]), fineResidual);

    // restrict residuum
    this->mgTransfer_[level-1]->restrict(fineResidual, rhs[level-1]);

    // Choose all zeros as the initial correction
    *(x[level-1]) = 0;

    // ///////////////////////////////////////
    // Recursively solve the coarser system
    // ///////////////////////////////////////
    for (int i=0; i<this->mu_; i++)
        coarseCorrectionIterate(level-1);

    ///////////////////////////////////////////
    // Prolongation
    ///////////////////////////////////////////

    // add correction to the presmoothed solution
    VectorType tmp;
    this->mgTransfer_[level-1]->prolong(*(x[level-1]), tmp);
    *(x[level]) += tmp;

    // restore the true (non-defect) obstacles
    obstacle_[level]      = obstacleBackup;

    // Postsmoothing
    Dune::shared_ptr<LinearIterationStep<MatrixType, VectorType> > linearPostsmoother = this->postsmootherDefault_;
    linearPostsmoother->setProblem(*(this->matrixHierarchy_[level]), *(x[level]), rhs[level]);

    std::dynamic_pointer_cast<CanIgnore<Dune::BitSetVector<1> > >(linearPostsmoother)->ignoreNodes_ = this->ignoreNodesHierarchy_[level];

    if (Dune::shared_ptr<RichardsCoarseGSStep<MatrixType,VectorType> > linearPostSmoother
        = std::dynamic_pointer_cast<RichardsCoarseGSStep<MatrixType,VectorType> >(linearPostsmoother)) {
        linearPostSmoother->setObstacles(obstacle_[level]);
        linearPostSmoother->lipschitzConstants = &lipschitzConstants_[level];
        linearPostSmoother->B_l = &B_l_;
    }

    for (int i=0; i<this->nu2_; i++)
        linearPostsmoother->iterate();

}
