#include "../tnnmg/bisection.hh"
#include "../tnnmg/onedconvexfunction.hh"

#include "richardstransforms.hh"

template<class MatrixType, class VectorType>
void RichardsGSStep<MatrixType, VectorType>::iterate()
{
    VectorType& x = *this->x_;
    
    int count = 0;

    for (size_t i=0; i<x.size(); i++) {
        
        // Handle Dirichlet nodes
        if ((*this->ignoreNodes_)[i][0])
            continue;

        const field_type& a_ii = (*this->mat_)[i][i];

        // Hack: the right hand side has already been multiplied by the time step,
        // but the matrix hasn't.  We temporarily remove the 'timestep' factor in front
        // of rhs to be able to call the standard 'residual' method correctly.
        /** \todo Fix this! */
        (*const_cast<VectorType*>(this->rhs_))[i] /= timestep_;
        Dune::FieldVector<field_type, 1> r;
        // r = rhs_[i] - r
        this->residual(i, r);
        /** \todo Fix this! */
        r *= timestep_;
        (*const_cast<VectorType*>(this->rhs_))[i] *= timestep_;
        
        OneDConvexFunction<NonlinearityType> richardsConvexFunction(timestep_ * a_ii,
                                                                    r + x[i]*a_ii*timestep_,
                                                                    *nonlinearity_,
                                                                    i,
                                                                    0);
            
        Bisection bisection(0,             //acceptError
                            1  ,           //acceptFactor
                            1e+20,         //requiredResidual
                            false,         //fastquadratic
                            1e-20          //safety
                            );
        
        int localCount = 0;
        x[i] = bisection.template minimize<OneDConvexFunction<NonlinearityType> >(richardsConvexFunction,
                                                                                  x[i], x[i], localCount);
        count += localCount;

        // /////////////////////////////////////
        //   Handle the Signorini boundary
        // /////////////////////////////////////
#ifdef CENTER_IN_HATU
        if (signoriniNodes_ && (*signoriniNodes_)[i][0] && x[i] > -parameters_->u_c_)
            x[i] = -parameters_->u_c_;
#else
        if (signoriniNodes_ && (*signoriniNodes_)[i][0] && x[i] > 0)
            x[i] = 0;
#endif
    }
    
}
