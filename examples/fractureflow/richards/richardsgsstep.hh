#ifndef DUNE_RICHARDS_GAUSS_SEIDEL_STEP_HH
#define DUNE_RICHARDS_GAUSS_SEIDEL_STEP_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include "../solvers/blockgsstep.hh"

#include "../tnnmg/nonlinearity.hh"

#include "richardsparameters.hh"

    template<class MatrixType, class VectorType>
    class RichardsGSStep : public BlockGSStep<MatrixType, VectorType>
    {
        typedef typename VectorType::block_type VectorBlock;

        typedef typename VectorType::field_type field_type;

        enum {BlockSize = VectorBlock::dimension};

        typedef Nonlinearity<Dune::FieldVector<double,1>,Dune::FieldMatrix<double,1,1> > NonlinearityType;

    public:

        RichardsGSStep()
            : signoriniNodes_(NULL)
        {}
        
        RichardsGSStep(MatrixType& mat, VectorType& x, VectorType& rhs)
            : BlockGSStep<MatrixType,VectorType>(mat, x, rhs),
              signoriniNodes_(NULL)
        {}

        virtual void iterate();

        //! The problem parameters
        const RichardsParameters* parameters_;

        //!
        field_type timestep_;

        //! Specifies for each degree of freedom whether it has a Signorini boundary condition
        const Dune::BitSetVector<1>* signoriniNodes_;

        /** \brief The nonlinearity of the Richards problem */
        Dune::shared_ptr<NonlinearityType> nonlinearity_;

    };

#include "richardsgsstep.cc"

#endif
