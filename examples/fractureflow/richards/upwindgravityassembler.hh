#ifndef UPWIND_GRAVITY_ASSEMBLER_HH
#define UPWIND_GRAVITY_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include "../fufem/localoperatorassembler.hh"

/** \brief Local assembler for the Laplace problem */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE>
class UpwindGravityAssembler
: public LocalOperatorAssembler <GridType,
                                 TrialLocalFE,
                                 AnsatzLocalFE,
                                 Dune::FieldMatrix<double,1,1> >
{
    private:
        typedef Dune::FieldMatrix<double,1,1> T;
        static const int dim      = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;
        const int quadOrder_;
        Dune::FieldVector<double,GridType::dimensionworld> gravityDirection_;


    public:
        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::Element Element;
        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::BoolMatrix BoolMatrix;
        typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::LocalMatrix LocalMatrix;

        /** \brief Constructor */
        UpwindGravityAssembler(const Dune::FieldVector<double,GridType::dimensionworld>& gravityDirection)
        : quadOrder_(1),
          gravityDirection_(gravityDirection)
        { }

        void setup(const Dune::FieldVector<double,GridType::dimensionworld>& gravityDirection)
        {
            gravityDirection_ = gravityDirection;
        }

        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dim> FVdim;
            typedef typename Dune::template FieldVector<double,dimworld> FVdimworld;
            typedef typename Dune::template FieldMatrix<double,dimworld,dim> FMdimworlddim;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());
            assert(aFE.type() == element.type());

            localMatrix = 0.0;

            // get quadrature rule
            const Dune::QuadratureRule<double, dim>& quad = Dune::QuadratureRules<double, dim>::rule(element.type(), quadOrder_);

            // store gradients of shape functions and base functions
            std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<FVdimworld> gradients(tFE.localBasis().size());


            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const FVdim& quadPos = quad[pt].position();

                // get transposed inverse of Jacobian of transformation
                const FMdimworlddim& invJacobian = element.geometry().jacobianInverseTransposed(quadPos);

                // get integration factor
                const double integrationElement = element.geometry().integrationElement(quadPos);

                // get gradients of shape functions
                tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                // transform gradients
                for (size_t i=0; i<gradients.size(); ++i)
                    invJacobian.mv(referenceGradients[i][0], gradients[i]);

                // Compute derivative in the direction of the gravity
                std::vector<double> directionalDerivative(gradients.size());

                for(size_t i=0; i<gradients.size(); i++)
                    directionalDerivative[i] = gradients[i] * gravityDirection_;

                // Compute diameter of the element
                double diameter = 0;
                for (int i=0; i<element.geometry().corners(); i++)
                    for (int j=0; j<i; j++)
                        diameter = std::max(diameter, (element.geometry().corner(i)-element.geometry().corner(j)).two_norm());

                double h = diameter / std::sqrt(dim);  // gives the edge length in uniform grids

                // Evaluate base functions
                std::vector<Dune::FieldVector<double,1> > v;
                tFE.localBasis().evaluateFunction(quadPos,v);

                for (size_t i=0; i<gradients.size(); i++)
                    for (size_t j=0; j<gradients.size(); j++ ) {
                        localMatrix[i][j][0][0] += (directionalDerivative[i] * v[j]) * quad[pt].weight() * integrationElement;
                        localMatrix[i][j][0][0] -= (h/3.0) * ( directionalDerivative[i] * directionalDerivative[j] ) * quad[pt].weight() * integrationElement;
                    }

            }

        }

};

#endif
