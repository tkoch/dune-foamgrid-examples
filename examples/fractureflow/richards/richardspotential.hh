#ifndef RICHARDS_POTENTIAL_HH
#define RICHARDS_POTENTIAL_HH

#include "richardsparameters.hh"
#include "../tnnmg/pointwisenonlinearity.hh"

template <class field_type>
class RichardsPotential
    : public PointwiseNonlinearity<Dune::FieldVector<field_type,1>, Dune::FieldMatrix<field_type,1,1> >
{

#ifdef CENTER_IN_HATU
    static_assert(false, "The RichardsPotential class doesn't support CENTER_IN_HATU");
#endif

public:
    typedef Dune::FieldVector<field_type,1> LocalVectorType;
    typedef Dune::FieldMatrix<field_type,1,1> LocalMatrixType;

    typedef Nonlinearity<LocalVectorType, LocalMatrixType> NonlinearityType;

    using NonlinearityType::block_size;


    typedef typename NonlinearityType::VectorType VectorType;
    typedef typename NonlinearityType::MatrixType MatrixType;
    typedef typename NonlinearityType::IndexSet IndexSet;


    RichardsPotential(const RichardsParameters* parameters,
                      const VectorType& nodalBasisFunctionIntegrals,
                      double _TOL = 1e-15) :
        TOL(_TOL),
        parameters_(parameters),
        lambda_(parameters->lambda),
        theta_m_(parameters->theta_m_),
        theta_M_(parameters->theta_M_),
        u_c_(parameters->u_c_),
        hatn_(parameters->hatn),
        tzero_(parameters->tzero),
        nodalBasisFunctionIntegrals_(nodalBasisFunctionIntegrals)
    {}

    /** \brief Evaluate the functional */
    double operator()(const VectorType& v) const
    {
        std::cout << "Calling untested operator()" << std::endl;
        static_assert(block_size==1, "RichardsPotential can only be instantiated for vectors of scalars");
        double r = 0.0;
        const double c1 = lambda_*(1+parameters_->elambda())-1;
        const double c2 = lambda_*parameters_->elambda() - 1;

        for(std::size_t row=0; row<v.size(); ++row) {

            const double& u = v[row][0];

            if (u < u_c_)
                return std::numeric_limits<double>::max();

            if (u >=-1)
                r += theta_M_ * u * nodalBasisFunctionIntegrals_[row];
            else {
                r += theta_m_ * u * nodalBasisFunctionIntegrals_[row]
                    + (theta_M_ - theta_m_)/c1
                    * (pow(c2*u + lambda_*parameters_->elambda(), c1/c2) - lambda_*(1+parameters_->elambda()))
                    * nodalBasisFunctionIntegrals_[row];
            }

        }

        r *= hatn_ / tzero_;

        return r;
    }

    void addGradient(const VectorType& v, VectorType& gradient) const {
        for (std::size_t i=0; i<v.size(); ++i)
            gradient[i][0] += d(i, v[i][0]);
    }


    void addHessian(const VectorType& v, MatrixType& hessian) const {
        // if u[i] is equal to hatu_a or -1 (the phase boundaries), calHatHDer isn't defined, because
        // calHatH is not differentiable at the phase boundaries.  Thus we don't add anything to the
        // Newton matrix, which doesn't matter, because those entries get skipped anyways.  (see the
        // how the obstacles are set for those i)
        for(size_t row = 0; row < hessian.N(); ++row) {
            const field_type& x = v[row][0];
            if (x != parameters_->u_c_ && x!=-1)
                hessian[row][row] += (hatn_/tzero_)*parameters_->MPrime(x)*nodalBasisFunctionIntegrals_[row];
        }
    }

    void addHessianIndices(IndexSet& indices) const {
        for (size_t i=0; i<indices.rows(); ++i)
            indices.add(i,i);
    }

    void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const
    {
        D[0] = D[1] = d(i,x);

        if (x < u_c_)
            {
                D[0] = -std::numeric_limits<double>::max();
                D[1] = -std::numeric_limits<double>::max();
            }

        if (x <= u_c_+TOL)
            D[0] = -std::numeric_limits<double>::max();

    }

    double regularity(int i, double x, int j) const
    {
        if (x <= u_c_ + TOL)
            return std::numeric_limits<double>::max();

        return std::fabs(parameters_->MPrimePrime(x));
    }

    /** \brief The domain of definition of the functional
     */
    void domain(int i, Dune::Solvers::Interval<double>& dom, int j) const
    {
#ifdef ELLIPTIC
        dom[0] = -std::numeric_limits<field_type>::max();
#else
        dom[0] = u_c_;
#endif
        dom[1] = std::numeric_limits<field_type>::max();
        return;
    }

    /** \brief The functional gradient */
    double d(int i, double x) const
    {
        return (hatn_/tzero_)*parameters_->M(x)*nodalBasisFunctionIntegrals_[i];
    }

    /** \brief Compute a local domain around t=0 where the nonlinearity restricted to the
     * line u_pos' +t e_(i,j) is smooth.
     *
     * Here e_(i,j) is the (i,j)-th Euclidean unit vector,
     *
     * \param i global index
     * \param j local index
     * \param x value of the (i,j)-th entry of position to evaluate the nonlinearity at
     * \param[out] dom the domain
     */
    virtual void smoothDomain(int i, double x, double regularity, Dune::Solvers::Interval<double>& dom, int j) const
    {
        if (x > parameters_->generalizedPhaseBoundary()) {
            dom[0] = parameters_->generalizedPhaseBoundary();
            dom[1] = std::numeric_limits<double>::max();
        } else {
#ifdef ELLIPTIC
            dom[0] = -std::numeric_limits<field_type>::max();
#else
            dom[0] = u_c_;
#endif
            dom[1] = parameters_->generalizedPhaseBoundary();
        }
    }

private:
    const double TOL;

    const RichardsParameters* parameters_;

    const double lambda_;
    const double theta_m_;
    const double theta_M_;
    const double u_c_;
    const double hatn_;
    const double tzero_;

    const VectorType& nodalBasisFunctionIntegrals_;

};

#endif

