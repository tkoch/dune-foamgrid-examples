#ifndef KIRCHHOFF_TRANSFORMATION_HH
#define KIRCHHOFF_TRANSFORMATION_HH

#include <cmath>

#include <dune/common/parametertree.hh>

template <class field_type, bool elliptic_regularization>
class KirchhoffTransformation
{
#ifdef CENTER_IN_HATU
    dune_static_assert(not elliptic_regularization, 
                       "CENTER_IN_HATU and elliptic_regularization have never been tested together!");
#endif
    
    // Porengrößenverteilung
    field_type lambda;

    // Exponent in the Brooks-Corey saturation-to-rel.-permeability mapping
    field_type elambda() const {return 3 + 2/lambda;}

    // critical generalized pressure
    field_type u_c_;

    //////////////////////////////////////////////////////////////////////////////////
    //  The following three variables are only needed for elliptic regularization
    //////////////////////////////////////////////////////////////////////////////////

    field_type alpha_;

    field_type p_alpha_;

    field_type u_alpha_;

public:

    void setup(const Dune::ParameterTree& parameterSet) {

        if (elliptic_regularization)
            alpha_  = parameterSet.get<field_type>("alpha");

        lambda = parameterSet.get<field_type>("lambda");
        u_c_ = -lambda*elambda() / (lambda*elambda() - 1);

        if (elliptic_regularization) {
            u_alpha_ = std::pow(alpha_,1- 1/(lambda*elambda())) / ((lambda*elambda())-1);
            u_alpha_ = u_alpha_ - (lambda*elambda())/(lambda*elambda()-1);

            p_alpha_ = -std::pow(alpha_, -1/(lambda*elambda()));
        }

    }

    field_type evaluate(field_type p) const
    {
        field_type u;
#ifdef CENTER_IN_HATU
        if (p >= -1)
            u = p - u_c;
#else
        if (p >= -1)
            u = p;
#endif

        else if (elliptic_regularization and p<p_alpha_) {

            u = u_alpha_ + (p - p_alpha_) * alpha_;

        } else {
            u =  (1/ (lambda*elambda() - 1));
            u *= pow( -p, -lambda*elambda() + 1);
#ifndef CENTER_IN_HATU
            u -= (lambda*elambda()) / (lambda*elambda()-1);
#endif
        }
        
        return u;
    }

    /** \brief Evaluate the derivative of the Kirchhoff transformation at p
     */
    field_type evaluateDerivative(field_type p) const
    {
        field_type u;
        if (p >= -1)
            u = 1;
        else if (elliptic_regularization and p<p_alpha_) {

            u = alpha_;

        } else {
            u = -1 * pow( -p, -lambda*elambda() );
        }
        
        return u;
    }

    field_type inverse(field_type u) const
    {
        field_type p;
#ifdef CENTER_IN_HATU
        if (u >= -1 - u_c)
            p = u + u_c;
#else
        if (u >= -1)
            p = u;
#endif
        else if (elliptic_regularization and u <= u_alpha_) {

            p = (u-u_alpha_)/alpha_ + p_alpha_;

        } else {
#ifdef CENTER_IN_HATU
            p =  (u * (lambda*elambda()-1));
#else
            p =  (u * (lambda*elambda()-1)) + lambda*elambda();
#endif
            p = -pow(p, 1/(-lambda*elambda()+1));
        }
        
        return p;
    }

    // Not used when saturation is disregarded
    field_type inversePrime(field_type u) const
    {
        if (elliptic_regularization and u <= u_alpha_) 
            return 1/alpha_;
        else
#ifdef CENTER_IN_HATU
            if (u>=-1-u_c)
                return 1;
#else
        if (u>=-1)
            return 1;
#endif
        else {
            field_type p = (lambda*elambda()-1) * u;
#ifndef CENTER_IN_HATU
            p += lambda*elambda();
#endif
            return pow(p, -lambda*elambda() / (lambda*elambda()-1));
        }
    }

    field_type inversePrimePrime(field_type u) const
    {
        if (elliptic_regularization)
            DUNE_THROW(Dune::NotImplemented, "invKirchhoffTransformDerDer with elliptic regularization");

#ifdef CENTER_IN_HATU
        DUNE_THROW(Dune::NotImplemented, "invKirchhoffTransformDerDer for CENTER_IN_HATU");
#endif
        if (u>=-1)
            return 0;
        
        return -lambda*elambda() * std::pow((lambda*elambda()-1)*u + lambda*elambda(), -1*(2*lambda*elambda()-2)/(lambda*elambda()-1));
    }
    
    field_type primitiveOfInverse(field_type u) const
    {
        if (elliptic_regularization)
            DUNE_THROW(Dune::NotImplemented, "primitiveOfInvKirchhoffTransform with elliptic regularization");

        if (u >= -1)
            return 0.5*u*u + 1/(lambda*elambda()-2) - 0.5;
        
        field_type p = (lambda*elambda()-1) * u + lambda*elambda();
        return (1/(lambda*elambda()-2))*std::pow(p, (lambda*elambda()-2)/(lambda*elambda()-1));
        
    }
    
};

#endif
