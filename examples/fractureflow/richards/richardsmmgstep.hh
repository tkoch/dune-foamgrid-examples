#ifndef DUNE_RICHARDS_MONOTONE_MG_STEP_HH
#define DUNE_RICHARDS_MONOTONE_MG_STEP_HH

#include <vector>

#include "../solvers/multigridstep.hh"
#include "../solvers/mandelobsrestrictor.hh"

#include "richardscoarsegs.hh"
#include "richardsgsstep.hh"
#include "richardsparameters.hh"

    /**

    */
    template<class MatrixType, class VectorType>
    class RichardsMultigridStep : public MultigridStep<MatrixType, VectorType>
    {
        // The type used for individual real numbers
        typedef typename VectorType::field_type field_type;

        // The type of superposition operator that is part of our problem
        typedef Nonlinearity<Dune::FieldVector<double,1>,Dune::FieldMatrix<double,1,1> > NonlinearityType;

    public:

        RichardsMultigridStep() : MultigridStep<MatrixType, VectorType>(),
                                  bilinearForm(NULL), l(NULL), u(NULL),
                                  nonlinearLevel_(-1),
                                  parameters_(NULL), h_p_(NULL), h_Gamma_p_(NULL),
                                  robinNodes_(NULL), signoriniNodes_(NULL), fineGridDirichlet_(0)
        {} 

        RichardsMultigridStep(int numLevels) 
            : bilinearForm(NULL), l(NULL), u(NULL),
              nonlinearLevel_(numLevels-1),
              parameters_(NULL), h_p_(NULL), h_Gamma_p_(NULL),
              robinNodes_(NULL), signoriniNodes_(NULL), fineGridDirichlet_(0),
              obstacle_(numLevels)
        {
            this->setNumberOfLevels(numLevels);
        }

        /** \brief Set up a RichardsMultigridStep for a given problem and a given number of levels */
        RichardsMultigridStep(const MatrixType& mat, 
                              VectorType& x, 
                              VectorType& rhs,
                              const std::shared_ptr<NonlinearityType>& nonlinearity,
                              int numLevels)
            : MultigridStep<MatrixType, VectorType>(),
              bilinearForm(Dune::stackobject_to_shared_ptr(mat)), l(&rhs), u(&x),
              nonlinearLevel_(numLevels-1),
              nonlinearity_(nonlinearity),
              parameters_(NULL), h_p_(NULL), h_Gamma_p_(NULL),
              robinNodes_(NULL), signoriniNodes_(NULL), fineGridDirichlet_(0)
        {
            this->setNumberOfLevels(numLevels);
            this->xHierarchy_.resize(numLevels);
            this->rhsHierarchy_.resize(numLevels);
            this->matrixHierarchy_.resize(numLevels);

            nonlinearPreSmoother_.nonlinearity_  = nonlinearity;
            nonlinearPostSmoother_.nonlinearity_ = nonlinearity;
        }

        virtual void setProblem(const MatrixType& mat, 
                                VectorType& x, 
                                VectorType& rhs,
                                const Dune::shared_ptr<NonlinearityType>& nonlinearity,
                                int numLevels) 
        {
            MultigridStep<MatrixType, VectorType>::setProblem(mat, x, rhs);
            // the previous method makes matrixHierarchy_.back() point to mat.  We don't want that
            this->matrixHierarchy_.back().reset();
            bilinearForm = Dune::stackobject_to_shared_ptr(mat);
            l = &rhs;
            u = &x;

            setNumberOfLevels(numLevels);

            nonlinearLevel_ = numLevels-1;
            
            nonlinearPreSmoother_.nonlinearity_  = nonlinearity;
            nonlinearPostSmoother_.nonlinearity_ = nonlinearity;

            nonlinearity_ = nonlinearity;
        }

        virtual void setProblem(const MatrixType& mat, 
                                VectorType& x, 
                                VectorType& rhs,
                                int numLevels)
        {
            MultigridStep<MatrixType, VectorType>::setProblem(mat, x, rhs);
            // the previous method makes matrixHierarchy_.back() point to mat.  We don't want that
            this->matrixHierarchy_.back().reset();
            bilinearForm = Dune::stackobject_to_shared_ptr(mat);
            l = &rhs;
            u = &x;

            setNumberOfLevels(numLevels);
            
            nonlinearLevel_ = numLevels-1;
        }

        virtual void setProblem(const MatrixType& mat, 
                                VectorType& x, 
                                const VectorType& rhs)
        {
            MultigridStep<MatrixType, VectorType>::setProblem(mat, x, rhs);
            // the previous method makes mat_.back() point to mat.  We don't want that
            this->matrixHierarchy_.back().reset();
            bilinearForm = Dune::stackobject_to_shared_ptr(mat);
            l = &rhs;
            u = &x;
        }
        
        virtual void setNumberOfLevels(int numLevels)
        {
            for (int i=0; i<int(this->ignoreNodesHierarchy_.size())-1; i++)
                if (this->ignoreNodesHierarchy_[i])
                    delete(this->ignoreNodesHierarchy_[i]);

            this->matrixHierarchy_.resize(numLevels);
            this->ignoreNodesHierarchy_.resize(numLevels);

            for (int i=0; i<int(this->matrixHierarchy_.size())-1; i++)
            {
                this->matrixHierarchy_[i].reset();
                this->ignoreNodesHierarchy_[i] = NULL;
            }

            this->presmoother_.resize(numLevels);
            this->postsmoother_.resize(numLevels);

            this->xHierarchy_.resize(numLevels);
            this->rhsHierarchy_.resize(numLevels);
        }

        virtual ~RichardsMultigridStep() {}
        
        virtual void iterate();

        virtual void nestedIteration();

    private:

        void computeObstacles(std::vector<BoxConstraint<double,1> >& obstacles);

        //! Solve the nonlinear coarse grid obstacle problems
        void coarseCorrection();

        void coarseCorrectionIterate(int level);

        void computeNewtonLinearization();

    public:

        virtual void preprocess();

        //! Return solution function
        virtual VectorType getSol();

        /** \brief Get screen output of one iteration */
        std::string getOutput() const;

        virtual const MatrixType* getMatrix()
        {
            return bilinearForm.get();
        }

        virtual void setRobinDamping(double gamma)
        {
            gamma_ = gamma;
        }
        
        //! Top level presmoother
        RichardsGSStep<MatrixType, VectorType> nonlinearPreSmoother_;

        //! Top level postsmoother
        RichardsGSStep<MatrixType, VectorType> nonlinearPostSmoother_;

        MandelObstacleRestrictor<VectorType> obstacleRestrictor_;

        std::shared_ptr<const MatrixType> bilinearForm;

        const VectorType* l;

        VectorType* u;

    private:

        std::vector<std::vector<BoxConstraint<double,1> > > obstacle_;

        int nonlinearLevel_;

    public:
        
        /** \brief The nonlinearity of the Richards problem */
        Dune::shared_ptr<Nonlinearity<Dune::FieldVector<field_type,1>, Dune::FieldMatrix<field_type,1,1> > > nonlinearity_;

        const RichardsParameters* parameters_;

        std::vector<Dune::BlockVector<Dune::FieldVector<double,1> > >* h_p_;

        Dune::BlockVector<Dune::FieldVector<double,1> >* h_Gamma_p_;

        /** \brief The Lipschitz constant of the local nonlinearity.
         *
         * Mathematically speaking, this is not a discrete function.  However,
         * it gets restricted similarly, so we implement it as one.
         */
        std::vector<VectorType> lipschitzConstants_;

        /** \brief The factor \f$B_l\f$ defined in formula (5.6) of Kornhuber91.
         */
        double B_l_;

    private:
        /** \brief The parameter for the Robin-DD method */
        double gamma_;

    public:
        /** \brief The time step size */
        double timestep_;

        //! Specifies for each degree of freedom whether it is on the Robin boundary
        const Dune::BitSetVector<1>* robinNodes_;

        //! Specifies for each degree of freedom whether it is a signorini boundary
        const Dune::BitSetVector<1>* signoriniNodes_;

        /** \brief Specifies for each dof whether it is a dirichlet node that
            cannot be resolved on the coarse grids and is thus handled like an obstacle */
        Dune::BitSetVector<1> fineGridDirichlet_;

        mutable std::ostringstream outStream_;

    };

#include "richardsmmgstep.cc"

#endif
