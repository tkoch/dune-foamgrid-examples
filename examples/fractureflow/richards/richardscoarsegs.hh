#ifndef RICHARDS_COARSE_GS_STEP_HH
#define RICHARDS_COARSE_GS_STEP_HH

#include <dune/common/fvector.hh>

#include "../solvers/blockgsstep.hh"

#include "richardsparameters.hh"

template<class MatrixType, class VectorType>
class RichardsCoarseGSStep : public BlockGSStep<MatrixType, VectorType>
{

    typedef typename VectorType::field_type field_type;

public:
    
    RichardsCoarseGSStep() {}
    
    RichardsCoarseGSStep(MatrixType& mat, VectorType& x, VectorType& rhs)
        : BlockGSStep<MatrixType,VectorType>(mat, x, rhs)
    {}
    
    virtual ~RichardsCoarseGSStep() {}
    
    virtual void iterate();
    
    void setObstacles(const std::vector<BoxConstraint<double,1> >& obstacle) {
        obstacle_ = &obstacle;
    }
    
    field_type computeDampingFactor(const field_type& residual, const field_type& z_l,
                                const field_type& a_ll, const field_type& L_l) const;
    
    VectorType* localAreaWeights;
    
    const std::vector<BoxConstraint<double,1> >* obstacle_;
    
    const VectorType* lipschitzConstants;
    
    const RichardsParameters* parameters;
    
    field_type* B_l;
    
    
};

#include "richardscoarsegs.cc"

#endif
