#ifndef OPERATOR_ASSEMBLER_HH
#define OPERATOR_ASSEMBLER_HH

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>

#include "functionspacebasis.hh"
#include "../solvers/arithmetic.hh"

//! Generic global assembler for operators on a gridview
template <class TrialBasis, class AnsatzBasis>
class OperatorAssembler
{
    private:
        typedef typename TrialBasis::GridView GridView;

    public:
        //! create assembler for grid
        OperatorAssembler(const TrialBasis& tBasis, const AnsatzBasis& aBasis) :
            tBasis_(tBasis),
            aBasis_(aBasis)
        {}


        template <class LocalAssemblerType>
        void addIndices(LocalAssemblerType& localAssembler, Dune::MatrixIndexSet& indices, const bool lumping=false) const
        {
            if (lumping)
                addIndicesStaticLumping<LocalAssemblerType,true>(localAssembler, indices);
            else
                addIndicesStaticLumping<LocalAssemblerType,false>(localAssembler, indices);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void addEntries(LocalAssemblerType& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            if (lumping)
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,true>(localAssembler, A);
            else
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,false>(localAssembler, A);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void assemble(LocalAssemblerType& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            int rows = tBasis_.size();
            int cols = aBasis_.size();

            Dune::MatrixIndexSet indices(rows, cols);

            addIndices(localAssembler, indices, lumping);

            indices.exportIdx(A);
            A=0.0;

            addEntries(localAssembler, A, lumping);

            return;
        }


    protected:

        template <class LocalAssemblerType, bool lumping>
        void addIndicesStaticLumping(LocalAssemblerType& localAssembler, Dune::MatrixIndexSet& indices) const
        {
            //typedef typename GridView::template Codim<0>::Iterator ElementIterator;
            typedef typename LocalAssemblerType::BoolMatrix BoolMatrix;

            auto it = tBasis_.getGridView().template begin<0, Dune::Interior_Partition>();
            auto end = tBasis_.getGridView().template end<0, Dune::Interior_Partition>();
            for (; it != end; ++it)
            {
                // get shape functions
                const typename TrialBasis::LocalFiniteElement& tFE = tBasis_.getLocalFiniteElement(*it);
                const typename AnsatzBasis::LocalFiniteElement& aFE = aBasis_.getLocalFiniteElement(*it);

                BoolMatrix localIndices(tFE.localBasis().size(), aFE.localBasis().size());
                localAssembler.indices(*it, localIndices, tFE, aFE);

                for (size_t i=0; i<tFE.localBasis().size(); ++i)
                {
                    int rowIndex = tBasis_.index(*it, i);
                    for (size_t j=0; j<aFE.localBasis().size(); ++j)
                    {
                        if (localIndices[i][j])
                        {
                            if (lumping)
                            {
                                indices.add(rowIndex, rowIndex);
                            }
                            else
                            {
                                int colIndex = aBasis_.index(*it, j);
                                indices.add(rowIndex, colIndex);
                            }
                        }
                    }
                }
            }
        }


        template <class LocalAssemblerType, class GlobalMatrixType, bool lumping>
        void addEntriesStaticLumping(LocalAssemblerType& localAssembler, GlobalMatrixType& A) const
        {
            //typedef typename GridView::template Codim<0>::Iterator ElementIterator;
            typedef typename LocalAssemblerType::LocalMatrix LocalMatrix;

            auto it = tBasis_.getGridView().template begin<0, Dune::Interior_Partition>();
            auto end = tBasis_.getGridView().template end<0, Dune::Interior_Partition>();
            for (; it != end; ++it)
            {
                // get shape functions
                const typename TrialBasis::LocalFiniteElement& tFE = tBasis_.getLocalFiniteElement(*it);
                const typename AnsatzBasis::LocalFiniteElement& aFE = aBasis_.getLocalFiniteElement(*it);

                LocalMatrix localA(tFE.localBasis().size(), aFE.localBasis().size());
                localAssembler.assemble(*it, localA, tFE, aFE);

                for (size_t i=0; i<tFE.localBasis().size(); ++i)
                {
                    int rowIndex = tBasis_.index(*it, i);
                    for (size_t j=0; j<aFE.localBasis().size(); ++j)
                    {

                        if (localA[i][j].infinity_norm()!=0.0)
                        {
                            if (lumping)
                            {
                                Arithmetic::addProduct(A[rowIndex][rowIndex], 1.0, localA[i][j]);
                            }
                            else
                            {
                                int colIndex = aBasis_.index(*it, j);
                                Arithmetic::addProduct(A[rowIndex][colIndex], 1.0, localA[i][j]);
                            }
                        }
                    }
                }
            }
        }


        const TrialBasis& tBasis_;
        const AnsatzBasis& aBasis_;
};

#endif

