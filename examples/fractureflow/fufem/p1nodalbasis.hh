#ifndef P1_NODALBASIS_HH
#define P1_NODALBASIS_HH

#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include "functionspacebasis.hh"



template <class GV, class RT=double>
class P1NodalBasis :
    public FunctionSpaceBasis<
        GV,
        RT,
        typename Dune::PQkLocalFiniteElementCache<typename GV::Grid::ctype, RT, GV::dimension, 1>::FiniteElementType >
{
    protected:
        typedef typename GV::Grid::ctype ctype;

        typedef typename Dune::PQkLocalFiniteElementCache<typename GV::Grid::ctype, RT, GV::dimension, 1> FiniteElementCache;
        typedef typename FiniteElementCache::FiniteElementType LFE;

        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;

        using Base::dim;
        using Base::gridview_;


    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;

        P1NodalBasis(const GridView& gridview) :
            Base(gridview)
        {}

        size_t size() const
        {
            return gridview_.indexSet().size(dim);
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            return cache_.get(e.type());
        }

        int index(const Element& e, const int i) const
        {
            return gridview_.indexSet().subIndex(e, getLocalFiniteElement(e).localCoefficients().localKey(i).subEntity(), dim);
        }

    protected:
        FiniteElementCache cache_;
};

#endif

