#ifndef BASIS_FUNCTION_INTERPOLATOR_HH
#define BASIS_FUNCTION_INTERPOLATOR_HH

#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/common/function.hh>

#include <dune/localfunctions/common/virtualinterface.hh>

#include "../solvers/arithmetic.hh"
#include "virtualgridfunction.hh"
//#include <dune/fufem/functionspacebases/conformingbasis.hh>

#include "basisgridfunction.hh"
#include "cachedcomponentwrapper.hh"



template<class F, class Grid, class Base>
class LocalFunctionComponentWrapper :
    public CachedComponentWrapper<
        LocalFunctionComponentWrapper<F, Grid, Base>,
        typename F::RangeType,
        Base>
{
    private:
        CachedComponentWrapper<LocalFunctionComponentWrapper<F, Grid, Base>, typename F::RangeType, Base> BaseClass;
        typedef typename Grid::template Codim<0>::Entity Element;
        typedef typename Element::Geometry Geometry;
    public:
        typedef typename F::RangeType AllRangeType;

        typedef typename Base::DomainType DomainType;
        typedef typename Base::RangeType RangeType;

        LocalFunctionComponentWrapper(const F& f, const Element& e, int comp) :
            BaseClass(comp),
            f_(f),
            e_(e),
            geometry_(e_.geometry())
        {}

        void evaluateAll(const DomainType& x, AllRangeType& y) const
        {
            typedef VirtualGridFunction<Grid, AllRangeType> GridFunction;

            const GridFunction* gf = dynamic_cast<const GridFunction*>(&f_);
            if (gf and gf->isDefinedOn(e_))
                gf->evaluateLocal(e_, x, y);
            else
                f_.evaluate(geometry_.global(x), y);
        }


    protected:
        const F& f_;
        const Element& e_;
        const Geometry geometry_;
};



/**
 * \brief Structure encapsulating the interpolation of a given function in a discrete function space
 *
 * \tparam B Global function space basis
 * \tparam RepresentationType Representation type (type of coefficient vector)
 * \tparam F Type of function to interpolate
 */
template <class B, class RepresentationType, class F>
struct BasisInterpolator
{
    typedef typename B::GridView GridView;
    typedef typename GridView::Grid Grid;
    typedef typename Grid::template Codim<0>::Entity Element;

    struct AllTrueBitSetVector
    {
            struct AllTrueBitSet
            {
                bool any() const { return true; }
            } allTrue_;

            const AllTrueBitSet& operator[](int i) const
            {
                return allTrue_;
        }
    };



    static void interpolate(const B& basis, RepresentationType& coeff, const F& f)
    {
        // If no BitVector is given use one where all entries are 'true' (statically, no memory used)
        AllTrueBitSetVector allTrue;
        interpolate(basis, coeff, f, allTrue);
    }



    /**
     * \brief Interpolate given function in discrete function space
     *
     * \tparam BitVector Type of BitVector for interpolateDOFFlags
     * 
     * \param basis Global function space basis of discrete function space
     * \param coeff Coefficient vector to represent the interpolation
     * \param f Function to interpolate
     * \param interpolateDOFFlags BitVector with flags to indicate if DOFs should be interpolated or not.
     *                            This may be scalar, vector valued or AllTrueBitSetVector
     */
    template <class BitVectorType>
    static void interpolate(const B& basis, RepresentationType& coeff, const F& f, const BitVectorType& interpolateDOFFlags)
    {
        typedef typename GridView::template Codim<0>::Iterator ElementIterator;
        typedef typename B::LocalFiniteElement LocalFiniteElement;

        typedef typename Dune::LocalFiniteElementFunctionBase<typename B::LocalFiniteElement>::type FunctionBaseClass;
        typedef LocalFunctionComponentWrapper<F, Grid, FunctionBaseClass> LocalWrapper;

        const GridView& gridview = basis.getGridView();

        if (coeff.size() != basis.size())
            coeff.resize(basis.size());

        typename Dune::BitSetVector<1> processed(basis.size(), false);
        for(size_t i=0; i<basis.size(); ++i)
        {
            if (interpolateDOFFlags[i].any())
            {
                if (basis.isConstrained(i))
                {
                    coeff[i] = 0;
                    processed[i][0] = true;
                }
            }
            else
                processed[i][0] = true;
        }

        std::vector<typename LocalWrapper::RangeType> interpolationValues;

        ElementIterator it = gridview.template begin<0>();
        ElementIterator end = gridview.template end<0>();
        for(; it != end; ++it)
        {
            auto&& element = *it;
            const LocalFiniteElement& fe = basis.getLocalFiniteElement(element);

            // check if all components have already been processed
            bool allProcessed = true;
            for(size_t i=0; i<fe.localBasis().size(); ++i)
                allProcessed = allProcessed and processed[basis.index(element, i)][0];

            if (not(allProcessed))
            {
                LocalWrapper fjLocal(f, element, 0);

                size_t lastComponent = Components<typename F::RangeType>::size(coeff[0])-1;
                for(size_t j=0; j<=lastComponent; ++j)
                {
                    fjLocal.setIndex(j);
                    fe.localInterpolation().interpolate(fjLocal, interpolationValues);

                    for(size_t i=0; i<fe.localBasis().size(); ++i)
                    {
                        size_t index = basis.index(element, i);

                        // check if DOF is unprocessed
                        if (not(processed[index][0]))
                        {
                            Components<typename F::RangeType>::setComponent(coeff[index], j, interpolationValues[i]);
                            if (j==lastComponent)
                                processed[index][0] = true;
                        }
                    }
                }
            }
        }
    }
};


#if 0
/**
 * \brief Specialization for interpolation of conforming basis grid function to nonconforming basis
 *
 * \tparam B Global function space basis
 * \tparam C Representation type (type of coefficient vector)
 */
template <class B, class C>
struct BasisInterpolator<B, C, BasisGridFunction<ConformingBasis<B>, C> >
{
    typedef BasisGridFunction<ConformingBasis<B>, C> F;


    /**
     * \brief Interpolate given function in discrete function space
     *
     * \param basis Global function space basis of discrete function space
     * \param coeff Coefficient vector to represent the interpolation
     * \param f Function to interpolate
     */
    static void interpolate(const B& basis, C& coeff, const F& f)
    {
        ConformingBasis<B> fBasis = f.basis();

        coeff = f.coefficientVector();

        for(size_t i=0; i<fBasis.size(); ++i)
        {
            if (fBasis.isConstrained(i))
            {
                coeff[i] = 0.0;
                for(size_t k=0; k<fBasis.constraints(i).size(); ++k)
                    Arithmetic::addProduct(coeff[i], fBasis.constraints(i)[k].factor, coeff[fBasis.constraints(i)[k].index]);
            }
        }
    }
};
#endif



namespace Functions
{

template <class B, class C, class F, class BF>
void interpolate(const B& basis, C& coeff, const F& f, const BF& bitField)
{
    BasisInterpolator<B, C, F>::interpolate(basis, coeff, f, bitField);
}

template <class B, class C, class F>
void interpolate(const B& basis, C& coeff, const F& f)
{
    BasisInterpolator<B, C, F>::interpolate(basis, coeff, f);
}

} // end namespace Functions

#endif

