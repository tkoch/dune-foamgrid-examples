#ifndef STATIC_MATRIX_TOOL_HH
#define STATIC_MATRIX_TOOL_HH

#include "dune/common/fmatrix.hh"
#include "dune/common/diagonalmatrix.hh"
#include "dune/istl/scaledidmatrix.hh"
#include "dune/istl/bcrsmatrix.hh"
#include "dune/istl/matrixindexset.hh"

#include "../solvers/arithmetic.hh"

namespace StaticMatrix
{
        // type promotion (smallest matrix that can hold the sum of two matrices *******************
        template<class MatrixA, class MatrixB>
        struct Promote
        {
            typedef Dune::FieldMatrix<typename MatrixA::field_type, MatrixA::rows, MatrixA::cols> Type;
        };

        template<class Matrix>
        struct Promote<Matrix, Matrix>
        {
            typedef Matrix Type;
        };

        template<typename FieldType, int n>
        struct Promote<Dune::FieldMatrix<FieldType,n,n>, Dune::DiagonalMatrix<FieldType,n> >
        {
            typedef Dune::FieldMatrix<FieldType,n,n> Type;
        };

        template<typename FieldType, int n>
        struct Promote<Dune::DiagonalMatrix<FieldType,n>, Dune::FieldMatrix<FieldType,n,n> >
        {
            typedef Dune::FieldMatrix<FieldType,n,n> Type;
        };

        template<typename FieldType, int n>
        struct Promote<Dune::DiagonalMatrix<FieldType,n>, Dune::ScaledIdentityMatrix<FieldType,n> >
        {
            typedef Dune::DiagonalMatrix<FieldType,n> Type;
        };

        template<typename FieldType, int n>
        struct Promote<Dune::ScaledIdentityMatrix<FieldType,n>, Dune::DiagonalMatrix<FieldType,n> >
        {
            typedef Dune::DiagonalMatrix<FieldType,n> Type;
        };


        // add scalar to matrix diagonal ***********************************************************
        template<class Matrix>
        static void addToDiagonal(Matrix& x, const typename Matrix::field_type a)
        {
            for(int i=0; i<Matrix::rows; ++i)
                x[i][i] += a;
        }

        template <typename FieldType, int n>
//        static void addToDiagonal(Dune::ScaledIdentityMatrix<FieldType,n>& x, const FieldType a)
        // the commented line should NOT be used as it may lead to ambiguities in which case the general method above will be used.
        // an example is the line taken from massassembler.hh (line 83 at the time):
        //   StaticMatrix::addToDiagonal(localMatrix[i][i], values[i] * zi);
        // where values[i] is of type FieldVector<FieldType,1> and zi is a double. This call then does not exactly fit this specialization (without the implicit cast of FV<FT,1>)
        // and hence some wild template voodoo decides which of the templates is to be taken - in this case with a gcc 4.4.5 that was the one above leading to a wrong factor n
        // in the diagonal value
        static void addToDiagonal(Dune::ScaledIdentityMatrix<FieldType,n>& x, const typename Dune::ScaledIdentityMatrix<FieldType,n>::field_type a)
        {
            x.scalar() += a;
        }


        // add scalar times matrix to matrix *******************************************************
        template<class MatrixA, class MatrixB>
        DUNE_DEPRECATED_MSG("Use Arithmetic::addProduct from arithmetic.hh instead.") static void axpy(MatrixA& A, const typename MatrixA::field_type a, const MatrixB& B)
        {
            Arithmetic::addProduct(A,a,B);
        }

        // add transformed matrix A += T1^t*B*T2 ******************************************************
        template<class MatrixA, class MatrixB, class TransformationMatrix, bool AisScalar, bool BisScalar, bool TisScalar>
        struct TransformMatrixHelper
        {
            static void addTransformedMatrix(MatrixA& A, const TransformationMatrix& T1, const MatrixB& B, const TransformationMatrix& T2)
            {
                for (size_t i=0; i<A.N(); ++i)
                {
                    typename MatrixA::row_type::Iterator jIt=A[i].begin();
                    typename MatrixA::row_type::Iterator jEnd=A[i].end();
                    for(; jIt!=jEnd; ++jIt)
                    {
                        for (size_t k=0; k<B.N(); ++k)
                        {
                            if (T1.exists(k,i))
                            {
                                typename MatrixB::row_type::ConstIterator lIt=B[k].begin();
                                typename MatrixB::row_type::ConstIterator lEnd=B[k].end();
                                for(; lIt!=lEnd; ++lIt)
                                    if (T2.exists(lIt.index(),jIt.index()))
                                        *jIt += T1[k][i] * (*lIt) * T2[lIt.index()][jIt.index()];
                            }
                        }
                    }
                }
            }

//            static void transformMatrix(MatrixA& A, const TransformationMatrix& T1, const MatrixB& B, const TransformationMatrix& T2)
//            {
//                A = 0.0;
//                for (int k=0; k<B.N(); ++k)
//                {
//                    typename MatrixB::row_type::ConstIterator lIt=B[k].begin();
//                    typename MatrixB::row_type::ConstIterator lEnd=B[k].end();
//                    for (; lIt!=lEnd; ++lIt)
//                    {
//                        typename TransformationMatrix::row_type::ConstIterator iIt=T1[k].begin();
//                        typename TransformationMatrix::row_type::ConstIterator iEnd=T1[k].end();
//                        for (; iIt!=iEnd; ++iIt)
//                        {
//                            typename TransformationMatrix::row_type::ConstIterator jIt=T2[lIt.index()].begin();
//                            typename TransformationMatrix::row_type::ConstIterator jEnd=T2[lIt.index()].end();
//                            for (; jIt!=jEnd; ++jIt)
//                                A[iIt.index()][jIt.index()] = (*iIt) * (*lIt) * (*jIt);
//                        }
//                    }
//                }
//            }
        };

        template<class K1, class K2, class K3, int n, int m>
        struct TransformMatrixHelper<Dune::FieldMatrix<K1,m,m>, Dune::FieldMatrix<K3,n,n>, Dune::FieldMatrix<K2,n,m>, false, false, false>
        {
            typedef Dune::FieldMatrix<K1, m, m> MatrixA;
            typedef Dune::FieldMatrix<K3, n, n> MatrixB;
            typedef Dune::FieldMatrix<K2, n, m> TransformationMatrix;

            static void addTransformedMatrix(MatrixA& A, const TransformationMatrix& T1, const MatrixB& B, const TransformationMatrix& T2)
            {
                Dune::FieldMatrix<K1, m, n> T1transposedB;
                T1transposedB = 0;
                for (size_t i=0; i<MatrixA::rows; ++i)
                {
                    for (size_t k=0; k<MatrixB::rows; ++k)
                    {
                        if (T1[k][i]!=0)
                            for (size_t l=0; l<MatrixB::cols; ++l)
                                T1transposedB[i][l] += T1[k][i] * B[k][l];
                    }
                }
                for (size_t k=0; k<TransformationMatrix::rows; ++k)
                {
                    for (size_t l=0; l<TransformationMatrix::cols; ++l)
                    {
                        if (T2[k][l]!=0)
                            for (size_t i=0; i<MatrixA::rows; ++i)
                                A[i][l] += T1transposedB[i][k] * T2[k][l];
                    }
                }
            }

//            static void transformMatrix(MatrixA& A, const TransformationMatrix& T1, const MatrixB& B, const TransformationMatrix& T2)
//            {
//                A = 0.0;
//                for (int k=0; k<B.N(); ++k)
//                    for (int l=0; l<B.M(); ++l)
//                        for (int i=0; i<T1.M(); ++i)
//                            for (int j=0; j<T2.M(); ++j)
//                                A[i][j] = T1[k][i] * B[k][l] * T2[l][j];
//            }
        };

        template<class MatrixA, class MatrixB, class ScalarTransform, bool AisScalar, bool BisScalar>
        struct TransformMatrixHelper<MatrixA, MatrixB, ScalarTransform, AisScalar, BisScalar, true>
        {
            static void addTransformedMatrix(MatrixA& A, const ScalarTransform& T1, const MatrixB& B, const ScalarTransform& T2)
            {
                Arithmetic::addProduct(A, T1*T2, B);
            }
        };

        template<class MatrixA, class ScalarB, class TransformationMatrix, bool AisScalar, bool TisScalar>
        struct TransformMatrixHelper<MatrixA, ScalarB, TransformationMatrix, AisScalar, true, TisScalar>
        {
            static void addTransformedMatrix(MatrixA& A, const TransformationMatrix& T1, const ScalarB& B, const TransformationMatrix& T2)
            {
                for (size_t k=0; k<TransformationMatrix::rows; ++k)
                {
                    typename TransformationMatrix::ConstColIterator Skj   = T2[k].begin();
                    typename TransformationMatrix::ConstColIterator SkEnd = T2[k].end();
                    for (; Skj!=SkEnd; ++Skj)
                    {
                        typename TransformationMatrix::ConstColIterator Tki   = T1[k].begin();
                        typename TransformationMatrix::ConstColIterator TkEnd = T1[k].end();
                        for (; Tki!=TkEnd; Tki++)
                            if (A.exists(Tki.index(),Skj.index()))
                                A[Tki.index()][Skj.index()] += (*Tki) * B * (*Skj);
                    }
                }
            }
        };

        template<class FieldType, int n, class ScalarB, class TransformationMatrix, bool AisScalar, bool TisScalar>
        struct TransformMatrixHelper<Dune::ScaledIdentityMatrix<FieldType,n>, ScalarB, TransformationMatrix, AisScalar, true, TisScalar>
        {
            typedef Dune::ScaledIdentityMatrix<FieldType,n> MatrixA;
            static void addTransformedMatrix(MatrixA& A, const TransformationMatrix& T1, const ScalarB& B, const TransformationMatrix& T2)
            {
                TransformMatrixHelper<FieldType, ScalarB, typename TransformationMatrix::field_type, true, true, true>::addTransformedMatrix(A.scalar(),T1.scalar(),B,T2.scalar());
            }
        };

        template<class ScalarA, class ScalarB, class ScalarTransform>
        struct TransformMatrixHelper<ScalarA, ScalarB, ScalarTransform, true, true, true>
        {
            static void addTransformedMatrix(ScalarA& A, const ScalarTransform& T1, const ScalarB& B, const ScalarTransform& T2)
            {
                A += T1*B*T2;
            }
        };

        template<class MatrixA, typename FieldType, int n, class TransformationMatrix>
        struct TransformMatrixHelper<MatrixA, Dune::DiagonalMatrix<FieldType,n>, TransformationMatrix, false, false, false>
        {
            static void addTransformedMatrix(MatrixA& A, const TransformationMatrix& T1, const Dune::DiagonalMatrix<FieldType,n>& B, const TransformationMatrix& T2)
            {
                for (size_t k=0; k<n; ++k)
                {
                    typename TransformationMatrix::ConstColIterator Skj   = T2[k].begin();
                    typename TransformationMatrix::ConstColIterator SkEnd = T2[k].end();
                    for (; Skj!=SkEnd; ++Skj)
                    {
                        typename TransformationMatrix::ConstColIterator Tki   = T1[k].begin();
                        typename TransformationMatrix::ConstColIterator TkEnd = T1[k].end();
                        for (; Tki!=TkEnd; Tki++)
                            if (A.exists(Tki.index(),Skj.index()))
                            {
                                A[Tki.index()][Skj.index()] += (*Tki) * B.diagonal(k) * (*Skj);
                            }
                    }
                }
            }
        };

        template<class MatrixA, typename FieldType, int n, class TransformationMatrix>
        struct TransformMatrixHelper<MatrixA, Dune::ScaledIdentityMatrix<FieldType,n>, TransformationMatrix, false, false, false>
        {
            static void addTransformedMatrix(MatrixA& A, const TransformationMatrix& T1, const Dune::ScaledIdentityMatrix<FieldType,n>& B, const TransformationMatrix& T2)
            {
                  TransformMatrixHelper<MatrixA, FieldType, TransformationMatrix, false, true, false>::addTransformedMatrix(A,T1,B.scalar(),T2);
            }
        };

        template <typename FieldType, int n, class TransformationMatrix>
        struct TransformMatrixHelper<Dune::DiagonalMatrix<FieldType,n>, Dune::DiagonalMatrix<FieldType,n>, TransformationMatrix, false, false, false>
        {
            static void addTransformedMatrix(Dune::DiagonalMatrix<FieldType,n>& A, const TransformationMatrix& T1, const Dune::DiagonalMatrix<FieldType,n>& B, const TransformationMatrix& T2)
            {
                for (int k=0; k<n; k++)
                {
                    typename TransformationMatrix::ConstColIterator Tki   = T1[k].begin();
                    typename TransformationMatrix::ConstColIterator TkEnd = T1[k].end();
                    for (; Tki!=TkEnd; ++Tki)
                        A.diagonal(Tki.index()) += (*Tki) * B.diagonal(k) * T2[k][Tki.index()];
                }
            }
        };

        template <typename FieldType, int n, class TransformationMatrix>
        struct TransformMatrixHelper<Dune::ScaledIdentityMatrix<FieldType,n>, Dune::ScaledIdentityMatrix<FieldType,n>, TransformationMatrix, false, false, false>
        {
            static void addTransformedMatrix(Dune::ScaledIdentityMatrix<FieldType,n>& A, const TransformationMatrix& T1, const Dune::ScaledIdentityMatrix<FieldType,n>& B, const TransformationMatrix& T2)
            {
                for (int k=0; k<n; k++)
                    A.scalar() += T1[k][0] * B.scalar() * T2[k][0];
            }
        };

        template <typename FieldType, int n>
        struct TransformMatrixHelper<Dune::ScaledIdentityMatrix<FieldType,n>, Dune::ScaledIdentityMatrix<FieldType,n>, Dune::ScaledIdentityMatrix<FieldType,n>, false, false, false>
        {
            static void addTransformedMatrix(Dune::ScaledIdentityMatrix<FieldType,n>& A, const Dune::ScaledIdentityMatrix<FieldType,n>& T1, const Dune::ScaledIdentityMatrix<FieldType,n>& B, const Dune::ScaledIdentityMatrix<FieldType,n>& T2)
            {
                TransformMatrixHelper<FieldType, FieldType, FieldType, true, true, true>::addTransformedMatrix(A.scalar(),T1.scalar(),B.scalar(),T2.scalar());
            }
        };

        template<class MatrixA, class MatrixB, class TransformationMatrix>
        void addTransformedMatrix(MatrixA& A, const TransformationMatrix& T1, const MatrixB& B, const TransformationMatrix& T2)
        {
            TransformMatrixHelper<MatrixA,MatrixB,TransformationMatrix,
                            Arithmetic::ScalarTraits<MatrixA>::isScalar, Arithmetic::ScalarTraits<MatrixB>::isScalar, Arithmetic::ScalarTraits<TransformationMatrix>::isScalar>
                        ::addTransformedMatrix(A,T1,B,T2);
        }

        template<class MatrixA, class MatrixB, class TransformationMatrix>
        void transformMatrix(MatrixA& A, const TransformationMatrix& T1, const MatrixB& B, const TransformationMatrix& T2)
        {
            A = 0;
            TransformMatrixHelper<MatrixA,MatrixB,TransformationMatrix,
                            Arithmetic::ScalarTraits<MatrixA>::isScalar, Arithmetic::ScalarTraits<MatrixB>::isScalar, Arithmetic::ScalarTraits<TransformationMatrix>::isScalar>
                        ::addTransformedMatrix(A,T1,B,T2);
        }

        template<class MatrixBlockA, class MatrixB, class TransformationMatrix>
        static void transformMatrix(Dune::BCRSMatrix<MatrixBlockA>& A, const TransformationMatrix& T1, const MatrixB& B, const TransformationMatrix& T2)
        {
            transformMatrixPattern(A, T1, B, T2);
            A = 0.0;
            for (int k=0; k<B.N(); ++k)
            {
                typename MatrixB::row_type::ConstIterator BklIt = B[k].begin();
                typename MatrixB::row_type::ConstIterator BklEnd = B[k].end();
                for (; BklIt!=BklEnd; ++BklIt)
                {
                    int l = BklIt.index();

                    typename TransformationMatrix::row_type::ConstIterator T1kiIt = T1[k].begin();
                    typename TransformationMatrix::row_type::ConstIterator T1kiEnd = T1[k].end();
                    for (; T1kiIt!=T1kiEnd; ++T1kiIt)
                    {
                        int i = T1kiIt.index();

                        typename TransformationMatrix::row_type::ConstIterator T2ljIt = T2[l].begin();
                        typename TransformationMatrix::row_type::ConstIterator T2ljEnd = T2[l].end();
                        for (; T2ljIt!=T2ljEnd; ++T2ljIt)
                        {
                            int j = T2ljIt.index();
                            MatrixBlockA Aij;
                            transformMatrix(Aij,*T1kiIt, *BklIt, *T2ljIt);
                            A[i][j] += Aij;
                        }
                    }
                }
            }
        }

        template<class MatrixBlockA, class MatrixB, class TransformationMatrix>
        static void transformMatrixPattern(Dune::BCRSMatrix<MatrixBlockA>& A, const TransformationMatrix& T1, const MatrixB& B, const TransformationMatrix& T2)
        {
            Dune::MatrixIndexSet indices(T1.M(), T2.M());
            for (int k=0; k<B.N(); ++k)
            {
                typename MatrixB::row_type::ConstIterator BklIt = B[k].begin();
                typename MatrixB::row_type::ConstIterator BklEnd = B[k].end();
                for (; BklIt!=BklEnd; ++BklIt)
                {
                    int l = BklIt.index();

                    typename TransformationMatrix::row_type::ConstIterator T1kiIt = T1[k].begin();
                    typename TransformationMatrix::row_type::ConstIterator T1kiEnd = T1[k].end();
                    for (; T1kiIt!=T1kiEnd; ++T1kiIt)
                    {
                        int i = T1kiIt.index();

                        typename TransformationMatrix::row_type::ConstIterator T2ljIt = T2[l].begin();
                        typename TransformationMatrix::row_type::ConstIterator T2ljEnd = T2[l].end();
                        for (; T2ljIt!=T2ljEnd; ++T2ljIt)
                        {
                            int j = T2ljIt.index();
                            indices.add(i, j);
                        }
                    }
                }
            }
            indices.exportIdx(A);
        }


        // compute v^T*A*v for an edge vector e = e_i - e_j with i!=j ******************************
        template<class Matrix>
        static typename Matrix::field_type simplexEdgeDiagonal(const Matrix& A, int i, int j)
        {
            return A[i][i] - A[i][j] - A[j][i] + A[j][j];
        }

        template <typename FieldType, int n>
        static FieldType simplexEdgeDiagonal(const Dune::DiagonalMatrix<FieldType,n>& A, int i, int j)
        {
            return A.diagonal(i) + A.diagonal(j);
        }

        template <typename FieldType, int n>
        static FieldType simplexEdgeDiagonal(const Dune::ScaledIdentityMatrix<FieldType,n>& A, int i, int j)
        {
            return 2*A.scalar();
        }



        // compute i-th row of A*v for an edge vector e = e_i - e_j with i!=j **********************
        template<class Matrix>
        static typename Matrix::field_type simplexEdgeResidual(const Matrix& A, int i, int j)
        {
            return A[i][i] - A[i][j];
        }

        template <typename FieldType, int n>
        static FieldType simplexEdgeResidual(const Dune::DiagonalMatrix<FieldType,n>& A, int i, int j)
        {
            return A.diagonal(i);
        }

        template <typename FieldType, int n>
        static FieldType simplexEdgeResidual(const Dune::ScaledIdentityMatrix<FieldType,n>& A, int i, int j)
        {
            return A.scalar();
        }




        // compute i-th row of A*v for an edge vector e = e_i - e_j with i!=j **********************
        template <class K>
        static Dune::FieldMatrix<K,1,1>& toMatrix(K& x)
        {
            return *(reinterpret_cast< Dune::FieldMatrix<K,1,1>* > (&x));
        }

        template <class K, int n, int m>
        static Dune::FieldMatrix<K,n,m>& toMatrix(Dune::FieldMatrix<K,n,m>& x)
        {
            return x;
        }

        template <class K, int n>
        static Dune::ScaledIdentityMatrix<K,n>& toMatrix(Dune::ScaledIdentityMatrix<K,n>& x)
        {
            return x;
        }

        template <class K, int n>
        static Dune::DiagonalMatrix<K,n>& toMatrix(Dune::DiagonalMatrix<K,n>& x)
        {
            return x;
        }

        template <class K>
        static Dune::FieldMatrix<K,1,1>& toMatrix(Dune::ScaledIdentityMatrix<K,1>& x)
        {
            return *(reinterpret_cast< Dune::FieldMatrix<K,1,1>* > (&x));
        }

        template <class K>
        static Dune::FieldMatrix<K,1,1>& toMatrix(Dune::DiagonalMatrix<K,1>& x)
        {
            return *(reinterpret_cast< Dune::FieldMatrix<K,1,1>* > (&x));
        }

        /** \brief Compute an LDL^T decomposition
         *
         * The methods computes a decomposition A=LDL^T of a given dense
         * symmetric matrix A such that L is lower triangular with all
         * diagonal elements equal to 1 and D is diagonal. If A is positive
         * definite then A=(LD^0.5)(LD^0.5)^T is the Cholesky decomposition.
         * However, the LDL^T decomposition does also work for indefinite
         * symmetric matrices and is more stable than the Cholesky decomposition
         * since no square roots are required.
         *
         * The method does only change the nontrivial entries of the given matrix
         * L and D, i.e., it does not set the trivial 0 and 1 entries.
         * Thus one can store both in a single matrix M and use
         * M as argument for L and D.
         *
         * The method can furthermore work in-place, i.e., it is safe to
         * use A as argument for L and D. In this case the entries of A
         * below and on the diagonal are changed to those to those of
         * L and D, respectively.
         *
         * \param A Matrix to be decomposed. Only the lower triangle is used.
         * \param L Matrix to store the lower triangle. Only entries below the diagonal are written.
         * \param D Matrix to store the diagonal. Only diagonal entries are written.
         */
        template<class SymmetricMatrix, class LowerTriangularMatrix, class DiagonalMatrix>
        static void ldlt(const SymmetricMatrix& A, LowerTriangularMatrix& L, DiagonalMatrix& D)
        {
          for(unsigned int i = 0; i < A.N(); ++i)
          {
            D[i][i] = A[i][i];
            for(unsigned int j = 0; j < i; ++j)
            {
              L[i][j] = A[i][j];
              for(unsigned int k = 0; k < j; ++k)
                L[i][j] -= L[i][k] * L[j][k] * D[k][k];
              L[i][j] /= D[j][j];
            }
            for(unsigned int k = 0; k < i; ++k)
              D[i][i] -= L[i][k]*L[i][k] * D[k][k];
          }
        }

        /** \brief Solve linear system using a LDL^T decomposition.
         *
         * The methods solves a linear system Mx=b where A is given
         * by a decomposition A=LDL^T. The method does only use
         * the values of L and D below and on the diagonal, respectively.
         * The 1 entries on the diagonal of L are not required.
         * If L and D are stored in a single matrix it is safe
         * the use this matrix as argument for both.
         *
         * Note that the solution vector must already have the correct size.
         *
         * \param L Matrix containing the lower triangle of the decomposition
         * \param D Matrix containing the diagonal of the decomposition
         * \param b Right hand side on the linear system
         * \param x Vector to store the solution of the linear system
         */
        template<class LowerTriangularMatrix, class DiagonalMatrix, class RhsVector, class SolVector>
        static void ldltSolve(const LowerTriangularMatrix& L, const DiagonalMatrix& D, const RhsVector& b, SolVector& x)
        {
          for(unsigned int i = 0; i < x.size(); ++i)
          {
            x[i] = b[i];
            for(unsigned int j = 0; j < i; ++j)
              x[i] -= L[i][j] * x[j];
          }
          for(unsigned int i = 0; i < x.size(); ++i)
            x[i] /= D[i][i];
          for(int i = x.size()-1; i >=0; --i)
          {
            for(unsigned int j = i+1; j < x.size(); ++j)
              x[i] -= L[j][i] * x[j];
          }
        }




}

#endif

