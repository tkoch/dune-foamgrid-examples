inputname = "brain99.txt"
inputname2 = "brainflow.txt"
inputname3 = "brainBC.txt"
outputname = "brain.dgf"

inputfile = open(inputname)
inputfile2 = open(inputname2)
inputfile3 = open(inputname3)
outputfile = open(outputname, 'w')

# parse input file 
vertices = []
simplices = []
velocities = []

micron = 1e-6
mm = 1e-3

# skip first two lines
for i in range(2): inputfile.next() 
for line in inputfile:
	#strip the endline character
	line = line.strip("\n")
	#get array with all numbers in a row
	line = line.split()
	#add vertices found in this line
	v0 = [float(line[5])*micron, float(line[6])*micron, float(line[7])*micron]
	v1 = [float(line[8])*micron, float(line[9])*micron, float(line[10])*micron]
	
	if v0 not in vertices: 
		vertices.append(v0)
	if v1 not in vertices:
		vertices.append(v1)
	#add simplcies with parameter
	simplices.append([vertices.index(v0), vertices.index(v1), float(line[3])*0.5*micron]) #get radius from diameter

#read velocities from file
for i in range(2): inputfile2.next() 
for line in inputfile2:
	#strip the endline character
	line = line.strip("\n")
	#get array with all numbers in a row
	line = line.split()
	#add vertices found in this line
	if(len(line)>=4):
		velocities.append(float(line[4])*mm)

vIdx = []
bFlag = []
#read boundary flags from file
for i in range(1): inputfile3.next() 
for line in inputfile3:
	#strip the endline character
	line = line.strip("\n")
	#get array with all numbers in a row
	line = line.split()
	#add vertices and flag found in this line
	if line[0] != "#":
		vIdx.append(int(line[0]))
		bFlag.append(int(line[1]))

idx = 0
# write DGF file
outputfile.write("DGF\n")
outputfile.write("Vertex\n")
outputfile.write("parameters 1\n") #BCflag:: 0: default 1: inflow 2: outflow
for vertex in vertices:
	for coord in vertex:
		outputfile.write(str(coord) + " ")
	if idx in vIdx:
		outputfile.write(str(bFlag[vIdx.index(idx)]) + " ")
	else:
		outputfile.write(str(0) + " ")
	outputfile.write("\n")
	idx = idx+1
outputfile.write("#\n")

outputfile.write("SIMPLEX\n")
outputfile.write("parameters 2\n")
for simplex in simplices:
	for idx in simplex:
		outputfile.write(str(idx) + " ")
	outputfile.write(str(velocities[simplices.index(simplex)]))
	outputfile.write("\n")
outputfile.write("#\n")

outputfile.write("BOUNDARYDOMAIN\ndefault 1\n#")












