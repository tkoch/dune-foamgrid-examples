// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase blood flow model with bifurcations:
 *        Blood is flowing through a 1d network grid with a bifurcation
 *        provided by dune-foamgrid.
 */
#ifndef DUMUX_BLOOD_FLOW_PROBLEM_HH
#define DUMUX_BLOOD_FLOW_PROBLEM_HH

#include <dumux/freeflow/1dstokes2c/1dstokes2cmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/cellcentered/variableccfvelementgeometry.hh>
#include <dumux/material/spatialparameters/1dstokesspatialparams.hh>
#include <dumux/material/fluidsystems/bloodfluidsystem.hh>
#include <dumux/linear/seqsolverbackend.hh>

// adaptivity
#include <dumux/freeflow/1dstokes2c/gridadaptionindicator1dstokes2c.hh>
#include <dumux/implicit/adaptive/gridadaptinitializationindicator.hh>

namespace Dumux
{
template <class TypeTag>
class BloodFlowProblem;

namespace Properties
{
NEW_TYPE_TAG(BloodFlowProblem, INHERITS_FROM(OneDStokesTwoC));
NEW_TYPE_TAG(BloodFlowCCProblem, INHERITS_FROM(CCModel, BloodFlowProblem));

// Set the fluid system
SET_TYPE_PROP(BloodFlowProblem, FluidSystem, Dumux::FluidSystems::BloodFluidSystem<TypeTag>);

// Set the fv geometry for network grids
SET_TYPE_PROP(BloodFlowProblem, FVElementGeometry, Dumux::VariableCCFVElementGeometry<TypeTag>);

// Enable velocity output
SET_BOOL_PROP(BloodFlowProblem, VtkAddVelocity, true);

// Set the grid type
SET_TYPE_PROP(BloodFlowProblem, Grid, Dune::FoamGrid<1, 3>);

// Set the problem property
SET_TYPE_PROP(BloodFlowProblem, Problem, Dumux::BloodFlowProblem<TypeTag>);

// Set the spatial parameters
SET_TYPE_PROP(BloodFlowProblem, SpatialParams, Dumux::OneDStokesSpatialParams<TypeTag>);

// Set a direct solver as the network structure leads to a difficult matrix structure
#if HAVE_UMFPACK
SET_TYPE_PROP(BloodFlowProblem, LinearSolver, UMFPackBackend<TypeTag>);
#endif

// Enable gravity
SET_BOOL_PROP(BloodFlowProblem, ProblemEnableGravity, false);

// Adaptivity
SET_BOOL_PROP(BloodFlowProblem, AdaptiveGrid, true);
SET_TYPE_PROP(BloodFlowProblem, AdaptionIndicator, GridAdaptionIndicatorOneDStokesTwoC<TypeTag>);
SET_TYPE_PROP(BloodFlowProblem, AdaptionInitializationIndicator, ImplicitGridAdaptInitializationIndicator<TypeTag>);
}

template <class TypeTag>
class BloodFlowProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    // copy some indices for convenience
    static const int dim = GridView::dimension;
    static const int dimworld = GridView::dimensionworld;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // indices of the equations
        conti0EqIdx = Indices::conti0EqIdx,
        transportEqIdx = Indices::transportEqIdx
    };
    enum {
        // indices of the primary variables
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
        pressureIdx = Indices::pressureIdx
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;
    typedef Dune::FieldVector<Scalar, dimworld> GlobalPosition;
    typedef typename Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

public:
    BloodFlowProblem(TimeManager &timeManager, const GridView &gridView) : ParentType(timeManager, gridView)
    {
        // initialize fluid system
        FluidSystem::init();

        // get some parameters from the input file
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        p_in_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryConditions, PressureInput);
        delta_p_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryConditions, DeltaPressure);
        x_in_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryConditions, MoleFraction);
        lp_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, VesselWall, Permeability)/
                GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, InterstitialFluid, Viscosity)/
                GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, VesselWall, Thickness);
        lc_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, VesselWall, DiffusionCoefficient)/
                GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, VesselWall, Thickness);
        sigma_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, VesselWall, ReflectionCoefficient);
        p3d_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Tissue, Pressure);
        x3d_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Tissue, MoleFraction);

        // stating in the console whether mole or mass fractions are used
        if(!useMoles)
            std::cout<<"This problem uses mass-fractions"<<std::endl;
        else
            std::cout<<"This problem uses mole-fractions"<<std::endl;

        // set spatial params grid meta data
        this->spatialParams().setParams();

        // create map from boundarySegmentIndex to boundaryFlag for boundary conditions
        setBoundaryDomainMarker_();
    }

    void postAdapt()
    {
        // update the spatial params
        this->spatialParams().setParams();
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 37.0; } // Body temperature

    /*!
     * \brief Return the sources within the domain.
     *
     * \param values Stores the source values, acts as return value
     * \param globalPos The global position
     */
    void sourceAtPos(PrimaryVariables &values,
                const GlobalPosition &globalPos) const
    {
        values = 0;
    }
    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param intersection The intersection for which the boundary type is set
     */
    void boundaryTypes(BoundaryTypes &values,
                       const Intersection &intersection) const
    {
        values.setAllDirichlet();
        // set neumann boundary condition at inflow segments
        if(boundaryDomainMarker_[intersection.boundarySegmentIndex()] == 1)
            values.setNeumann(conti0EqIdx);
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param intersection The intersection for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values,
                   const Intersection &intersection) const
    {
        int boundarySegmentIndex = intersection.boundarySegmentIndex();
        if(boundaryDomainMarker_[boundarySegmentIndex] == 1)
        {
            bool boundaryIsInflow = false;
            // read the vector of inflow segments from the input file
            auto inflowSegments = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, BoundaryConditions, InflowSegments);
            for(const Scalar& index : inflowSegments)
            {
                if (boundarySegmentIndex == index)
                {
                    boundaryIsInflow = true;
                    break;
                }
            }
            if(boundaryIsInflow)
                values[massOrMoleFracIdx] = x_in_;
            else
                values[massOrMoleFracIdx] = 0.0;
        }
        else if(boundaryDomainMarker_[boundarySegmentIndex] == 2)
            values[pressureIdx] = p_in_-delta_p_;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    using ParentType::neumann;
    void neumann(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &intersection,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        Scalar radius = this->spatialParams().radius(element);
        Scalar vel = this->spatialParams().velocity_estimate(element);
        int boundarySegmentIndex = intersection.boundarySegmentIndex();
        if(boundaryDomainMarker_[boundarySegmentIndex] == 1)
            priVars[conti0EqIdx] = -vel*M_PI*radius*radius;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The source and sink values for the conservation equations in
     * units of \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scvIdx The local subcontrolvolume index
     * \param elemVolVars All volume variables for the element
     *
     * For this method, the \a values parameter stores the rate mass
     * generated or annihilate per volume unit. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void solDependentSource(PrimaryVariables &values,
                     const Element &element,
                     const FVElementGeometry &fvGeometry,
                     const int scvIdx,
                     const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.0;

        // get current 1d pressure
        Scalar p1D = elemVolVars[scvIdx].pressure();
        Scalar c1D;
        if(useMoles)
            c1D = elemVolVars[scvIdx].moleFraction(massOrMoleFracIdx)*elemVolVars[scvIdx].molarDensity();
        else
            c1D = elemVolVars[scvIdx].massFraction(massOrMoleFracIdx)*elemVolVars[scvIdx].density();
        Scalar c3D = 0.0;

        // get the radius of the element
        const SpatialParams& spatialParams = this->spatialParams();
        Scalar radius = spatialParams.radius(element);

        values[pressureIdx] = 2*M_PI*radius*lp_*(p3d_ - p1D);
        values[massOrMoleFracIdx] = 2*M_PI*radius*lc_*(x3d_*elemVolVars[scvIdx].molarDensity() - c1D);

        if(values[pressureIdx] > 0) // 3d is upstream
            values[massOrMoleFracIdx] += (1.0-sigma_)*values[pressureIdx]*x3d_*elemVolVars[scvIdx].molarDensity();
        else // 1d is upstream
            values[massOrMoleFracIdx] += (1.0-sigma_)*values[pressureIdx]*c1D;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &priVars,
                        const GlobalPosition &globalPos) const
    {
        priVars[pressureIdx] = p_in_;
        priVars[massOrMoleFracIdx] = 0.0;
    }

    // \}

private:

    void setBoundaryDomainMarker_()
    {
        boundaryDomainMarker_.resize(GridCreator::grid().numBoundarySegments());
        const auto& gv = GridCreator::grid().levelGridView(0);

        for (auto&& element : elements(gv))
            for (auto&& intersection : intersections(gv, element))
                if (intersection.boundary())
                    if (GridCreator::parameters(element.template subEntity<1>(intersection.indexInInside())).size() > 0)
                        boundaryDomainMarker_[intersection.boundarySegmentIndex()] = GridCreator::parameters(element.template subEntity<1>(intersection.indexInInside()))[0];
    }

    std::string name_;

    Scalar p_in_;
    Scalar delta_p_;
    Scalar x_in_;
    Scalar lp_, lc_, sigma_;
    Scalar p3d_, x3d_;

    std::vector<int> boundaryDomainMarker_;
};
} //end namespace

#endif
