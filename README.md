The `dune-foamgrid-examples` module
===================================

A supplementary module containing examples using `dune-foamgrid`.
The examples are described in more detail in the publication
[O. Sander, T. Koch, N. Schröder, B. Flemisch, The Dune FoamGrid  implementation for surface and network grids, arXiv:1511.03415, 2015](http://arxiv.org/abs/1511.03415)

The `dune-foamgrid` module is an implementation of the `dune-grid` interface that implements one- and two-dimensional grids in a physical space of arbitrary dimension. The grids are not expected to have a manifold structure, i.e., more than two elements can share a common facet. This makes FoamGrid the grid data structure of choice for simulating structures such as foams, discrete fracture networks, or network flow problems.

Installation
------------

`dune-foamgrid-examples` requires the DUNE core modules, version 2.4. It further depends
on `dune-foamgrid` and `dumux`.

Please see the [general instructions for building DUNE modules](https://www.dune-project.org/doc/installation-notes.html) for detailed instructions on how to build the module.

`dune-foamgrid-examples` provides a one-click installation shell script. After cloning out `dune-foamgrid-examples`
(git clone https://users.dune-project.org/repositories/projects/dune-foamgrid-examples.git) just run the installation script (./dune-foamgrid-examples/bin/oneclick-install.sh). The executables for the examples can be found under dune-foamgrid-examples/build-cmake/examples/*.
