// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GRIDADAPTIONINDICATOR_1DSTOKES2C_HH
#define DUMUX_GRIDADAPTIONINDICATOR_1DSTOKES2C_HH

#include <dune/common/version.hh>

#include "1dstokes2cproperties.hh"

/**
 * @file
 * @brief  Class defining a standard, concentration dependent indicator for the 1d stokes 2c model
 */
namespace Dumux
{
/*!\ingroup TwoPModel
 * @brief  Class defining a standard, concentration dependent indicator for grid adaption
 *
 * \tparam TypeTag The problem TypeTag
 */
template<class TypeTag>
class GridAdaptionIndicatorOneDStokesTwoC
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum
    {
    	massOrMoleFracIdx = Indices::massOrMoleFracIdx,
		pressureIdx = Indices::pressureIdx
    };

public:
    /*! @brief Constructs a GridAdaptionIndicator instance
     *
     *  This standard indicator is based on the concentration gradient.
     *  It checks the local gradient compared to the maximum global gradient.
     *  The indicator is compared locally to a refinement/coarsening threshold to decide whether
     *  a cell should be marked for refinement or coarsening or should not be adapted.
     *
     * \param problem The problem object
     */
    GridAdaptionIndicatorOneDStokesTwoC (Problem& problem) : problem_(problem)
    {
        refinetol_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, GridAdapt, RefineTolerance);
        coarsentol_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, GridAdapt, CoarsenTolerance);
        minLevel_ = GET_PARAM_FROM_GROUP(TypeTag, int, GridAdapt, MinLevel);
        maxLevel_ = GET_PARAM_FROM_GROUP(TypeTag, int, GridAdapt, MaxLevel);
    }

    /*! \brief Calculates the indicator used for refinement/coarsening for each grid cell.
     *
     * This standard indicator is based on the concentration gradient.
     */
    void calculateIndicator()
    {
        // prepare an indicator for refinement
        if(indicatorVector_.size() != problem_.model().numDofs())
            indicatorVector_.resize(problem_.model().numDofs());

        indicatorVector_ = std::numeric_limits<Scalar>::lowest();
        Scalar globalMax = std::numeric_limits<Scalar>::lowest();
        Scalar globalMin = std::numeric_limits<Scalar>::max();

        // 1) calculate Indicator -> min, maxvalues
        // loop over all leaf-elements
        for (auto&& element : elements(problem_.gridView()))
        {
            // calculate minimum and maximum concentration
            // index of the current leaf-elements
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
        	int globalIdxI = problem_.elementMapper().index(element);
#else
        	int globalIdxI = problem_.elementMapper().map(element);
#endif

            Scalar concentrationI = problem_.model().curSol()[globalIdxI][massOrMoleFracIdx];

            globalMin = std::min(concentrationI, globalMin);
            globalMax = std::max(concentrationI, globalMax);

            // calculate refinement indicator in all cells
            for (auto&& intersection : intersections(problem_.gridView(), element))
            {
                // Only consider internal intersections
                if (intersection.neighbor())
                {
                    // Access neighbor
                    auto&& outside = intersection.outside();
                    int globalIdxJ = problem_.elementMapper().index(outside);

                    // Visit intersection only once
                    if (element.level() > outside.level() || (element.level() == outside.level() && globalIdxI < globalIdxJ))
                    {
                        Scalar concentrationJ = problem_.model().curSol()[globalIdxJ][massOrMoleFracIdx];

                        Scalar localdelta = std::abs(concentrationI - concentrationJ);
                        indicatorVector_[globalIdxI][0] = std::max(indicatorVector_[globalIdxI][0], localdelta);
                        indicatorVector_[globalIdxJ][0] = std::max(indicatorVector_[globalIdxJ][0], localdelta);
                    }
                }
            }
        }

        Scalar globaldelta = globalMax - globalMin;

        refineBound_ = refinetol_*globaldelta;
        coarsenBound_ = coarsentol_*globaldelta;
    }

    /*! \brief Indicator function for marking of grid cells for refinement
     *
     * Returns true if an element should be refined.
     *
     *  \param element A grid element
     */
    bool refine(const Element& element)
    {
        return (indicatorVector_[problem_.elementMapper().index(element)] > refineBound_ && element.level() < maxLevel_);
    }

    /*! \brief Indicator function for marking of grid cells for coarsening
     *
     * Returns true if an element should be coarsened.
     *
     *  \param element A grid element
     */
    bool coarsen(const Element& element)
    {
        return (indicatorVector_[problem_.elementMapper().index(element)] < coarsenBound_ && element.level() > minLevel_);
    }

    /*! \brief Initializes the adaption indicator class*/
    void init()
    {
        refineBound_ = 0.;
        coarsenBound_ = 0.;
        indicatorVector_.resize(problem_.model().numDofs());
    };

protected:
    Problem& problem_;
    Scalar refinetol_;
    Scalar coarsentol_;
    Scalar refineBound_;
    Scalar coarsenBound_;
    int minLevel_;
    int maxLevel_;
    Dune::BlockVector<Dune::FieldVector<Scalar, 1> > indicatorVector_;
};
}

#endif
