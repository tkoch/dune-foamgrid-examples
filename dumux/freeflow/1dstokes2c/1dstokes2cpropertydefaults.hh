// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup OneDStokesTwoCModel
 * \file
 *
 * \brief Defines some default values for the properties of the the
 *        single-phase, two-component fully implicit model.
 */

#ifndef DUMUX_1D_STOKES_2C_PROPERTY_DEFAULTS_HH
#define DUMUX_1D_STOKES_2C_PROPERTY_DEFAULTS_HH

#include "1dstokes2cproperties.hh"
#include "1dstokes2cmodel.hh"
#include "1dstokes2clocalresidual.hh"
#include "1dstokes2cvolumevariables.hh"
#include "1dstokes2cfluxvariables.hh"
#include "1dstokes2cindices.hh"

#include <dumux/material/spatialparameters/1dstokesspatialparams.hh>

namespace Dumux
{
// \{
namespace Properties
{
//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////


SET_INT_PROP(OneDStokesTwoC, NumEq, 2); //!< set the number of equations to 2
SET_INT_PROP(OneDStokesTwoC, NumPhases, 1); //!< The number of phases in the 1dstokes2c model is 1
SET_INT_PROP(OneDStokesTwoC, NumComponents, 2); //!< The number of components in the 1dstokes2c model is 2
SET_SCALAR_PROP(OneDStokesTwoC, Scaling, 1); //!< Scaling of the model is set to 1 by default
SET_BOOL_PROP(OneDStokesTwoC, UseMoles, true); //!< Define that mole fractions are used in the balance equations

//! define the local residual
SET_TYPE_PROP(OneDStokesTwoC, LocalResidual, OneDStokesTwoCLocalResidual<TypeTag>);

//! define the model
SET_TYPE_PROP(OneDStokesTwoC, Model, OneDStokesTwoCModel<TypeTag>);

//! define the VolumeVariables
SET_TYPE_PROP(OneDStokesTwoC, VolumeVariables, OneDStokesTwoCVolumeVariables<TypeTag>);

//! define the FluxVariables
SET_TYPE_PROP(OneDStokesTwoC, FluxVariables, OneDStokesTwoCFluxVariables<TypeTag>);

//! set default upwind weight to 1.0, i.e. fully upwind
SET_SCALAR_PROP(OneDStokesTwoC, ImplicitMassUpwindWeight, 1.0);

//! weight for the upwind mobility in the velocity calculation
SET_SCALAR_PROP(OneDStokesTwoC, ImplicitMobilityUpwindWeight, 1.0);

//! Set the indices used by the 1p2c model
SET_TYPE_PROP(OneDStokesTwoC, Indices, OneDStokesTwoCIndices<TypeTag>);
//! The spatial parameters to be employed.
//! Use OneDStokesSpatialParams by default.
SET_TYPE_PROP(OneDStokesTwoC, SpatialParams, OneDStokesSpatialParams<TypeTag>);

//! Set the phaseIndex per default to zero (important for two-phase fluidsystems).
SET_INT_PROP(OneDStokesTwoC, PhaseIdx, 0);

// enable velocity output by default
SET_BOOL_PROP(OneDStokesTwoC, VtkAddVelocity, true);

// enable gravity by default
SET_BOOL_PROP(OneDStokesTwoC, ProblemEnableGravity, false);
}
// \}
}

#endif

