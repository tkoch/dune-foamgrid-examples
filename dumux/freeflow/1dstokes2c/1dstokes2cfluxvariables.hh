// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        all fluxes of fluid phases over a face of a finite volume.
 *
 * This means pressure and mole-fraction gradients, phase densities at
 * the integration point, etc.
 *
 */
#ifndef DUMUX_1D_STOKES_2C_FLUX_VARIABLES_HH
#define DUMUX_1D_STOKES_2C_FLUX_VARIABLES_HH

#include "1dstokes2cproperties.hh"

#include <dumux/common/math.hh>
#include <dumux/common/valgrind.hh>

#include <dumux/freeflow/1dstokes/1dstokesfluxvariables.hh>

namespace Dumux
{

/*!
 * \ingroup OneDStokesTwoCModel
 * \ingroup ImplicitFluxVariables
 * \brief This template class contains the data which is required to
 *        calculate the fluxes of the fluid phases over a face of a
 *        finite volume for the one-phase, two-component model.
 *
 * This means pressure and mole-fraction gradients, phase densities at
 * the intergration point, etc.
 */
template <class TypeTag>
class OneDStokesTwoCFluxVariables : public OneDStokesFluxVariables<TypeTag>
{
    typedef OneDStokesFluxVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum { transportCompIdx = Indices::transportCompIdx };
    enum { phaseIdx = Indices::phaseIdx };

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename GridView::ctype CoordScalar;
    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;
    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimWorldMatrix;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;

    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

public:
    /*
     * \brief The constructor
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the fully implicit scheme
     * \param scvfIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    OneDStokesTwoCFluxVariables(const Problem &problem,
                          const Element &element,
                          const FVElementGeometry &fvGeometry,
                          const int faceIdx,
                          const ElementVolumeVariables &elemVolVars,
                          const bool onBoundary = false)
        : ParentType(problem, element, fvGeometry, faceIdx, elemVolVars, onBoundary), onBoundary_(onBoundary)
    {
        calculateGradients_(problem, element, elemVolVars);
        this->calculateNormalVelocity_(problem, element, elemVolVars);
        calculateDiffusiveFlux_(problem, element, elemVolVars);
    };

        /*!
     * \brief Return the diffusive flux over a face of a given phase.
     *
     * \param compIdx index of the component
     */
    const Scalar diffusiveFlux(const unsigned int compIdx) const
    { return diffusiveFlux_; }

    const Scalar concentrationAtFace() const
    { return ipConcentration_;}

protected:

    /*
     * \brief Calculation of the potential gradients
     *
     * \param problem The problem
     * \param element The finite element
     * \param elemVolVars The volume variables of the current element
     */
    void calculateGradients_(const Problem &problem,
                             const Element &element,
                             const ElementVolumeVariables &elemVolVars)
    {
        if(!this->onBoundary_)
        {
            // get number of flux approximation points for this face and init pressure vector
            std::vector<Scalar> pressures;
            pressures.reserve(this->face().numFap);
            std::vector<Scalar> concentrations;
            concentrations.reserve(this->face().numFap);
            // to calcluate the bifurcation point pressure we also need all the permeabilities
            std::vector<Scalar> permeabilities;
            permeabilities.reserve(this->face().numFap);
            std::vector<Scalar> diffCoefficients;
            diffCoefficients.reserve(this->face().numFap);

            // get spatialparams from problem (for the radius)
            const SpatialParams &spatialParams = problem.spatialParams();

            //the integration point pressure and concentration
            this->ipPressure_ = 0.0;
            ipConcentration_ = 0.0;
            Scalar weightP = 0.0;
            Scalar weightC = 0.0;
            for (int fapIdx = 0; fapIdx < this->face().numFap; ++fapIdx)
            {
                pressures.push_back(elemVolVars[this->face().fapIndices[fapIdx]].pressure());
                if(useMoles)
                    concentrations.push_back(elemVolVars[this->face().fapIndices[fapIdx]].moleFraction(transportCompIdx)*
                                             elemVolVars[this->face().fapIndices[fapIdx]].molarDensity());
                else
                    concentrations.push_back(elemVolVars[this->face().fapIndices[fapIdx]].massFraction(transportCompIdx)*
                                             elemVolVars[this->face().fapIndices[fapIdx]].density());

                // average permeability
                try { permeabilities.push_back(spatialParams.intrinsicPermeabilityAtPos(this->face().ipGlobal)); }
                catch (Dune::InvalidStateException &e) { permeabilities.push_back(spatialParams.intrinsicPermeability(element)); }

                // average diffusion coefficients
                diffCoefficients.push_back(elemVolVars[this->face().fapIndices[fapIdx]].diffCoeff());

                this->ipPressure_ += permeabilities[fapIdx]/this->face().fapDistances[fapIdx]*pressures[fapIdx];
                ipConcentration_ += diffCoefficients[fapIdx]/this->face().fapDistances[fapIdx]*concentrations[fapIdx];
                weightP += permeabilities[fapIdx]/this->face().fapDistances[fapIdx];
                weightC += diffCoefficients[fapIdx]/this->face().fapDistances[fapIdx];
            }
            this->ipPressure_ /= weightP;
            ipConcentration_ /= weightC;

            // index for the element volume variables of the neigbor we calculate the gradient to
            const VolumeVariables &volVarsJ = elemVolVars[this->face().j];

            // the pressure differnce
            this->gradP_ = -(this->ipPressure_ - volVarsJ.pressure())/this->face().fapDistances[1];

            if(useMoles)
                gradC_ = -(ipConcentration_ - volVarsJ.moleFraction(transportCompIdx)*volVarsJ.molarDensity())/this->face().fapDistances[1];
            else
                gradC_ = -(ipConcentration_ - volVarsJ.massFraction(transportCompIdx)*volVarsJ.density())/this->face().fapDistances[1];
        }
        else //on boundary
        {
            // index for the element volume variables
            const VolumeVariables &volVarsI = elemVolVars[this->face().i];
            const VolumeVariables &volVarsJ = elemVolVars[this->face().j];
            this->gradP_ = (volVarsJ.pressure() - volVarsI.pressure())/this->face().fapDistances[1];
            if(useMoles)
                gradC_ = (volVarsJ.moleFraction(transportCompIdx)*volVarsJ.molarDensity()
                            -volVarsI.moleFraction(transportCompIdx)*volVarsI.molarDensity())/this->face().fapDistances[1];
            else
                gradC_ = (volVarsJ.massFraction(transportCompIdx)*volVarsJ.density()
                            -volVarsI.massFraction(transportCompIdx)*volVarsI.density())/this->face().fapDistances[1];

            // make this->ipPressure_ available for interpolation purposes (e.g. L2-Norm calculation)
            this->ipPressure_ = elemVolVars[this->face().j].pressure();
            if(useMoles)
                ipConcentration_ = elemVolVars[this->face().j].moleFraction(transportCompIdx)*elemVolVars[this->face().j].molarDensity();
            else
                ipConcentration_ = elemVolVars[this->face().j].massFraction(transportCompIdx)*elemVolVars[this->face().j].density();
        }

        //TODO Implement gravity
        // correct the pressure gradient by the gravitational acceleration
        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
        {
            DUNE_THROW(Dune::NotImplemented, "Gravity");
        }
    }

    /*!
    * \brief Calculation of the effective diffusion coefficient.
    *
    *        \param problem The considered problem file
    *        \param element The considered element of the grid
    *        \param elemVolVars The parameters stored in the considered element
    */
    void calculateDiffusiveFlux_(const Problem &problem,
                                   const Element &element,
                                   const ElementVolumeVariables &elemVolVars)
    {
        diffusiveFlux_ = -elemVolVars[this->face().j].diffCoeff()*gradC_;
    }

    const bool onBoundary_;                     //!< Specifying whether we are currently on the boundary of the simulation domain
    Scalar diffusiveFlux_;                      //!< Diffusive flux fo the transported component
    Scalar gradC_;                              //!< Concentration gradient
    Scalar ipConcentration_ ;                   //!< Concentration on the face

};

} // end namespace

#endif
