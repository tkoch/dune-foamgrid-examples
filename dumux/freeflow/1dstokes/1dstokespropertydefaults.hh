// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup OneDStokesModel
 * \file
 *
 * \brief Defines the properties required for the one-phase fully implicit model.
 */
#ifndef DUMUX_1D_STOKES_PROPERTY_DEFAULTS_HH
#define DUMUX_1D_STOKES_PROPERTY_DEFAULTS_HH

#include <dumux/implicit/box/boxproperties.hh>

#include "1dstokesmodel.hh"
#include "1dstokesproperties.hh"
#include "1dstokeslocalresidual.hh"
#include "1dstokesvolumevariables.hh"
#include "1dstokesindices.hh"
#include "1dstokesfluxvariables.hh"

#include <dumux/material/fluidsystems/gasphase.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>
#include <dumux/material/components/nullcomponent.hh>
#include <dumux/material/fluidsystems/1pfluidsystem.hh>
#include <dumux/material/spatialparameters/1dstokesspatialparams.hh>

namespace Dumux
{
// \{

///////////////////////////////////////////////////////////////////////////
// default property values for the isothermal single phase model
///////////////////////////////////////////////////////////////////////////
namespace Properties {
SET_INT_PROP(OneDStokes, NumEq, 1); //!< set the number of equations to 1
SET_INT_PROP(OneDStokes, NumPhases, 1); //!< The number of phases in the 1p model is 1

//! The local residual function
SET_TYPE_PROP(OneDStokes,
              LocalResidual,
              OneDStokesLocalResidual<TypeTag>);

//! the Model property
SET_TYPE_PROP(OneDStokes, Model, OneDStokesModel<TypeTag>);

//! the VolumeVariables property
SET_TYPE_PROP(OneDStokes, VolumeVariables, OneDStokesVolumeVariables<TypeTag>);

//! the FluxVariables property
SET_TYPE_PROP(OneDStokes, FluxVariables, OneDStokesFluxVariables<TypeTag>);

//! The indices required by the isothermal single-phase model
SET_TYPE_PROP(OneDStokes, Indices, OneDStokesIndices);

//! The spatial parameters to be employed. 
//! Use OneDStokesSpatialParams by default.
SET_TYPE_PROP(OneDStokes, SpatialParams, OneDStokesSpatialParams<TypeTag>);

//! The fluid system to use by default
SET_PROP(OneDStokes, FluidSystem)
{ 
private:
    typedef typename GET_PROP_TYPE(TypeTag, Fluid) Fluid;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef FluidSystems::OneP<Scalar, Fluid> type;
};

SET_PROP(OneDStokes, Fluid)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::NullComponent<Scalar> > type;
};

// enable gravity by default
SET_BOOL_PROP(OneDStokes, ProblemEnableGravity, true);

// disable velocity output by default
SET_BOOL_PROP(OneDStokes, VtkAddVelocity, false);

// \}
} // end namespace Properties

} // end namespace Dumux

#endif
