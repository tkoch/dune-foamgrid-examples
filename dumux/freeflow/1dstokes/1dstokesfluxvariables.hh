// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        the fluxes of the Stokes model over a face of a finite volume.
 *
 * This means pressure gradients, phase densities at the integration point, etc.
 */
#ifndef DUMUX_1D_STOKES_FLUX_VARIABLES_HH
#define DUMUX_1D_STOKES_FLUX_VARIABLES_HH

#include "1dstokesproperties.hh"
#include <cmath>
#include <dumux/common/parameters.hh>

namespace Dumux
{

namespace Properties
{
// forward declaration of properties
NEW_PROP_TAG(SpatialParams);
NEW_PROP_TAG(ProblemEnableGravity);
}

/*!
 * \ingroup OneDStokesModel
 * \ingroup ImplicitFluxVariables
 * \brief This template class contains the data which is required to
 *        calculate the mass and momentum fluxes over the face of a
 *        sub-control volume for the Stokes model.
 *
 * This means pressure gradients, phase densities, viscosities, etc.
 * at the integration point of the sub-control-volume face.
 */
template <class TypeTag>
class OneDStokesFluxVariables
{
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    enum { dim = GridView::dimension};
    enum { dimWorld = GridView::dimensionworld};
    enum { phaseIdx = 0};

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;

public:
    /*
     * \brief The constructor
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param faceIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    OneDStokesFluxVariables(const Problem &problem,
                        const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int faceIdx,
                        const ElementVolumeVariables &elemVolVars,
                        const bool onBoundary = false)
        : fvGeometry_(fvGeometry), faceIdx_(faceIdx), onBoundary_(onBoundary)
    {
        calculateGradients_(problem, element, elemVolVars);
        calculateNormalVelocity_(problem, element, elemVolVars);
    };

public:
    /*!
     * \brief Return the volumetric flux over a face of a given phase.
     *
     *        This is the calculated velocity multiplied by the unit normal
     *        and the area of the face.
     *        face().normal
     *        has already the magnitude of the area.
     *
     * \param phaseIdx index of the phase
     */
    const Scalar volumeFlux(const unsigned int phaseIdx) const
    {
        assert (phaseIdx == 0);
        return volumeFlux_;
    }

    /*!
     * \brief Return the local index of the downstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int downstreamIdx(const unsigned phaseIdx) const
    { return downstreamIdx_; }

    /*!
     * \brief Return the local index of the upstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int upstreamIdx(const unsigned phaseIdx) const
    { return upstreamIdx_; }

    /*!
     * \brief Return the SCV (sub-control-volume) face. This may be either
     *        a face within the element or a face on the element boundary,
     *        depending on the value of onBoundary_.
     */
    const SCVFace &face() const
    {
        if (onBoundary_)
            return fvGeometry_.boundaryFace[faceIdx_];
        else
            return fvGeometry_.subContVolFace[faceIdx_];
    }

    const Scalar pressureAtFace() const
    { return ipPressure_;}

protected:
    /*
     * \brief Calculation of the potential gradients
     *
     * \param problem The problem
     * \param element The finite element
     * \param elemVolVars The volume variables of the current element
     */
    void calculateGradients_(const Problem &problem,
                             const Element &element,
                             const ElementVolumeVariables &elemVolVars)
    {
        if(!onBoundary_)
        {
            // get number of flux approximation points for this face and init pressure vector
            std::vector<Scalar> pressures;
            pressures.reserve(face().numFap);
            // to calcluate the integration point pressure we also need all the permeabilities
            std::vector<Scalar> permeabilities;
            permeabilities.reserve(face().numFap);
            // get spatialparams from problem
            const SpatialParams &spatialParams = problem.spatialParams();
            // calculate the integration point pressure
            ipPressure_ = 0.0;
            Scalar weight = 0.0;
            for (int fapIdx = 0; fapIdx < face().numFap; ++fapIdx)
            {
                pressures.push_back(elemVolVars[face().fapIndices[fapIdx]].pressure());
                try { permeabilities.push_back(spatialParams.intrinsicPermeabilityAtPos(face().ipGlobal)); }
                catch (Dune::InvalidStateException &e) { permeabilities.push_back(spatialParams.intrinsicPermeability(element)); }
                ipPressure_ += permeabilities[fapIdx]/face().fapDistances[fapIdx]*pressures[fapIdx];
                weight += permeabilities[fapIdx]/face().fapDistances[fapIdx];
            }
            ipPressure_ /= weight;

            const VolumeVariables &volVarsJ = elemVolVars[face().j];
            // calculate the pressure gradient
            gradP_ = -(ipPressure_ - volVarsJ.pressure())/face().fapDistances[1];
        }
        else //on boundary
        {
            const VolumeVariables &volVarsI = elemVolVars[face().i];
            const VolumeVariables &volVarsJ = elemVolVars[face().j];
            // calculate the gradient
            gradP_ = (volVarsJ.pressure() - volVarsI.pressure())/face().fapDistances[1];

            // make ipPressure available for interpolation purposes (e.g. L2-Norm calculation)
            ipPressure_ = elemVolVars[face().j].pressure();
        }

        //TODO Implement gravity
        // correct the pressure gradient by the gravitational acceleration
        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
        {
            DUNE_THROW(Dune::NotImplemented, "Gravity");
        }
    }

    /*
     * \brief Actual calculation of the normal Stokes mean velocities.
     *
     * \param problem The problem
     * \param element The finite element
     * \param elemVolVars The volume variables of the current element
     */
    void calculateNormalVelocity_(const Problem &problem,
                                  const Element &element,
                                  const ElementVolumeVariables &elemVolVars)
    {
        //get permeability of the neighbor element
        const SpatialParams &spatialParams = problem.spatialParams();

        //get permeabily and distance from the neighbor element to calculate the velocity
        try { volumeFlux_ = -1.0*spatialParams.intrinsicPermeabilityAtPos(face().ipGlobal)/elemVolVars[face().j].dynamicViscosity()*gradP_; }
        catch (Dune::InvalidStateException &e) { volumeFlux_ = -1.0*spatialParams.intrinsicPermeability(*fvGeometry_.neighbors[face().j])/elemVolVars[face().j].dynamicViscosity()*gradP_; }

        // set the upstream and downstream vertices
        upstreamIdx_ = face().i;
        downstreamIdx_ = face().j;

        if (volumeFlux_ < 0)
            std::swap(upstreamIdx_, downstreamIdx_);
    }

    const FVElementGeometry &fvGeometry_;       //!< Information about the geometry of discretization
    const unsigned int faceIdx_;                //!< The index of the sub control volume face
    const bool onBoundary_;                     //!< Specifying whether we are currently on the boundary of the simulation domain
    unsigned int upstreamIdx_ , downstreamIdx_; //!< local index of the upstream / downstream vertex
    Scalar volumeFlux_ ;                        //!< Velocity multiplied with normal (magnitude=area)
    Scalar gradP_ ;                             //!< Pressure gradient
    Scalar ipPressure_ ;                        //!< Pressure on the face
};
} // end namespace

#endif
