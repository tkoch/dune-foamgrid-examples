// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Base class for all models which use the one-phase,
 *        fully implicit model.
 *        Adaption of the fully implicit scheme to the one-phase flow model.
 */

#ifndef DUMUX_1D_STOKES_MODEL_HH
#define DUMUX_1D_STOKES_MODEL_HH

#include "1dstokesproperties.hh"

namespace Dumux
{
/*!
 * \ingroup OnePModel
 * \brief A single-phase, isothermal flow model using the fully implicit scheme.
 *
 * Single-phase, isothermal flow model, which uses a standard Darcy approach as the
 * equation for the conservation of momentum:
 * \f[
 v = - \frac{\textbf K}{\mu}
 \left(\textbf{grad}\, p - \varrho {\textbf g} \right)
 * \f]
 *
 * and solves the mass continuity equation:
 * \f[
 \phi \frac{\partial \varrho}{\partial t} + \text{div} \left\lbrace
 - \varrho \frac{\textbf K}{\mu} \left( \textbf{grad}\, p -\varrho {\textbf g} \right) \right\rbrace = q,
 * \f]
 * All equations are discretized using a vertex-centered finite volume (box)
 * or cell-centered finite volume scheme as spatial
 * and the implicit Euler method as time discretization.
 * The model supports compressible as well as incompressible fluids.
 */
template<class TypeTag >
class OneDStokesModel : public GET_PROP_TYPE(TypeTag, BaseModel)
{
    typedef typename GET_PROP_TYPE(TypeTag, BaseModel) ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    enum { dim = GridView::dimension };
    enum { dimWorld = GridView::dimensionworld };

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

public:

    /*!
     * \brief \copybrief Dumux::ImplicitModel::addOutputVtkFields
     *
     * Specialization for the One1DStokesModel, adding the pressure and
     * the process rank to the VTK writer.
     */
    template<class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol,
                            MultiWriter &writer)
    {
        typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;
        bool velocityOutput = GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, AddVelocity);
        // create the required scalar fields
        unsigned numDofs = this->numDofs();
        ScalarField *p = writer.allocateManagedBuffer(numDofs);
        ScalarField *R = writer.allocateManagedBuffer(numDofs);
        ScalarField *velocity = writer.allocateManagedBuffer(numDofs);

        unsigned numElements = this->gridView_().size(0);
        ScalarField *rank = writer.allocateManagedBuffer(numElements);

        ElementIterator eIt = this->gridView_().template begin<0>();
        ElementIterator eEndIt = this->gridView_().template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                int eIdx = this->problem_().model().elementMapper().index(*eIt);
                (*rank)[eIdx] = this->gridView_().comm().rank();

                int globalIdx = this->dofMapper().subIndex(*eIt, 0, dofCodim);
                const SpatialParams &spatialParams = this->problem_().spatialParams();

                (*p)[globalIdx] = sol[eIdx][Indices::pressureIdx];
                (*R)[globalIdx] = spatialParams.radius(*eIt);

                if(velocityOutput)
                {
                    FVElementGeometry fvGeometry;
                    fvGeometry.update(this->gridView_(), *eIt);

                    ElementVolumeVariables elemVolVars;
                    elemVolVars.update(this->problem_(),
                                       *eIt,
                                       fvGeometry,
                                       false /* oldSol? */);
                    (*velocity)[globalIdx] = calculateVelocity(elemVolVars, fvGeometry, *eIt);
                }
            }
        }

        writer.attachDofData(*p, "p", isBox);
        writer.attachDofData(*R, "radius", isBox);
        if(velocityOutput) writer.attachDofData(*velocity,  "velocity", isBox);
        writer.attachCellData(*rank, "process rank");
    }

    Scalar calculateVelocity(const ElementVolumeVariables& elemVolVars,
                                const FVElementGeometry& fvGeometry,
                                const Element& element)
    {
        // fluxes for every face
        Dune::FieldVector<Scalar, dim<<1 > fluxes(0.0);
        int innerFaceIdx = 0;
        const IntersectionIterator isEndIt = this->problem_().gridView().iend(element);
        for (IntersectionIterator isIt = this->problem_().gridView().ibegin(element); isIt != isEndIt; ++isIt)
        {
            int faceIdx = isIt->indexInInside();
            if (isIt->neighbor())
            {
                FluxVariables fluxVars(this->problem_(),
                                        element,
                                        fvGeometry,
                                        innerFaceIdx,
                                        elemVolVars);
                fluxes[faceIdx] += fluxVars.volumeFlux(0);
                innerFaceIdx++;
            }
            else if (isIt->boundary())
            {
                FluxVariables fluxVars(this->problem_(),
                                        element,
                                        fvGeometry,
                                        faceIdx,
                                        elemVolVars,true);
                fluxes[faceIdx] += fluxVars.volumeFlux(0);
            }
        }
        const Scalar &radius = this->problem_().spatialParams().radius(element);
        return 0.5*(std::abs(fluxes[0]) + std::abs(fluxes[1]))/(M_PI*radius*radius);
    }
};
}

#include "1dstokespropertydefaults.hh"

#endif
