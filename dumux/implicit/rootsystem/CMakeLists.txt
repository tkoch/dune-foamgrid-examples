
install(FILES
        rootsystemfluxvariables.hh
        rootsystemindices.hh
        rootsystemlocalresidual.hh
        rootsystemmodel.hh
        rootsystemnewtoncontroller.hh
        rootsystemproblem.hh
        rootsystemproperties.hh
        rootsystempropertydefaults.hh
        rootsystemspatialparamsGrowth.hh
        rootsystemspatialparams.hh
        rootsystemvolumevariables.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/implicit/rootsystem)
