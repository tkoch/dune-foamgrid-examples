// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate
 *        all fluxes in a rootsystem over a face.
 *
 * This means pressure and temperature gradients, phase densities at
 * the integration point, etc.
 */
#ifndef DUMUX_ROOTSYSTEM_FLUX_VARIABLES_HH
#define DUMUX_ROOTSYSTEM_FLUX_VARIABLES_HH

#include <dumux/common/parameters.hh>
#include <dumux/common/math.hh>

namespace Dumux
{
    
namespace Properties
{
// forward declaration of properties 
NEW_PROP_TAG(ImplicitMobilityUpwindWeight);
NEW_PROP_TAG(SpatialParams);
NEW_PROP_TAG(NumPhases);
NEW_PROP_TAG(ProblemEnableGravity);
}   

/*!
 * \ingroup RootSystemFluxVariables
 * \brief Evaluates the normal component of the Darcy velocity 
 * on a (sub)control volume face.
 */
template <class TypeTag>
class RootsystemFluxVariables
{
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
     typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;

    enum { dim = GridView::dimension} ;
    enum { dimWorld = GridView::dimensionworld} ;
    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases)} ;
    enum { phaseIdx = Indices::phaseIdx };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldMatrix<Scalar, dimWorld, dimWorld> DimMatrix;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

public:
    /*
     * \brief The constructor
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param faceIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    RootsystemFluxVariables(const Problem &problem,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int faceIdx,
                 const ElementVolumeVariables &elemVolVars,
                 const bool onBoundary = false)
    : fvGeometry_(fvGeometry), faceIdx_(faceIdx), onBoundary_(onBoundary)
    {
        calculateGradients_(problem, element, elemVolVars);
        calculateNormalVelocity_(problem, element, elemVolVars);
    }

public:
    /*!
     * \brief Return the volumetric flux over a face of a given phase.
     *
     *        This is the calculated velocity multiplied by the unit normal
     *        and the area of the face.
     *        face().normal
     *        has already the magnitude of the area.
     *
     * \param phaseIdx index of the phase
     */
    Scalar volumeFlux(const unsigned int phaseIdx) const
    { return volumeFlux_; }
    
    /*!
     * \brief Return the velocity of a given phase.
     *
     *        This is the full velocity vector on the
     *        face (without being multiplied with normal).
     *
     * \param phaseIdx index of the phase
     */
    GlobalPosition velocity(const unsigned int phaseIdx) const
    { return velocity_[phaseIdx] ; }

    /*!
     * \brief Return intrinsic permeability multiplied with potential
     *        gradient multiplied with normal.
     *        I.e. everything that does not need upwind decisions.
     *
     * \param phaseIdx index of the phase
     */
    Scalar kGradPNormal(const unsigned int phaseIdx) const
    { return kGradPNormal_[phaseIdx] ; }

    /*!
     * \brief Return the local index of the downstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int downstreamIdx(const unsigned phaseIdx) const
    { return downstreamIdx_[phaseIdx]; }
    
    /*!
     * \brief Return the local index of the upstream control volume
     *        for a given phase.
     *
     * \param phaseIdx index of the phase
     */
    const unsigned int upstreamIdx(const unsigned phaseIdx) const
    { return upstreamIdx_[phaseIdx]; }

    /*!
     * \brief Return the SCV (sub-control-volume) face. This may be either
     *        a face within the element or a face on the element boundary,
     *        depending on the value of onBoundary_.
     */
    const SCVFace &face() const
    {
        if (onBoundary_)
            return fvGeometry_.boundaryFace[faceIdx_];
        else
            return fvGeometry_.subContVolFace[faceIdx_];
    }

protected:
    
    /*
     * \brief Calculation of the potential gradients
     *
     * \param problem The problem
     * \param element The finite element
     * \param elemVolVars The volume variables of the current element
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    void calculateGradients_(const Problem &problem,
                             const Element &element,
                             const ElementVolumeVariables &elemVolVars)
    {             
        const SpatialParams &spatialParams = problem.spatialParams();

        if(!onBoundary_)
        {
        	// get number of flux approximation points for this face and init pressure vector
        	std::vector<Scalar> pressures;
            pressures.reserve(face().numFap);
            // to calcluate the bifurcation point pressure we also need all the permeabilities
            std::vector<Scalar> permeabilities;
            permeabilities.reserve(face().numFap);
            // get spatialparams from problem
            for (int fapIdx = 0; fapIdx < face().numFap; ++fapIdx)
        	{
        		pressures.push_back(elemVolVars[face().fapIndices[fapIdx]].pressure());
                permeabilities.push_back(spatialParams.Kx(element, fvGeometry_, 0));
        	}
  		
        	// initialize the integration point pressure
        	ipPressure_ = 0.0;
        	ipPerm_ = 0.0;
        	Scalar weight = 0.0;
        	for (int fapIdx = 0; fapIdx < face().numFap; ++fapIdx)
        	{
        		ipPressure_ += permeabilities[fapIdx]/face().fapDistances[fapIdx]*pressures[fapIdx];
        		weight += permeabilities[fapIdx]/face().fapDistances[fapIdx];
        		ipPerm_ = spatialParams.Kx(element, fvGeometry_, 0);

        	}
        	ipPressure_ /= weight;

        	// index for the element volume variables of the neigbor we calculate the gradient to
        	const VolumeVariables &volVarsJ = elemVolVars[face().j];

        	// the pressure difference
        	gradP_ = -(ipPressure_ - volVarsJ.pressure())/face().fapDistances[1];
            //std::cout <<"ipPerm_: "<<ipPerm_ << "gradP_: "<< gradP_ <<std::endl;
        }
        else //on boundary
        {
        	// index for the element volume variables 
        	const VolumeVariables &volVarsI = elemVolVars[face().i];
        	const VolumeVariables &volVarsJ = elemVolVars[face().j];
        	gradP_ = (volVarsJ.pressure() - volVarsI.pressure())/face().fapDistances[1];

            // make ipPressure available for interpolation purposes (e.g. L2-Norm calculation)
            ipPressure_ = volVarsJ.pressure();
            ipPerm_ = spatialParams.Kx(element, fvGeometry_, 0);
        }
        
        //TODO Implement gravity
        // correct the pressure gradient by the gravitational acceleration
        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
        {
            DUNE_THROW(Dune::NotImplemented, "Gravity");
        } 

    }
    /*
     * \brief Actual calculation of the normal Darcy velocities.
     *
     * \param problem The problem
     * \param element The finite element
     * \param elemVolVars The volume variables of the current element
     */
    void calculateNormalVelocity_(const Problem &problem,
                                  const Element &element,
                                  const ElementVolumeVariables &elemVolVars)
    {
        //get permeability of the neighbor element

        //get permeabily and distance from the neighbor element to calculate the velocity
        volumeFlux_ = -ipPerm_*gradP_;
    }

    const FVElementGeometry &fvGeometry_;   //!< Information about the geometry of discretization
    const unsigned int faceIdx_;            //!< The index of the sub control volume face
    const bool      onBoundary_;                //!< Specifying whether we are currently on the boundary of the simulation domain
    unsigned int    upstreamIdx_[numPhases] , downstreamIdx_[numPhases]; //!< local index of the upstream / downstream vertex
    Scalar          volumeFlux_ ;    //!< Velocity multiplied with normal (magnitude=area)
    GlobalPosition  velocity_[numPhases] ;      //!< The velocity as determined by Darcy's law or by the Forchheimer relation
    Scalar          kGradPNormal_[numPhases] ;  //!< Permeability multiplied with gradient in potential, multiplied with normal (magnitude=area)
    GlobalPosition  kGradP_[numPhases] ; //!< Permeability multiplied with gradient in potential
    GlobalPosition  potentialGrad_[numPhases] ; //!< Gradient of potential, which drives flow
    Scalar          mobilityUpwindWeight_;      //!< Upwind weight for mobility. Set to one for full upstream weighting
    Scalar gradP_ ;                             //!< Pressure difference
    Scalar ipPressure_ ;                        //!< Pressure on the face
    Scalar ipPerm_ ;                        //!< Permeability on the face

};

} // end namespace

#endif
