// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters of problems using the
 *        fv method for 1d stokes problems.
 */
#ifndef DUMUX_1D_STOKES_SPATIAL_PARAMS_HH
#define DUMUX_1D_STOKES_SPATIAL_PARAMS_HH

#include <dumux/common/propertysystem.hh>
#include <dumux/common/math.hh>

#include <dumux/common/basicproperties.hh>

#include <dune/common/fmatrix.hh>

namespace Dumux
{
// forward declation of property tags
namespace Properties
{
NEW_PROP_TAG(SpatialParams);
}


/*!
 * \ingroup SpatialParameters
 */

/**
 * \brief The base class for spatial parameters of problems using the
 *        fv method.
 */
template<class TypeTag>
class OneDStokesSpatialParams
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;

    enum
    {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    OneDStokesSpatialParams(const GridView &gridView) : gridView_(gridView) {}

    ~OneDStokesSpatialParams() {}

    void setParams()
    {
        int numElems = gridView_.size(0);
        radius_.resize(numElems);
        velocity_estimate_.resize(numElems);

        //gridView_ is a leafGridView. Parameters are only set on level 0.
        //Elements have to inherit spatial parameters from their father.
        for (auto&& element : elements(gridView_))
        {
            Element level0element = element;
            for(int levelIdx = element.level(); levelIdx != 0; levelIdx--)
                level0element = level0element->father();

            int elemIdx = gridView_.indexSet().index(element);
            radius_[elemIdx] = GridCreator::parameters(level0element)[0];
            velocity_estimate_[elemIdx] = GridCreator::parameters(level0element)[1];
        }
        parametersAvailable_ = true;
    }

    /*!
     * \brief Function for defining the radius.
     *
     * \return radius
     * \param element The element
     */
    Scalar radius(const Element& element) const
    {
        if(parametersAvailable_)
        {
            int eIdx = gridView_.indexSet().index(element);
            return radius_[eIdx];
        }
        else
            return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Radius);
    }

    /*!
     * \brief Function for defining the radius.
     *
     * \return radius
     * \param element The element
     */
    Scalar radius(int eIdx) const
    {
        if(parametersAvailable_)
            return radius_[eIdx];
        else
            return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Radius);
    }

    /*!
     * \brief Function for defining the velocity estimate.
     *
     * \return velocity estimate
     * \param element The element
     */
    Scalar velocity_estimate(const Element& element) const
    {
        if(parametersAvailable_)
        {
            int eIdx = gridView_.indexSet().index(element);
            return velocity_estimate_[eIdx];
        }
        else
            return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, VelocityEstimate);
    }

    /*!
     * \brief Function for defining the intrinsic (absolute) permeability.
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return the intrinsic permeability
     */
    const Scalar intrinsicPermeability (const Element& element) const
    {
        // This is the factor of Poisseuille's equation
        const Scalar radius = this->radius(element);
        double gamma = 2;
        return M_PI*radius*radius*radius*radius/(2*(2+gamma));
    }

    /*!
     * \brief Function for defining the intrinsic (absolute) permeability.
     *
     * \return intrinsic (absolute) permeability
     * \param globalPos The position of the center of the element
     */
    const Scalar intrinsicPermeabilityAtPos (const GlobalPosition& globalPos) const
    {
        DUNE_THROW(Dune::InvalidStateException, "The spatial parameters do not provide a intrinsicPermeabilityAtPos() method.");
    }

private:
    bool parametersAvailable_ = false;
    std::vector<Scalar> radius_;
    std::vector<Scalar> velocity_estimate_;
    const GridView gridView_;

    Implementation &asImp_()
    {
        return *static_cast<Implementation*> (this);
    }

    const Implementation &asImp_() const
    {
        return *static_cast<const Implementation*> (this);
    }
};

} // namespace Dumux

#endif
