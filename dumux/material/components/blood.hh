// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief Blood as a single phase fluid
 */
#ifndef DUMUX_BLOOD_HH
#define DUMUX_BLOOD_HH

#include <dumux/material/components/component.hh>

namespace Dumux
{
/*!
 * \ingroup Components
 *
 * \brief Blood component (controlled by the input file)
 *
 * \tparam Scalar The type used for scalar values
 */
template <class TypeTag>
class Blood : public Component<typename GET_PROP_TYPE(TypeTag, Scalar), Blood<TypeTag> >
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

public:
    /*!
     * \brief A human readable name for blood.
     */
    static const char *name()
    { return "Blood"; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of blood
     */
    static Scalar molarMass()
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Blood, MolarMass); }

    /*!
     * \brief Returns true if the liquid phase is assumed to be compressible
     */
    static bool liquidIsCompressible()
    { return false; }

    /*!
     * \brief The density of pure water at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDensity(Scalar temperature, Scalar pressure)
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Blood, Density); }

    /*!
     * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure water.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidViscosity(Scalar temperature, Scalar pressure)
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Blood, Viscosity); }

};

} // end namespace Dumux

#endif
